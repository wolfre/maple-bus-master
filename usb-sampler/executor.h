#ifndef _EXECUTOR_H
#define _EXECUTOR_H

#include <stdint.h>
#include <stdio.h>
#include <libusb-1.0/libusb.h>

#define USB_VENDOR_ID       0x16c0      
#define USB_PRODUCT_ID      0x05dc
#define USB_ENDPOINT_CMD_READ   (LIBUSB_ENDPOINT_IN  | 1)
#define USB_ENDPOINT_CMD_WRITE  (LIBUSB_ENDPOINT_OUT | 1)
#define USB_ENDPOINT_DATA_READ  (LIBUSB_ENDPOINT_IN  | 2)
#define USB_ENDPOINT_DATA_WRITE (LIBUSB_ENDPOINT_OUT | 2)
#define USB_TIMEOUT         1000
#define USB_MAX_PKG_SIZE    64



typedef struct
{
    FILE* input;
    FILE* output;
    libusb_device_handle* handle;
    
    uint16_t tx_buffer_size;
    uint8_t* tx_buffer;
    
    uint16_t rx_sample_buffer_size;
    uint8_t* rx_sample_buffer;
}
executor;


executor* executor_new(FILE* input, FILE* output);
void executor_delete(executor* object);

void executor_run(executor* object);


#endif
