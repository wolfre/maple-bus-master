
// Lots stolen from here https://github.com/Mathias-L/STM32F4-libusb-example/blob/master/simple/test.c

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>


#include "executor.h"





// Status request
static uint8_t maple_status_request[] = { 0x00, 0x00, 0x20, 0x01, 0x01 };
//static uint8_t maple_status_request[MAPLE_TX_BUFFER];




int main(int argc, const char** argv)
{
    if( argc < 2 )
    {
        puts("Need action");
        return -1;
    }
    
    if( strcmp(argv[1], "run-executor") != 0)
    {
        fprintf(stderr, "Don't know what to do with action '%s'\n", argv[1]);
        return -1;
    }
    
    
    executor* ex = executor_new(stdin, stdout);
    executor_run(ex);
    executor_delete(ex);
    return 0;
    

    return 0;
}
