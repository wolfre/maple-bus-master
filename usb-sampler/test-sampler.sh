#!/bin/bash

function fail
{
    echo "$@" 1>&2
    exit 1
}

function assert_eq
{
    if [[ "$1" != "$2" ]] ; then
        fail "$3 - Expected $2 but got $1"
    else
        echo "Pass '$3'"
    fi
}

function hex_to_bin
{
    # https://unix.stackexchange.com/questions/218514/xxd-output-without-line-breaks
    xxd -r -p -c 1000000
}

function bin_to_hex
{
    # https://unix.stackexchange.com/questions/218514/xxd-output-without-line-breaks
    xxd -p -c 1000000
} 


echo "00" | hex_to_bin | ./sampler run-executor
assert_eq "$?" 0 "exit"

output=$( echo "0300" | hex_to_bin | ./sampler run-executor | bin_to_hex )
assert_eq "$?" 0 "exit"
assert_eq "$output" "42" "Selftest"

