#include "common.h"
#include "executor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define COMMAND_GET_INFO 1
#define COMMAND_WRITE_TX_DATA 2
#define COMMAND_READ_SAMPLE_BUFFER 3
#define COMMAND_EXECUTE 4
#define COMMAND_COPY_TX_TO_RX 5

#define STATUS_OK  1
#define STATUS_TIMEOUT  2

#define encode_u16_msb(n)  (uint8_t)((n>>8) & 0xff)
#define encode_u16_lsb(n)  (uint8_t)(n & 0xff)

static void sleep_process_delay()
{
    usleep(1000*1);
}

static void sleep_execute_delay()
{
    // The actual delay is more like 8 ms
    usleep(1000*15);
}

static inline uint16_t decode_msb_first_u16(uint8_t* d)
{
    return (((uint16_t)d[0]) << 8) | (uint16_t)d[1];
}

static void usb_send(executor* me, uint8_t ep, uint32_t len, uint8_t* data)
{
    int nread;
    int ret = libusb_bulk_transfer(me->handle, ep, data, len, &nread, USB_TIMEOUT);
    if (ret != 0 || nread != len)
    {
        fprintf(stderr, "ERROR in bulk write to 0x%02x: %d (%d != %d)\n", ep, ret, nread, len);
        exit(-1);
    }
}

static uint32_t usb_receive(executor* me, uint8_t ep, uint32_t max_len, uint8_t* data)
{
    int nread;
    int ret = libusb_bulk_transfer(me->handle, ep, data, max_len, &nread, USB_TIMEOUT);
    if (ret != 0 || nread < 1)
    {
        fprintf(stderr, "ERROR in bulk read response from 0x%02x: %d (%d)\n", ep, ret, max_len);
        exit(-1);
    }
    
    return (uint32_t)nread;
}

static void usb_send_cmd(executor* me, uint32_t len, uint8_t* data)
{
    usb_send(me, USB_ENDPOINT_CMD_WRITE, len, data);
}

static uint32_t usb_receive_status(executor* me, uint32_t max_len, uint8_t* data)
{
    return usb_receive(me, USB_ENDPOINT_CMD_READ, max_len, data);
}

static void usb_send_data(executor* me, uint32_t len, uint8_t* data)
{
    usb_send(me, USB_ENDPOINT_DATA_WRITE, len, data);
}

static uint32_t usb_receive_data(executor* me, uint32_t max_len, uint8_t* data)
{
    return usb_receive(me, USB_ENDPOINT_DATA_READ, max_len, data);
}

// Gets info about the device and stores some importand things in our members
static void cmd_get_info(executor* me)
{
    uint8_t cmd = COMMAND_GET_INFO;
    usb_send_cmd(me, sizeof(cmd), &cmd);
    sleep_execute_delay();
    
    
    uint8_t resp[5];
    uint16_t read =  usb_receive_status(me, sizeof(resp), resp);
    
    if( read < 1 )
        die_0("Failed to read status, possibly not a maple usb device");
    
    if( read < sizeof(resp) )
        die_1("Got less data than expected, possibly incompatible device, indicated interface version: %d", resp[0]);
    
    if( resp[0] != 1 )
        die_1("Incompatible device interface version: %d", resp[0]);
    
    me->tx_buffer_size = decode_msb_first_u16(resp + 1);
    me->rx_sample_buffer_size = decode_msb_first_u16(resp + 3);
}

// Will write the content of our TX buffer to the device TX buffer, up to len bytes
static void cmd_write_tx_data(executor* me, uint32_t len)
{
    // Start writing at offset 0
    uint8_t cmd[] = { COMMAND_WRITE_TX_DATA, 0, 0 };
    
    usb_send_cmd(me, sizeof(cmd), cmd);
    sleep_execute_delay();
    
    uint32_t off = 0;
    while(off < len )
    {
        uint32_t w = len - off;
        if( w > USB_MAX_PKG_SIZE )
            w = USB_MAX_PKG_SIZE;
        
        usb_send_data(me, w, me->tx_buffer + off);
        off += w;
        sleep_process_delay();
    }
}

// Will read the entire sample buffer from device to our sample buffer
static void cmd_read_sample_buffer(executor* me)
{
    // Start reading at offset 0
    uint8_t cmd[] = { COMMAND_READ_SAMPLE_BUFFER, 0, 0 };
    
    usb_send_cmd(me, sizeof(cmd), cmd);
    sleep_execute_delay();
    
    uint32_t off = 0;
    while(off < me->rx_sample_buffer_size )
    {
        uint32_t r = me->rx_sample_buffer_size - off;
        if( r > USB_MAX_PKG_SIZE )
            r = USB_MAX_PKG_SIZE;
        
        r = usb_receive_data(me, r, me->rx_sample_buffer + off);
        off += r;
        sleep_process_delay();
    }
}

static uint8_t cmd_execute(executor* me, uint16_t len)
{
    if( len > me->tx_buffer_size )
        die_1("Execute len %d is too large", len);
    
    uint8_t cmd[] =
    {
        COMMAND_EXECUTE,
        encode_u16_msb(len),
        encode_u16_lsb(len),
    };
    
    usb_send_cmd(me, sizeof(cmd), cmd);
    sleep_execute_delay();
    
    uint8_t result = 0;
    usb_receive_status(me, 1, &result);
    
    if( result != STATUS_TIMEOUT && result != STATUS_OK )
        die_1("Unknown status returned %d", result);
    
    return result;
}

static void cmd_copy_tx_to_rx(executor* me)
{
    uint8_t cmd = COMMAND_COPY_TX_TO_RX;
    usb_send_cmd(me, sizeof(cmd), &cmd);
    sleep_execute_delay();
}

executor* executor_new(FILE* input, FILE* output)
{
    executor* me = malloc(sizeof(executor));
    memset(me, 0, sizeof(executor));
    
    me->input = input;
    me->output = output;
    
    fprintf(stderr, "Will now look for %04x:%04x\n", USB_VENDOR_ID, USB_PRODUCT_ID);
    
    libusb_context* ctx = NULL;
    libusb_init(&ctx);
    
    me->handle = libusb_open_device_with_vid_pid(ctx, USB_VENDOR_ID, USB_PRODUCT_ID);
    
    if (me->handle == NULL)
    {
        die_0("device not found");
    }
    
    fprintf(stderr, "Device found\n");
     
    //Claim Interface 0 from the device
    int r = libusb_claim_interface(me->handle, 0);
    if (r < 0) 
    {
        die_1("usb_claim_interface error %d", r);
    }
    
    fprintf(stderr, "Interface claimed\n");
    
    
    cmd_get_info(me);
    fprintf(stderr, "Device info: tx buffer size = %d, rx sample buffer size = %d\n", me->tx_buffer_size, me->rx_sample_buffer_size);
    
    me->tx_buffer = malloc(me->tx_buffer_size);
    me->rx_sample_buffer = malloc(me->rx_sample_buffer_size);
    
    
    fprintf(stderr, "Setup done\n");
    return me;
}

void executor_delete(executor* me)
{
    if( me->rx_sample_buffer != NULL )
        free(me->rx_sample_buffer );
    
    if( me->tx_buffer != NULL )
        free(me->tx_buffer );
    
    // TODO cleanup USB
    
    me->input = NULL;
    me->output = NULL;
    me->rx_sample_buffer = NULL;
    me->tx_buffer = NULL;
    
    free(me);
}

static void reset_rx_buffer(executor* me)
{
    for( int i=0; i<me->rx_sample_buffer_size; ++i)
        me->rx_sample_buffer[i] = 0xff; // both data lines are idle high
}

static uint32_t read_data_to_buffer(executor* me)
{
    uint32_t len = 0;
    
    int tmp = fgetc(me->input);
    if( tmp < 0 )
        die_0("EOF while reading length (MSB)");
    
    len |= tmp & 0xff;
    len <<= 8;
    
    tmp = fgetc(me->input);
    if( tmp < 0 )
        die_0("EOF while reading length (LSB)");
    
    len |= tmp & 0xff;
    
    if( len > me->tx_buffer_size )
        die_1("Length (%d) was greater than buffer size", len);
    
    
    size_t read = 0;
    while(read < len)
    {
        size_t left = len-read;
        size_t r = fread ( me->tx_buffer, 1, left, me->input );
        
        if( r < 1 )
            die_0("EOF while reading data");
        
        read += r;
    }
    
    return len;
}

static void write_data_to_output(executor* me, uint32_t length, uint8_t* data)
{
    size_t written = 0;
    while(written < length)
    {
        size_t left = length - written;
        size_t w = fwrite ( data, 1, left, me->output );
        if( w < 1 )
            die_1("EOF while writing %d bytes of data", length);
        
        written += w;
    }
}

static uint32_t get_bit_sample_count(executor* me)
{
    return me->rx_sample_buffer_size * 4;
}

static uint32_t bit_to_byte_length(uint32_t bits)
{
    uint32_t byte_length = bits;    
    byte_length /= 8;
    if( (bits % 8) != 0)
        byte_length += 1;
    return byte_length;
}

static void write_raw_packet_to_output(executor* me, uint32_t bit_sample_count)
{
    uint8_t sample_count[4];
    
    uint32_t tmp = bit_sample_count;
    sample_count[3] = tmp & 0xff;
    tmp >>= 8;
    sample_count[2] = tmp & 0xff;
    tmp >>= 8;
    sample_count[1] = tmp & 0xff;
    tmp >>= 8;
    sample_count[0] = tmp & 0xff;
    
    write_data_to_output(me, sizeof(sample_count), sample_count);
    
    if( bit_sample_count > 0 )
    {
        uint32_t byte_length = bit_to_byte_length(bit_sample_count);
        write_data_to_output(me, byte_length * 2, me->rx_sample_buffer);
    }
    
    fflush(me->output);    
}

static void do_request_response(executor* me)
{
    uint32_t len = read_data_to_buffer(me);
    cmd_write_tx_data(me, len);
    
 
    if( cmd_execute(me, len) != STATUS_OK )
    {
        write_raw_packet_to_output(me, 0);
    }
    else
    {
        cmd_read_sample_buffer(me);
        write_raw_packet_to_output(me, me->rx_sample_buffer_size * 4);
    }
}

static void self_test(executor* me)
{
    reset_rx_buffer(me);
    
    for(int i=0; i< me->tx_buffer_size; ++i)
        me->tx_buffer[i] = (uint8_t)(i % 251); // 251 is the largest 8-bit prime
    
    cmd_write_tx_data(me, me->tx_buffer_size);
    cmd_copy_tx_to_rx(me);
    cmd_read_sample_buffer(me);
    
    for(int i=0; i< me->tx_buffer_size; ++i)
    {
        if( me->tx_buffer[i] != me->rx_sample_buffer[i] )
        {
            fprintf(stderr, "Sample buffer differed at %04x, expected 0x%02x but got 0x%02x\n", i, me->tx_buffer[i], me->rx_sample_buffer[i]);
            die_0("Selftest failed");
        }
    }
    
    uint8_t ok = 0x42;
    write_data_to_output(me, 1, &ok);
}

void executor_run(executor* me)
{
    while(1)
    {
        int next_cmd = fgetc(me->input);
        if( next_cmd == 0 || next_cmd < 0 )
            return;
        
        if( next_cmd == 1 )
            do_request_response(me);

        if( next_cmd == 3 )
            self_test(me);
    }
}
