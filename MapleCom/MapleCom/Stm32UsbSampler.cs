﻿using System;
using System.Diagnostics;
using System.IO;

namespace MapleCom
{
    public class Stm32UsbSampler : IMapleBus
    {
        /// <summary>
        /// Timeout for sampler to return data. Usually a controller answers within
        /// 100 us, so 2 sec is more than enough to get the answer.
        /// </summary>
        static readonly TimeSpan samplerTimeout = TimeSpan.FromSeconds(2);
        readonly ILog log;
        readonly FileInfo samplerExe;

        readonly MapleCodec mapleCodec;
        readonly SamplerCodec samplerCodec;

        Process sampler = null;

        public Stm32UsbSampler(ILog log, FileInfo samplerExe)
        {
            this.log = log;
            this.samplerExe = samplerExe;

            mapleCodec = new MapleCodec(log);
            samplerCodec = new SamplerCodec(log);
        }

        ProcessStartInfo GenSamplerArgs()
        {
            return new ProcessStartInfo()
            {
                CreateNoWindow = true,
                ErrorDialog = false,

                RedirectStandardError = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                WindowStyle = ProcessWindowStyle.Hidden,

                FileName = samplerExe.FullName,
                Arguments = "run-executor"
            };
        }

        void StopSampler()
        {
            if (sampler == null)
                return;

            if (sampler.HasExited == false)
            {
                try
                {
                    sampler.StandardInput.BaseStream.WriteByte(SamplerOpcodes.OpCodeExit);
                    if (sampler.WaitForExit((int)samplerTimeout.TotalMilliseconds) == false)
                        sampler.Kill();
                }
                catch (Exception x)
                {
                    log.That(x);
                }
            }

            sampler = null;
        }

        void StartSampler()
        {
            StopSampler();
            sampler = Process.Start(GenSamplerArgs());
        }

        static class SamplerOpcodes
        {
            public const byte OpCodeExit = 0x00;
            public const byte OpCodeRequestResponse = 0x01;
        }

        MapleBusRawPacket DoBusTransaction(MapleBusRawPacket request)
        {
            if (sampler == null || sampler.HasExited)
            {
                StopSampler();
                StartSampler();
            }

            sampler.StandardInput.BaseStream.WriteByte(SamplerOpcodes.OpCodeRequestResponse);
            samplerCodec.ToCommandDataInput(sampler.StandardInput.BaseStream, request);
            var undecodedSamples = samplerCodec.FromCommandOutput(sampler.StandardOutput.BaseStream);

            if (undecodedSamples.samples == 0)
                throw new TimeoutException("Device did not answer");

            return samplerCodec.FromSamples(undecodedSamples);
        }

        public MapleBusDecodedFrame DoBusTransaction(MapleBusDecodedFrame request)
        {
            var rawReq = mapleCodec.Encode(request);
            var rawResp = DoBusTransaction(rawReq);
            return mapleCodec.Decode(rawResp);
        }

        public void Dispose()
        {
            try
            {
                StopSampler();
            }
            catch (Exception x)
            {
                log.That(x);
            }
        }
    }
}
