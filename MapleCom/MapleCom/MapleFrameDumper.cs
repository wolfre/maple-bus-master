﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MapleCom
{
    public class MapleFrameDumper
    {
        static readonly Dictionary<UInt32, string> functionBitsToName = new Dictionary<uint, string>();

        static MapleFrameDumper()
        {
            var bits = typeof(MapleConsts.Function)
                .GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                .Where(f => f.FieldType == typeof(UInt32))
                .Where(f => f.IsLiteral);

            foreach (var e in bits)
                functionBitsToName.Add((UInt32)e.GetValue(null), e.Name);

        }

        readonly ILog log;

        public MapleFrameDumper(ILog log)
        {
            this.log = log;
        }

        public void Dump(MapleBusRawPacket raw)
        {
            log.That(nameof(MapleBusRawPacket) + " [" + raw.data.Length + "]: " + Misc.ToHex(raw.data));
        }

        public string ToString(MapleBusDecodedFrame.MapleAddress a)
        {
            if (a == null)
                return "{null}";

            var s = "{";

            s += "port=" + a.portNumber + ",";

            var p = new List<string>();

            if (a.mainPeripheral)
                p.Add("Main");
            if (a.subPeripheral1)
                p.Add("Sub1");
            if (a.subPeripheral2)
                p.Add("Sub2");
            if (a.subPeripheral3)
                p.Add("Sub3");
            if (a.subPeripheral4)
                p.Add("Sub4");
            if (a.subPeripheral5)
                p.Add("Sub5");


            if (p.Count == 0)
            {
                s += "Console";
            }
            else
            {
                s += string.Join("+", p);
            }

            s += "}";
            return s;
        }

        static string NameFunction(UInt32 f)
        {
            if (functionBitsToName.ContainsKey(f))
                return functionBitsToName[f];
            return "0x" + f.ToString("X3");
        }

        string ToString(MapleBusDeviceInfoFrame.FunctionData fData)
        {
            var s = NameFunction(fData.function) + ":";

            {
                var unk = fData as MapleBusDeviceInfoFrame.FunctionDataUnknown;
                if (unk != null)
                    return s + "{data=0x" + unk.data.ToString("X8") + "}";
            }

            return s + "?";
        }

        string Concat(Dictionary<string, string> kv)
        {
            return string.Join(",", kv.Select(kvPair => kvPair.Key + "=" + kvPair.Value));
        }

        string ToStringPayload(MapleBusDecodedFrame pay)
        {
            {
                var unk = pay as MapleBusUnknownFrame;
                if (unk != null)
                    return "code=" + unk.code + ",data=" + Misc.ToHex(unk.data);
            }
            {
                var dNfo = pay as MapleBusDeviceInfoFrame;
                if (dNfo != null)
                {
                    var s = "supportedFunctions=[" + string.Join(",", dNfo.supportedFunctions.Select(NameFunction)) + "]";
                    s += ",functionData=[" + string.Join(",", dNfo.functionData.Select(ToString)) + "]";

                    return s + "," + Concat(new Dictionary<string, string>()
                    {
                        { "areaCode", ""+dNfo.areaCode },
                        { "connectorDirection", ""+dNfo.connectorDirection },
                        { "productName", "'"+dNfo.productName+"'" },
                        { "productLicense", "'"+dNfo.productLicense+"'"},
                        { "standbyPower", ""+dNfo.standbyPower },
                        { "maxPower", ""+dNfo.maxPower },
                    });
                }
            }
            {
                var data = pay as MapleBusDataTransferFrame;
                if (data != null)
                    return "data[" + data.data.Length + "]: " + Misc.ToHex(data.data);
            }

            return "n/a";
        }

        public string ToString(MapleBusDecodedFrame dec)
        {
            return dec.GetType() + " [r=" + ToString(dec.recipient) + ",s=" + ToString(dec.sender) + "] " + ToStringPayload(dec);
        }

        public void Dump(MapleBusDecodedFrame dec)
        {
            log.That(ToString(dec));
        }
    }
}
