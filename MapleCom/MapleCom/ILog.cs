﻿using System;
namespace MapleCom
{
    public interface ILog
    {
        void That(string text);
        void That(Exception ex);
    }
}
