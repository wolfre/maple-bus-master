﻿using System;
namespace MapleCom
{
    /// <summary>
    /// Raw unprocessed maple bus samples, as captured from the wire by sampler.
    /// See http://mc.pp.se/dc/maplewire.html
    /// </summary>
    public class MapleSamplesRaw
    {
        public UInt32 samples;
        public byte[] data1;
        public byte[] data5;
    }

    /// <summary>
    /// Maple bus packet with raw data, already wordswapped from the bus bits.
    /// See http://mc.pp.se/dc/maplewire.html
    /// </summary>
    public class MapleBusRawPacket
    {
        public byte[] data;
        public byte checksum;
    }

    /// <summary>
    /// A decoded maple bus packet, see http://mc.pp.se/dc/maplebus.html
    /// </summary>
    public abstract class MapleBusDecodedFrame 
    {
        public class MapleAddress
        {
            /// <summary>Console port [0;3]</summary>
            public byte portNumber;

            /// <summary>The main peripherial is addressed</summary>
            public bool mainPeripheral;
            /// <summary>Sub peripherial 5 is addressed</summary>
            public bool subPeripheral5;
            /// <summary>Sub peripherial 4 is addressed</summary>
            public bool subPeripheral4;
            /// <summary>Sub peripherial 3 is addressed</summary>
            public bool subPeripheral3;
            /// <summary>Sub peripherial 2 is addressed</summary>
            public bool subPeripheral2;
            /// <summary>Sub peripherial 1 is addressed</summary>
            public bool subPeripheral1;
        }

        public MapleAddress recipient;
        public MapleAddress sender;
    }

    public class MapleBusRequestDeviceInfoFrame : MapleBusDecodedFrame
    {

    }

    public class MapleBusGetConditionFrame : MapleBusDecodedFrame
    {
        public UInt32 function;
    }

    public class MapleBusDeviceInfoFrame : MapleBusDecodedFrame
    {
        public abstract class FunctionData
        {
            public UInt32 function;
        }

        public class FunctionDataUnknown : FunctionData
        {
            // NOTE memory card supposedly provide this structure
            // https://elixir.bootlin.com/linux/latest/source/drivers/mtd/maps/vmu-flash.c#L618
            // However aplying this to actual data from a VMU (0x7E7E3F40) didn't make any sense.
            public UInt32 data;
        }

        /// <summary>function codes supported by this peripheral</summary>
        public UInt32[] supportedFunctions;
        /// <summary>additional info for the supported function codes</summary>
        public FunctionData[] functionData;
        /// <summary>regional code of peripheral</summary>
        public byte areaCode;
        /// <summary>physical orientation of bus connection</summary>
        public byte connectorDirection;
        /// <summary>name of peripheral, 30 chars</summary>
        public string productName;
        /// <summary>license statement, 60 chars</summary>
        public string productLicense;
        /// <summary>standby power consumption/summary>
        public UInt16 standbyPower;
        /// <summary>maximum power consumption/summary>
        public UInt16 maxPower;
    }

    public class MapleBusDataTransferFrame : MapleBusDecodedFrame
    {
        public byte[] data;
    }

    public class MapleBusBlockReadFrame : MapleBusDecodedFrame
    {
        /// <summary>See <see cref="MapleConsts.Function"/></summary>
        public UInt32 function;
        /// <summary>Partition number. Should be 0 for VMS:s.</summary>
        public byte partition;
        /// <summary>Sequence number for piecewise block access</summary>
        public byte phase;
        /// <summary>Block number. Use 0 for LCD screens.</summary>
        public UInt16 block;
    }

    public class MapleBusBlockWriteFrame : MapleBusBlockReadFrame
    {
        /// <summary>The data, must be 4-byte aligned</summary>
        public byte[] data;
    }

    /// <summary>
    /// Finished a block, written in 4 phases using the <see cref="MapleBusBlockWriteFrame"/>.
    /// Set <see cref="MapleBusBlockReadFrame.phase"/> to <see cref="MapleBusBlockWriteFinishFrame.FinishPhase"/>.
    /// Not sending this after 4 phases / for each block, will give crazy and random results on the flash.
    /// </summary>
    public class MapleBusBlockWriteFinishFrame : MapleBusBlockReadFrame
    {
        /// <summary>
        /// https://github.com/requeijaum/arduino-maple/blob/616e242b3fbcc9e08303df0bcbe8bb416c3d3727/maple.py#L402
        /// </summary>
        public const byte FinishPhase = 4;
    }

    public class MapleBusCommandAcknowledgeFrame : MapleBusBlockReadFrame
    {
    }

    public class MapleBusCommandNeedsToBeSentAgainFrame : MapleBusBlockReadFrame
    {
    }


    /// <summary>
    /// A decoded maple bus packet with unknown payload
    /// </summary>
    public class MapleBusUnknownFrame : MapleBusDecodedFrame
    {
        /// <summary>
        /// http://mc.pp.se/dc/maplebus.html#cmds
        /// </summary>
        public sbyte code;
        /// <summary>Any payload after the header</summary>
        public byte[] data;
    }
}
