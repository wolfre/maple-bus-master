﻿using System;
namespace MapleCom
{
    public static class Misc
    {
        public static byte[] ByteArrayFromHex(string hex)
        {
            hex = hex.ToLower();
            if (hex.StartsWith("0x"))
                hex = hex.Substring(2);

            if (hex.Length % 2 != 0)
                hex += "0";

            var ret = new byte[hex.Length / 2];
            for (int i = 0; i < ret.Length; ++i)
                ret[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);

            return ret;
        }

        public static string ToHex(byte[] data, string prefix = "0x")
        {
            return prefix + BitConverter.ToString(data).Replace("-", "");
        }

        public static string ToHex(byte data, string prefix = "0x")
        {
            return prefix + data.ToString("x02");
        }
    }
}
