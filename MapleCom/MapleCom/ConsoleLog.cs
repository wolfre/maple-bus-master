﻿using System;
namespace MapleCom
{
    public class ConsoleLog : ILog
    {
        static string ToPrefix(DateTime ts)
        {
            return "[" + ts.ToString("HH:mm:ss.fff") + "] ";
        }

        void Write(string prefix, string line)
        {
            Console.Write(prefix);
            Console.WriteLine(line);
        }

        public void That(string text)
        {
            var p = ToPrefix(DateTime.Now);
            Write(p, text);
        }

        void Write(string prefix, Exception ex)
        {
            if (ex == null)
                return;

            Write(prefix, ex.GetType() + " - " + ex.Message);
            foreach(var l in ex.StackTrace.Split('\n','\r'))
                Write(prefix, l);

            var aggr = ex as AggregateException;
            if (aggr != null)
                foreach (var a in aggr.InnerExceptions)
                    Write(prefix, a);

            Write(prefix, ex.InnerException);
        }

        public void That(Exception ex)
        {
            var p = ToPrefix(DateTime.Now);
            Write(p, ex);
        }
    }
}
