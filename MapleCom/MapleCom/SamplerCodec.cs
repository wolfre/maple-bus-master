﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MapleCom
{
    public class MapleBusException : Exception
    {
        public MapleBusException(string msg) : base(msg)
        {
        }
    }

    static class Protocol
    {
        public static Exception Violation(string message)
        {
            return new MapleBusException("Maple bus protocol violation: " + message);
        }
    }

    public class SamplerCodec
    {
        readonly ILog log;

        public SamplerCodec(ILog log)
        {
            this.log = log;
        }

        byte[] ReadOrDie(UInt32 length, Stream samplerOutput)
        {
            var buffer = new byte[length];
            int read = 0;

            while (read < length)
            {
                var r = samplerOutput.Read(buffer, read, (int)(length - read));
                if (r < 1)
                    throw new IOException("EOF while reading " + length + " bytes from stream");
                read += r;
            }

            return buffer;
        }

        public MapleSamplesRaw FromCommandOutput(Stream samplerOutput)
        {
            var length = ReadOrDie(4, samplerOutput);

            UInt32 samples = length[0];
            samples <<= 8;
            samples |= length[1];
            samples <<= 8;
            samples |= length[2];
            samples <<= 8;
            samples |= length[3];

            UInt32 fullBytes = samples / 8;
            if (samples % 8 != 0)
                fullBytes += 1;

            var data1 = ReadOrDie(fullBytes, samplerOutput);
            var data5 = ReadOrDie(fullBytes, samplerOutput);

            return new MapleSamplesRaw()
            {
                samples = samples,
                data1 = data1,
                data5 = data5,
            };
        }

        public void ToCommandDataInput(Stream input, MapleBusRawPacket data)
        {
            var totalLength = data.data.Length + 1;

            if (totalLength > UInt16.MaxValue)
                throw new NotSupportedException("Data (" + totalLength + " bytes) is lager than " + UInt16.MaxValue + " bytes");

            var length = new byte[]
            {
                (byte)((totalLength >> 8) & 0xff),
                (byte)(totalLength & 0xff),
            };

            input.Write(length, 0, length.Length);

            var dataSwapped = new byte[data.data.Length];
            WordSwap(dataSwapped, data.data);

            input.Write(dataSwapped, 0, dataSwapped.Length);
            input.WriteByte(data.checksum);
        }

        public MapleSamplesRaw FromCompactLine(string line)
        {
            // s=152,D1=0x00888911111222234c44588889111111113200,D5=0x55a22244445888899911122222444444449801

            var p = line.Split(',');
            if (p[0].StartsWith("s=") == false)
                throw new ArgumentException("Malformed line '" + line + "'");

            var samples = UInt32.Parse(p[0].Substring(2));

            if (p[1].StartsWith("D1=") == false)
                throw new ArgumentException("Malformed line '" + line + "'");

            var data1 = Misc.ByteArrayFromHex(p[1].Substring(3));

            if (p[2].StartsWith("D5=") == false)
                throw new ArgumentException("Malformed line '" + line + "'");

            var data5 = Misc.ByteArrayFromHex(p[2].Substring(3));

            return new MapleSamplesRaw()
            {
                samples = samples,
                data1 = data1,
                data5 = data5,
            };
        }

        class SamplerDumper : IDisposable
        {
            TextWriter wrt;

            const char Sep = ',';

            public SamplerDumper(FileInfo csv)
            {
                wrt = new StreamWriter(csv.Open(FileMode.Create, FileAccess.Write, FileShare.Read));

                wrt.WriteLine("Sample" + Sep + "D1" + Sep + "D5");
            }

            public void Dispose()
            {
                if (wrt == null)
                    return;

                wrt.Flush();
                wrt.Dispose();
                wrt = null;
            }

            string ToCsvD5(bool d)
            {
                return d ? "1" : "0";
            }

            string ToCsvD1(bool d)
            {
                return d ? "3" : "2";
            }

            UInt32 bit = 0;

            public void NextSampleBits(bool d1, bool d5)
            {
                wrt.WriteLine(bit + "" + Sep + ToCsvD1(d1) + Sep + ToCsvD5(d5));
                ++bit;
            }
        }

        void ProcessRaw(MapleSamplesRaw raw, Action<bool, bool> nextBits)
        {
            for (int i = 0; i < raw.samples; ++i)
            {
                var offByte = i / 8;
                var bitMask = (byte)(0x80 >> i % 8);

                var d1 = (raw.data1[offByte] & bitMask) != 0;
                var d5 = (raw.data5[offByte] & bitMask) != 0;

                nextBits(d1, d5);
            }
        }

        static readonly FileInfo samplesCsv = new FileInfo("samples.csv");

        public MapleBusRawPacket FromSamples(MapleSamplesRaw raw)
        {
            try
            {
                var sm = new MapleBusStateMachine();

                ProcessRaw(raw, (d1, d5) => sm.NextSampleBits(d1, d5));

                var data = sm.GetData();
                return ToMapleBusRawPacket(data);
            }
            catch (Exception)
            {
                log.That("Will dump samples to '" + samplesCsv.FullName + "' due to exception");

                using (var dmp = new SamplerDumper(samplesCsv))
                    ProcessRaw(raw, (d1, d5) => dmp.NextSampleBits(d1, d5));

                throw;
            }
        }

        MapleBusRawPacket ToMapleBusRawPacket(byte[] data)
        {
            var actualDataLength = data.Length - 1;
            if (actualDataLength % 4 != 0)
                throw Protocol.Violation("the ammount of extracted data (" + data.Length + " bytes) was not a 4-byte-word + checksum format");

            // http://mc.pp.se/dc/maplewire.html
            // data on the wire is in 4-byte words and looks like this:
            // 0x 1234 5678  1234 5678
            // but the logical order is supposed to be this
            // 0x 7856 3412  7856 3412
            // do we have to swap every 4-byte block around

            var dataSwapped = new byte[actualDataLength];
            WordSwap(dataSwapped, data);

            return new MapleBusRawPacket()
            {
                data = dataSwapped,
                checksum = data[data.Length - 1]
            };
        }

        public string ToSampleRequestInput(MapleBusRawPacket raw)
        {
            if (raw == null)
                throw new ArgumentNullException(nameof(raw));
            if (raw.data == null)
                throw new ArgumentNullException(raw.GetType().Name + "." + nameof(raw.data));
            if (raw.data.Length < 4)
                throw new ArgumentException("At least 4 bytes of data are needed");
            if (raw.data.Length % 4 != 0)
                throw new ArgumentException("Data must be in 4-byte blocks, length was " + raw.data.Length);
                
            var dataSwapped = new byte[raw.data.Length];
            WordSwap(dataSwapped, raw.data);

            return Misc.ToHex(dataSwapped, "0x") + raw.checksum.ToString("x02");
        }

        /// <summary>
        /// Data on the bus is 4-byte swapped, this function does that
        /// </summary>
        /// <param name="dataSwapped">Data swapped</param>
        /// <param name="data">Data</param>
        static void WordSwap(byte[] dataSwapped, byte[] data)
        {
            for (int block = 0; block < dataSwapped.Length / 4; ++block)
            {
                int off = block * 4;

                dataSwapped[off + 0] = data[off + 3];
                dataSwapped[off + 1] = data[off + 2];
                dataSwapped[off + 2] = data[off + 1];
                dataSwapped[off + 3] = data[off + 0];
            }
        }
    }
}
