﻿using System;
using System.Collections.Generic;

namespace MapleCom
{
    public class UnstableMapleBus : IMapleBus
    {
        readonly ILog log;
        readonly IMapleBus unstable;
        readonly int maxRetries;
        readonly int sleepTry;

        public UnstableMapleBus(ILog log, IMapleBus unstable, int maxRetries)
        {
            this.log = log;
            this.unstable = unstable;
            this.maxRetries = maxRetries;
            sleepTry = maxRetries / 2;
        }

        public void Dispose()
        {
            unstable.Dispose();
        }

        public MapleBusDecodedFrame DoBusTransaction(MapleBusDecodedFrame request)
        {
            var exc = new List<Exception>();

            for (int i = 0; i < (maxRetries+1); ++i)
            {
                try
                {
                    return unstable.DoBusTransaction(request);
                }
                catch (Exception x)
                {
                    exc.Add(x);

                    // Stuff does not seem to go well, will relax a bit ...
                    if (exc.Count == sleepTry)
                        System.Threading.Thread.Sleep(500);
                }
            }

            throw new AggregateException("Bus transaction failed, too many errors", exc.ToArray());
        }
    }
}
