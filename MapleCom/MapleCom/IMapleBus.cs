﻿using System;
namespace MapleCom
{
    public interface IMapleBus : IDisposable
    {
        MapleBusDecodedFrame DoBusTransaction(MapleBusDecodedFrame request);
    }
}
