﻿using System;
using System.Collections.Generic;

namespace MapleCom
{
    /// <summary>
    /// Maple bus state machine, read http://mc.pp.se/dc/maplewire.html
    /// </summary>
    class MapleBusStateMachine
    {
        public byte[] GetData()
        {
            if (state != State.LeadOut)
                throw Protocol.Violation("state was not " + State.LeadOut + " but " + state + ", you possibly have garbled data (after " + samples + " samples)");
            if (dataPos % 8 != 0)
                throw Protocol.Violation("total data bits was " + dataPos + " which is not divisible by 8, you possibly have missing samples");

            return data.ToArray();
        }

        enum Clock
        {
            Data1,
            Data5,
        }

        enum State
        {
            LeadInStart,
            LeadInShort,
            Data,
            LeadOut
        }

        State state = State.LeadInStart;
        string leadIn = "";

        readonly List<byte> data = new List<byte>();
        byte dataNext = 0;
        int dataPos = 0;

        void NextDataBits(bool bit, Clock clock)
        {
            if (state == State.LeadInStart)
            {
                // Only the longer pattern is documented here http://mc.pp.se/dc/maplewire.html
                // We trigger on the shorter variant, see also notes below.
                const string ShortLeadInPattern = "Abbbb";
                string c = null;

                if (clock == Clock.Data1)
                    c = bit ? "A" : "a";

                if (clock == Clock.Data5)
                    c = bit ? "B" : "b";

                leadIn += c;

                if (leadIn.Length > ShortLeadInPattern.Length)
                    leadIn = leadIn.Substring(leadIn.Length - ShortLeadInPattern.Length);

                if (leadIn == ShortLeadInPattern)
                    state = State.LeadInShort;

                return;
            }

            if (state == State.LeadInShort)
            {
                // There are two types of lead in, only the longer one is documented here
                //   http://mc.pp.se/dc/maplewire.html
                //   The shorter one is actually missing the final transition from hi->low on pin 5,
                //   so it is one "bit short". The above detection will trigger on this short version
                //   setting the state to  State.LeadInShort  the next negative edge (this one right here) tells us
                //   what is going on:
                //   - negative edge on D5 -> long lead in, the next edge will be on D1 and this is the first bit
                //   - negative edge on D1 -> short lead in, the current edge is already the first data bit

                state = State.Data;

                if (clock == Clock.Data5)
                {
                    // It was a long lead in, and this was the last bit of lead in data
                    return;
                }

                // It really was a short leadin and this was the first bit of data.
                // Fall through and handle this bit as actual data ...
            }

            if (state == State.Data)
            {
                if ((dataPos & 1) == 0)
                {
                    if (clock != Clock.Data1)
                    {
                        state = State.LeadOut;
                        return;
                    }
                }
                else
                {
                    if (clock != Clock.Data5)
                        throw Protocol.Violation("expected clock on " + Clock.Data5 + ", but got " + clock + ", you probably have missing samples");
                }

                dataNext <<= 1;
                if (bit)
                    dataNext |= 1;

                ++dataPos;

                if (dataPos % 8 == 0)
                    this.data.Add(dataNext);

                return;
            }
        }

        UInt32 samples = 0;
        bool lastD1;
        bool lastD5;

        public void NextSampleBits(bool d1, bool d5)
        {
            if (samples == 0)
            {
                lastD1 = true;
                lastD5 = true;
            }

            if ((lastD1 == d1) && (lastD5 == d5))
            {
                ++samples;
                return;
            }

            // If both signal change at the same time we certainly have sample loss
            //  However in case both of them changed from low to hi, we can over look this
            //  as data is only valid on falling edges (we didn't miss any then)
            if ((lastD1 != d1) && (lastD5 != d5) && (d1 == false || d5 == false))
                throw Protocol.Violation("sample loss at sample " + samples);


            if (lastD1 == true && d1 == false)
                NextDataBits(d5, Clock.Data1);

            if (lastD5 == true && d5 == false)
                NextDataBits(d1, Clock.Data5);

            ++samples;
            lastD1 = d1;
            lastD5 = d5;
        }
    }

}
