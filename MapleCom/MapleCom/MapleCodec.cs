﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MapleCom
{
    public class MapleCodecException : Exception
    {
        public MapleCodecException(string msg) : base(msg)
        {
        }
    }

    public class MapleCodec
    {
        readonly Dictionary<Type, Action<MapleBusDecodedFrame, List<byte>>> encoder;
        readonly Dictionary<sbyte, Func<byte[], MapleBusDecodedFrame>> decode;
        readonly Dictionary<UInt32, Func<UInt32, MapleBusDeviceInfoFrame.FunctionData>> functionDataDecoders;

        readonly ILog log;

        public MapleCodec(ILog log)
        {
            this.log = log;

            encoder = new Dictionary<Type, Action<MapleBusDecodedFrame, List<byte>>>
            {
                { typeof(MapleBusUnknownFrame), EncodeMapleBusUnknownFrame },
                { typeof(MapleBusRequestDeviceInfoFrame), EncodeMapleBusRequestDeviceInfoFrame },
                { typeof(MapleBusGetConditionFrame), EncodeMapleBusGetConditionFrame },
                { typeof(MapleBusBlockWriteFrame), EncodeMapleBusBlockWriteFrame },
                { typeof(MapleBusBlockReadFrame), EncodeMapleBusBlockReadFrame },
                { typeof(MapleBusBlockWriteFinishFrame), EncodeMapleBusBlockWriteFinishFrame}
            };

            decode = new Dictionary<sbyte, Func<byte[], MapleBusDecodedFrame>>()
            {
                { MapleConsts.Command.DeviceInfo, DecodeMapleBusDeviceInfoFrame },
                { MapleConsts.Command.DataTransfer, DecodeMapleBusDataTransferFrame },
                { MapleConsts.Command.CommandAcknowledge, DecodeCommandAcknowledgeFrame },
                { MapleConsts.Command.CommandNeedsToBeSentAgain, DecodeMapleBusCommandNeedsToBeSentAgainFrame }
            };

            functionDataDecoders = new Dictionary<uint, Func<uint, MapleBusDeviceInfoFrame.FunctionData>>()
            {
                // TODO
            };
        }

        void HandleFunc(uint arg)
        {
        }


        #region encoders

        byte EncodeAddress(MapleBusDecodedFrame.MapleAddress address)
        {
            if (address.portNumber > 3)
                throw new ArgumentOutOfRangeException("Port number has to be [0;3], not " + address.portNumber);

            byte a = address.portNumber;
            a <<= 6;

            if (address.mainPeripheral)
                a |= 1 << 5;

            if (address.subPeripheral5)
                a |= 1 << 4;

            if (address.subPeripheral4)
                a |= 1 << 3;

            if (address.subPeripheral3)
                a |= 1 << 2;

            if (address.subPeripheral2)
                a |= 1 << 1;

            if (address.subPeripheral1)
                a |= 1 << 0;

            return a;
        }

        void EncodeHeader(List<byte> data, MapleBusDecodedFrame f, sbyte commandCode, byte words)
        {
            if (f.sender == null)
                throw new ArgumentException("Sender address was null");
            if (f.recipient == null)
                throw new ArgumentException("Recipient address was null");

            int commandCodeUnsigned = commandCode;
            commandCodeUnsigned &= 0xff;

            data.Add((byte)commandCodeUnsigned);
            data.Add(EncodeAddress(f.recipient));
            data.Add(EncodeAddress(f.sender));
            data.Add(words);
        }

        void Encode(List<byte> data, UInt32 u32)
        {
            var p0 = u32 & 0xff;
            u32 >>= 8;
            var p1 = u32 & 0xff;
            u32 >>= 8;
            var p2 = u32 & 0xff;
            u32 >>= 8;
            var p3 = u32 & 0xff;

            data.Add((byte)p3);
            data.Add((byte)p2);
            data.Add((byte)p1);
            data.Add((byte)p0);
        }

        void EncodeMapleBusRequestDeviceInfoFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            EncodeHeader(data, f, MapleConsts.Command.RequestDeviceInfo, 0);
        }

        void EncodeMapleBusGetConditionFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            var gc = (MapleBusGetConditionFrame)f;

            EncodeHeader(data, f, MapleConsts.Command.GetCondition, 1);
            Encode(data, gc.function);
        }

        const byte CommonReadWriteBlockLength = 2;

        void EncodeCommonReadWritePart(MapleBusBlockReadFrame rw, List<byte> data)
        {
            Encode(data, rw.function);
            data.Add(rw.partition);
            data.Add(rw.phase);
            data.Add((byte)(rw.block >> 8));
            data.Add((byte)(rw.block & 0xff));
        }

        void EncodeMapleBusBlockReadFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            var br = (MapleBusBlockReadFrame)f;

            EncodeHeader(data, f, MapleConsts.Command.BlockRead, CommonReadWriteBlockLength);
            EncodeCommonReadWritePart(br, data);
        }

        void EncodeMapleBusBlockWriteFinishFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            var br = (MapleBusBlockWriteFinishFrame)f;

            EncodeHeader(data, f, MapleConsts.Command.BlockWriteFinish, CommonReadWriteBlockLength);
            EncodeCommonReadWritePart(br, data);
        }

        void EncodeMapleBusBlockWriteFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            var bw = (MapleBusBlockWriteFrame)f;

            var dataFrames = bw.data.Length;
            if (dataFrames % 4 != 0)
                throw new MapleCodecException("Data in " + bw.GetType() + " has to be a multiple of 4 not " + dataFrames + " bytes");

            EncodeHeader(data, f, MapleConsts.Command.BlockWrite, (byte)(CommonReadWriteBlockLength + (dataFrames / 4)));
            EncodeCommonReadWritePart(bw, data);
            data.AddRange(bw.data);
        }

        void EncodeMapleBusUnknownFrame(MapleBusDecodedFrame f, List<byte> data)
        {
            var unk = (MapleBusUnknownFrame)f;

            var moreWords = unk.data == null ? 0 : unk.data.Length;
            if (moreWords % 4 != 0)
                throw new MapleCodecException("Data in " + unk.GetType() + " has to be a multiple of 4 not " + moreWords + " bytes");


            EncodeHeader(data, f, unk.code, (byte)(moreWords / 4));
            data.AddRange(unk.data);
        }

        public MapleBusRawPacket Encode(MapleBusDecodedFrame dec)
        {
            if (dec == null)
                throw new ArgumentNullException(nameof(dec));

            var t = dec.GetType();
            if (encoder.ContainsKey(t) == false)
                throw new NotSupportedException("I don't support encoding " + t);

            var data = new List<byte>();
            encoder[t](dec, data);

            var dataArr = data.ToArray();

            return new MapleBusRawPacket()
            {
                data = dataArr,
                checksum = CalcChecksum(dataArr),
            };
        }

        #endregion

        #region decoders

        UInt32 DecodeU32Be(byte[] enc, ref int off)
        {
            UInt32 n = 0;

            n |= enc[off];
            n <<= 8;
            ++off;
            n |= enc[off];
            n <<= 8;
            ++off;
            n |= enc[off];
            n <<= 8;
            ++off;
            n |= enc[off];
            ++off;

            return n;
        }

        UInt16 DecodeU16Le(byte[] enc, ref int off)
        {
            UInt16 n = 0;

            n |= enc[off];

            ++off;
            n |= (UInt16)(enc[off] << 8);
            ++off;

            return n;
        }

        #region function data decoders

        // TODO

        #endregion

        const int HeaderLength = 4;

        MapleBusDecodedFrame DecodeMapleBusDeviceInfoFrame(byte[] enc)
        {
            var expected = (28 * 4) + HeaderLength;
            if (enc.Length != expected)
                throw new MapleCodecException("Malformed payload: expected " + expected + " bytes but got " + enc.Length);

            var f = new MapleBusDeviceInfoFrame();

            int off = HeaderLength;
            var func = DecodeU32Be(enc, ref off);

            // NOTE this bit array should never contain more than 3 bits ...
            var allFuncs = new List<UInt32>();
            for (int i = 0; i < 32; ++i)
                if ((func & (1 << i)) != 0)
                    allFuncs.Add((UInt32)(1 << i));

            f.supportedFunctions = allFuncs.ToArray();

            // NOTE raw data always contains 3 entries for function data
            var functionDataRaw = new List<UInt32>();
            functionDataRaw.Add(DecodeU32Be(enc, ref off));
            functionDataRaw.Add(DecodeU32Be(enc, ref off));
            functionDataRaw.Add(DecodeU32Be(enc, ref off));

            var functionData = new List<MapleBusDeviceInfoFrame.FunctionData>();
            for (int fn = 0; fn < allFuncs.Count; ++fn)
            {
                var function = allFuncs[fn];
                var data = fn < functionDataRaw.Count ? functionDataRaw[fn] : 0;

                if (fn < functionDataRaw.Count && functionDataDecoders.ContainsKey(function))
                {
                    var dec = functionDataDecoders[function](data);
                    dec.function = function;
                    functionData.Add(dec);
                }
                else
                {

                    functionData.Add(new MapleBusDeviceInfoFrame.FunctionDataUnknown() { data = data, function = function });
                }
            }

            f.functionData = functionData.ToArray();

            f.areaCode = enc[off];
            ++off;

            f.connectorDirection = enc[off];
            ++off;

            f.productName = System.Text.Encoding.ASCII.GetString(enc, off, 30);
            off += 30;
            f.productLicense = System.Text.Encoding.ASCII.GetString(enc, off, 60);
            off += 60;

            f.standbyPower = DecodeU16Le(enc, ref off);
            f.maxPower = DecodeU16Le(enc, ref off);

            return f;
        }

        MapleBusDecodedFrame.MapleAddress DecodeAddress(byte raw)
        {
            return new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = (byte)(raw >> 6),
                mainPeripheral = (raw & (1 << 5)) != 0,
                subPeripheral5 = (raw & (1 << 4)) != 0,
                subPeripheral4 = (raw & (1 << 3)) != 0,
                subPeripheral3 = (raw & (1 << 2)) != 0,
                subPeripheral2 = (raw & (1 << 1)) != 0,
                subPeripheral1 = (raw & (1 << 0)) != 0,
            };
        }

        void DecodeHeader(out sbyte responseCode, out MapleBusDecodedFrame.MapleAddress recipient, out MapleBusDecodedFrame.MapleAddress sender, out byte addwords, byte[] enc)
        {
            var responseCode32 = (int)enc[0];
            if (responseCode32 >= 128)
                responseCode32 = responseCode32 - 256;

            responseCode = (sbyte)responseCode32;

            recipient = DecodeAddress(enc[1]);
            sender = DecodeAddress(enc[2]);
            addwords = enc[3];
        }

        MapleBusDecodedFrame DecodeMapleBusDataTransferFrame(byte[] data)
        {
            //MapleBusDataTransferFrame

            if (data.Length % 4 != 0)
                throw new MapleCodecException("Malformed payload: " + nameof(MapleBusDataTransferFrame) + " payload length was " + (data.Length - HeaderLength) + " which is not wordaligned");

            var payload = new byte[data.Length - HeaderLength];
            Array.Copy(data, HeaderLength, payload, 0, payload.Length);

            return new MapleBusDataTransferFrame()
            {
                data = payload,
            };
        }

        MapleBusDecodedFrame DecodeCommandAcknowledgeFrame(byte[] data)
        {
            if (data.Length > HeaderLength)
                throw new MapleCodecException("Response had unexpected additional data: " + Misc.ToHex(data));

            return new MapleBusCommandAcknowledgeFrame();
        }

        MapleBusDecodedFrame DecodeMapleBusCommandNeedsToBeSentAgainFrame(byte[] data)
        {
            if (data.Length > HeaderLength)
                throw new MapleCodecException("Response had unexpected additional data: " + Misc.ToHex(data));

            return new MapleBusCommandNeedsToBeSentAgainFrame();
        }

        public MapleBusDecodedFrame Decode(MapleBusRawPacket enc)
        {
            if (enc == null)
                throw new ArgumentNullException(nameof(enc));

            if (enc.data.Length < 4)
                throw new MapleCodecException("Data was too short, only " + enc.data.Length + " bytes were received");

            var checksum = CalcChecksum(enc.data);
            if (checksum != enc.checksum)
                throw new MapleCodecException("Checksum did not match, calculated 0x" + checksum.ToString("X02") + ", but got 0x" + enc.checksum.ToString("X02"));

            if ( enc.data.Length % 4 != 0)
                throw new MapleCodecException("Data was was not aligned on words, got " + enc.data.Length + " bytes");

            byte additionalWords;
            sbyte requestCode;
            MapleBusDecodedFrame.MapleAddress recipient;
            MapleBusDecodedFrame.MapleAddress sender;
            DecodeHeader(out requestCode, out recipient, out sender, out additionalWords, enc.data);

            var actualAdditionalWords = (enc.data.Length - 4) / 4;
            if (additionalWords != actualAdditionalWords)
                throw new MapleCodecException("Data was misformatted, claimed to have " + additionalWords + " words of payload, but actually had " + actualAdditionalWords + " words");

            MapleBusDecodedFrame f = null;
            if (decode.ContainsKey(requestCode))
            {
                f = decode[requestCode](enc.data);
            }
            else
            {
                var payload = new byte[enc.data.Length - 4];
                Array.Copy(enc.data, 4, payload, 0, payload.Length);
                f = new MapleBusUnknownFrame()
                {
                    code = requestCode,
                    data = payload,
                };
            }

            f.recipient = recipient;
            f.sender = sender;

            return f;
        }

        #endregion

        static byte CalcChecksum(byte[] data)
        {
            byte checksum = 0;
            foreach (var b in data)
                checksum ^= b;
            return checksum;
        }
    }
}
