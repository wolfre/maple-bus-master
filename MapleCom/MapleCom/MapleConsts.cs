﻿using System;
namespace MapleCom
{
    public static class MapleConsts
    {
        /// <summary>
        /// http://mc.pp.se/dc/maplebus.html#funcc
        /// </summary>
        public static class Function
        {
            public const UInt32 Controller = 0x001;
            public const UInt32 MemoryCard = 0x002;
            public const UInt32 Lcd = 0x004;
            public const UInt32 Clock = 0x008;
            public const UInt32 Microphone = 0x010;
            public const UInt32 ARGun = 0x020;
            public const UInt32 Keyboard = 0x040;
            public const UInt32 LightGun = 0x080;
            public const UInt32 PuruPuruPack = 0x100;
            public const UInt32 Mouse = 0x200;
        }

        /// <summary>
        /// http://mc.pp.se/dc/maplebus.html#cmds
        /// </summary>
        public static class Command
        {
            /// <summary>Requests <see cref="DeviceInfo"/></summary>
            public const sbyte RequestDeviceInfo = 1;
            /// <summary>Resonse to <see cref="RequestDeviceInfo"/></summary>
            public const sbyte DeviceInfo = 5;

            /// <summary>Requests <see cref="ExtendedDeviceInfo"/></summary>
            public const sbyte RequestExtendedDeviceInfo = 2;
            /// <summary>Resonse to <see cref="RequestExtendedDeviceInfo"/></summary>
            public const sbyte ExtendedDeviceInfo = 6;

            /// <summary>
            /// To get a condition, e.g. button state, will be answered with <see cref="DataTransfer"/>
            /// </summary>
            public const sbyte GetCondition = 9;
            /// <summary>
            /// ???, will be answered with <see cref="DataTransfer"/>
            /// </summary>
            public const sbyte GetMemoryInformation = 10;
            /// <summary>
            /// Reads a block of memory, will be answered with <see cref="DataTransfer"/>
            /// </summary>
            public const sbyte BlockRead = 11;

            /// <summary>
            /// Reads a block of memory, will be answered with <see cref="CommandAcknowledge"/>
            /// </summary>
            public const sbyte BlockWrite = 12;

            /// <summary>
            /// Finished a block, written in 4 phases
            /// https://github.com/requeijaum/arduino-maple/blob/616e242b3fbcc9e08303df0bcbe8bb416c3d3727/maple.py#L41
            /// </summary>
            public const sbyte BlockWriteFinish = 13;

            /// <summary>Ack</summary>
            public const sbyte CommandAcknowledge = 7;
            /// <summary>Contains the requested data</summary>
            public const sbyte DataTransfer = 8;

            /// <summary>An error response</summary>
            public const sbyte FunctionCodeUnsupported = -2;

            /// <summary>An error response</summary>
            public const sbyte UnknownCommand = -3;

            /// <summary>
            /// Command was incomplete / wrongly formattted.
            /// E.g. the function was missing from <see cref="GetCondition"/>
            /// Also if flash writes come to fast
            /// </summary>
            public const sbyte CommandNeedsToBeSentAgain  = -4;
        }

        /// <summary>
        /// http://mc.pp.se/dc/maplebus.html#cmds see section on error bitfield
        /// </summary>
        public static class ErrorBits
        {
            public const byte InvalidPartitionNumber = 0x01;
            public const byte PhaseError = 0x02;
            public const byte InvalidBlockNumber = 0x04;
            public const byte WriteError = 0x08;
            /// <summary>Use 192 bytes for LCD writes, 128 bytes for flash writes</summary>
            public const byte InvalidLength = 0x10;
            public const byte BadCrc = 0x20;
        }
    }
}
