﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace MapleCom.Tests
{
    [TestFixture]
    public class MapleBusStateMachineTest
    {
        ILog log;
        MapleBusStateMachine cut;

        // http://mc.pp.se/dc/maplewire.html

        [SetUp]
        public void Init()
        {
            log = new ConsoleLog();
            cut = new MapleBusStateMachine();
        }

        static Sample S(bool d1, bool d5)
        {
            return new Sample() { d1 = d1, d5 = d5 };
        }

        Sample[] LeadIn1
        {
            get
            {
                return new Sample[]
                {
                    S(true, true),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(true, true),
                    S(true, false),
                };
            }
        }

        Sample[] LeadIn2
        {
            get
            {
                return new Sample[]
                {
                    S(true, true),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(false, false),

                    S(false, true),
                    S(true, true),

                    // Missing the first idle transition compared to lead in 1
                };
            }
        }

        Sample[] LeadOut
        {
            get
            {
                return new Sample[]
                {
                    S(true, false),
                    S(true, true),

                    S(true, false),
                    S(false, false),

                    S(true, false),
                    S(false, false),

                    S(true, false),
                    S(true, true),
                };
            }
        }

        Sample[] DoubleBit(bool firstBit, bool secondBit)
        {
            return new Sample[]
            {
                S(true, firstBit),
                S(false, firstBit),

                S(false, true),
                S(secondBit, true),
                S(secondBit, false),

                S(true, false),
            };
        }

        void Run(params Sample[][] parts)
        {
            foreach (var p in parts)
                foreach (var s in p)
                    cut.NextSampleBits(s.d1, s.d5);
        }

        [Test]
        public void OneByte([Values(1,2)] int leadin, [Values(true, false)] bool leadInMissingFirstIdleSample, [Values(true, false)] bool bit1, [Values(true, false)] bool bit2)
        {
            var leadIn = leadin == 1 ? LeadIn1 : LeadIn2;

            // This is sometimes the case if the very first non-idle block sampled is exactly on falling edge
            if (leadInMissingFirstIdleSample)
                leadIn = leadIn.Skip(1).ToArray();

            Run(
                leadIn,
                DoubleBit(bit1, bit2), DoubleBit(true, true), DoubleBit(false, true), DoubleBit(true, false), 
                LeadOut);

            var dec = cut.GetData();

            Assert.That(dec.Length, Is.EqualTo(1));

            byte data = 0x36;

            if (bit1)
                data |= 0x80;
            if (bit2)
                data |= 0x40;

            Assert.That(dec[0], Is.EqualTo(data));
        }


        static readonly TestCaseData[] examples = Examples.RecordedSamplesCsv.All.Select(n => new TestCaseData(n)).ToArray();

        [Test]
        [TestCaseSource(nameof(examples))]
        public void Example(string name)
        {
            foreach(var next in SamplesFromCsvResource(name))
                cut.NextSampleBits(next.d1, next.d5);

            var dec = cut.GetData();

            Assert.That(dec.Length, Is.GreaterThanOrEqualTo(4));
        }

        [Test]
        public void Benchmark()
        {
            var anyCsvSamples = Examples.RecordedSamplesCsv.All.First();
            var samples = SamplesFromCsvResource(anyCsvSamples);
            const int Iter = 10;

            var start = DateTime.Now;

            for (int i = 0; i < Iter; ++i)
            {
                var dec = new MapleBusStateMachine();
                foreach (var s in samples)
                    dec.NextSampleBits(s.d1, s.d5);

                var data = dec.GetData();
                if (data.Length < 10)
                    throw new AssertionException("Data too short");
            }

            var time = DateTime.Now - start;

            long samplesPerSeconds = Iter * samples.Length;
            samplesPerSeconds *= TimeSpan.TicksPerSecond;
            samplesPerSeconds /= time.Ticks;

            Console.WriteLine("Samples per second: " + samplesPerSeconds);
            Console.WriteLine("Full decoding of " + samples.Length + " samples took " + TimeSpan.FromTicks(time.Ticks / Iter).TotalMilliseconds + " ms");

            Assert.That(samplesPerSeconds, Is.GreaterThanOrEqualTo(1000000));
        }

        static Sample[] SamplesFromCsvResource(string csvDataName)
        {
            var csvData = Examples.RecordedSamplesCsv.GetByName(csvDataName);
            var reader = new CsvSampleReader(csvData);

            var data = new List<Sample>();
            while (reader.EndOfStream == false)
            {
                var next = reader.Next();
                if (next != null)
                    data.Add(next);
            }

            return data.ToArray();
        }
    }
}
