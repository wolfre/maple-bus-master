﻿using System;
using System.IO;
using NUnit.Framework;

namespace MapleCom.Tests
{
    [TestFixture]
    public class SamplerCodecTest
    {
        ILog log;

        [SetUp]
        public void Init()
        {
            log = new ConsoleLog();
        }

        [Test]
        public void FromCompactLine()
        {
            var cut = new SamplerCodec(log);

            var p = cut.FromCompactLine("s=9,D1=0x128,D5=0x238");

            Assert.That(p.samples, Is.EqualTo(9));
            AssertEqual(p.data1, new byte[] { 0x12, 0x80 }, "data1");
            AssertEqual(p.data5, new byte[] { 0x23, 0x80 }, "data5");
        }

        void AssertEqual(byte[] actual, byte[] expected, string what = null)
        {
            Assert.That(BitConverter.ToString(actual), Is.EqualTo(BitConverter.ToString(expected)), what == null ? "data" : what);
        }

        const string TestLine = "s=4080,D1=0x000000000003e003e007e007e0ffe007e007c007c007c00fc00fc00fc00f800f800f800f83ff801f801f801f83ff801f001f003f003f003f003f003f003e003e007e007e007e007e007c007c1ffffc1f83ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc000000003ffe0003fe0001ff0001ff00ffff8000ff80007f80007fc0003fc0003fe0001fe0001ff0001ff0000ff0000ff80007f80007fc0003fc0003fe0001fe01ffff0001ff0000ff0000ff80007f80007fc0003fc0003fe0001fe0001ff0000ff0000ff8000ff80007fc0007fc0003fc0003fe0001fe0001ff0000ff0000ff80ffff807fffc07fffc03fffe03fffe01ffff01ffff00ffff0000ff80007f80007fc0003fc0003fe0001fe0001fe0001ff0000ff0000ff80007f80007fc0003fc0003fe0003fe0001ffc07c03c03c03ffe3e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff,D5=0xf0fc1f83e0ff83e007e007e007e007e007e007c007c00fc1ffc00fc00fc00f800f83ff801f801f801f801f801f801f801f003f003f003f003f003f003e003e007e007e007e007e1ffc1ffc007c1f8000001ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc7c7c3c3ffe03fe0001fe0001ff00ffff0000ff80ffff80007fc07fffc0003fe0003fe0001fe0001ff0000ff0000ff80ffff80007fc0003fc0003fe0001fe0001fe0001ff0000ff0000ff80007f80007fc0003fc0003fe0003fe0001ff0001ff0000ff8000ff80007f80007fc0003fc0003fe0001fe0001ff0000ff0000ff80ffff807fff807fffc03fffc03fffe03fffe01ffff01ffff00ffff80007f80007fc0007fc03fffe0003fe0001ff0001ff01ffff0000ff80007f80007fc07fffc0003fe0003fe0001fe0001f1ff0ff00f0fe00001fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";

        [Test]
        public void FromSamples()
        {
            var cut = new SamplerCodec(log);

            var p = cut.FromCompactLine(TestLine);

            var mapleFrame = cut.FromSamples(p);
            Console.WriteLine("Data[" + mapleFrame.data.Length + "]=" + Misc.ToHex(mapleFrame.data) + " checksum=" + Misc.ToHex(mapleFrame.checksum));

            AssertEqual(mapleFrame.data, new byte[] { 0x09, 0x20, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01 });
            Assert.That(mapleFrame.checksum, Is.EqualTo(0x29));
        }


        [Test]
        public void ToSampleRequestInput()
        {
            var cut = new SamplerCodec(log);

            var input = cut.ToSampleRequestInput(new MapleBusRawPacket()
            {
                data = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x10, 0x20, 0x30, 0x40 },
                checksum = 0x42,
            });

            Assert.That(input, Is.EqualTo("0x040302014030201042"));
        }

        [Test]
        public void FromOutput()
        {
            var samplerOutput = new MemoryStream(new byte[] 
            { 
                0x00, 0x00, 0x00, 0x11,

                0x10, 0x20, 0x30,
                0x01, 0x02, 0x03,
            });

            var cut = new SamplerCodec(log);

            var raw = cut.FromCommandOutput(samplerOutput);

            Assert.That(raw.samples, Is.EqualTo(0x11));
            Assert.That(Misc.ToHex(raw.data1), Is.EqualTo("0x102030"));
            Assert.That(Misc.ToHex(raw.data5), Is.EqualTo("0x010203"));
        }

        [Test]
        public void DataToInput()
        {
            var cut = new SamplerCodec(log);

            var samplerInput = new MemoryStream();

            cut.ToCommandDataInput(samplerInput, new MapleBusRawPacket()
            {
                data = new byte[] { 0x01, 0x02, 0x03, 0x04 },
                checksum = 0xff
            });

            Assert.That(Misc.ToHex(samplerInput.ToArray()), Is.EqualTo("0x000504030201FF"));
        }
    }
}
