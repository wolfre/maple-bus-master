﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MapleCom.Tests.Examples
{
    static class RecordedSamplesCsv
    {
        static Stream GetResource(string name)
        {
            var ass = typeof(RecordedSamplesCsv).Assembly;
            var s = ass.GetManifestResourceStream(name);
            if (s != null)
                return s;

            var fuzzy = ass.GetManifestResourceNames().FirstOrDefault(n => n.EndsWith(name));
            if (fuzzy != null)
                return ass.GetManifestResourceStream(fuzzy);

            throw new Exception("Couldn't find resource stream '" + name + "'");
        }

        public static string[] All
        {
            get { return new string[] { "samples-long-response-01.csv" }; }
        }

        public static Stream GetByName(string name)
        {
            return GetResource(name);
        }

    }
}
