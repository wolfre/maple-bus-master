﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MapleCom.Tests
{
    class Sample
    {
        public bool d1;
        public bool d5;
    }

    class CsvSampleReader
    {
        StreamReader rdr;

        public CsvSampleReader(Stream strm)
        {
            rdr = new StreamReader(strm);
            var l = rdr.ReadLine().Trim('\n', '\r');
            if (l != "Sample,D1,D5")
                throw new NotSupportedException("Don't know how to decode CSV with format: " + l);
        }

        static Dictionary<string, bool> bitValues = new Dictionary<string, bool>()
        {
            { "0", false },
            { "1", true },
            { "2", false },
            { "3", true },
        };

        public Sample Next()
        {
            if (EndOfStream)
                return null;

            var l = rdr.ReadLine().Trim('\n', '\r');
            Func<string, Exception> up = m => new ArgumentException("Malformed line '" + l + "': " + m);

            var p = l.Split(',');
            if (p.Length != 3)
                throw up("parts");

            var n = new Sample();

            if (bitValues.ContainsKey(p[1]) == false)
                throw up("not a valid bit value for D1");

            n.d1 = bitValues[p[1]];

            if (bitValues.ContainsKey(p[2]) == false)
                throw up("not a valid bit value for D5");

            n.d5 = bitValues[p[2]];

            return n;
        }

        public bool EndOfStream { get { return rdr.EndOfStream; } }
    }
}
