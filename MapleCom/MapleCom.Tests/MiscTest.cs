﻿using System;
using NUnit.Framework;

namespace MapleCom.Tests
{
    [TestFixture]
    public class MiscTest
    {
        static readonly TestCaseData[] hexTests = new TestCaseData[]
        {
            new TestCaseData("0x12", new byte[]{ 0x12 }),
            new TestCaseData("12", new byte[]{ 0x12 }),
            new TestCaseData("0x123", new byte[]{ 0x12, 0x30 }),
        };

        [Test]
        [TestCaseSource(nameof(hexTests))]
        public void ByteArrayFromHex(string hex, byte[] data)
        {
            var p = Misc.ByteArrayFromHex(hex);
            Assert.That(BitConverter.ToString(p), Is.EqualTo(BitConverter.ToString(data)));
        }
    }
}
