﻿using System;
using BusMaster.Actions;
using NUnit.Framework;

namespace MapleCom.Tests
{
    [TestFixture]
    public class VmuActionTest
    {
        class TestVmuAction : VmuAction
        {
            public static ushort AccessVmuBlockSize { get { return VmuBlockSize; } }

            public TestVmuAction(ILog log) : base(log)
            {
            }

            public bool[] AccessGetVmuBlockDiff(byte[] vmu1, byte[] vmu2)
            {
                return GetVmuBlockDiff(vmu1, vmu2);
            }
        }

        ILog log;
        TestVmuAction cut;

        [SetUp]
        public void Init()
        {
            log = new ConsoleLog();
            cut = new TestVmuAction(log);
        }

        [Test]
        public void GetVmuBlockDiff()
        {
            var vmu1 = new byte[TestVmuAction.AccessVmuBlockSize * 3];
            var vmu2 = new byte[TestVmuAction.AccessVmuBlockSize * 3];

            vmu2[TestVmuAction.AccessVmuBlockSize + 3] = 42;

            var ok = cut.AccessGetVmuBlockDiff(vmu1, vmu2);

            Assert.That(ok.Length, Is.EqualTo(3));
            Assert.That(ok[0], Is.True);
            Assert.That(ok[1], Is.False);
            Assert.That(ok[2], Is.True);
        }
    }
}
