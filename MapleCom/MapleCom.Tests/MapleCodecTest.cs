﻿using System;
using NUnit.Framework;

namespace MapleCom.Tests
{
    [TestFixture]
    public class MapleCodecTest
    {
        ILog log;
        MapleCodec cut;
        [SetUp]
        public void Init()
        {
            log = new ConsoleLog();
            cut = new MapleCodec(log);
        }

        [Test]
        public void EncodeUnknown()
        {
            var enc = cut.Encode(new MapleBusUnknownFrame()
            {
                code = 0x42,
                recipient = new MapleBusDecodedFrame.MapleAddress()
                {
                    portNumber = 2,
                    mainPeripheral = true,
                    subPeripheral1 = true,
                },
                sender = new MapleBusDecodedFrame.MapleAddress()
                {
                    portNumber = 0,
                },
                data = new byte[] { 0xf1, 0xf2, 0xf2, 0xf4 },
            });

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[] 
            { 
                0x42, (byte)(2<<6 | 1<<5 | 1<<0), 0x00, 0x01,
                0xf1, 0xf2, 0xf2, 0xf4 
            })));
            Assert.That(enc.checksum, Is.EqualTo(231));
        }

        void AddressX8181(MapleBusDecodedFrame f)
        {
            f.recipient = new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 2,
                subPeripheral1 = true,
            };
            f.sender = new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 2,
                subPeripheral1 = true,
            };
        }

        [Test]
        public void EncodeMapleBusRequestDeviceInfoFrame()
        {
            var f = new MapleBusRequestDeviceInfoFrame();
            AddressX8181(f);

            var enc = cut.Encode(f);

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[]
            {
                (byte)MapleConsts.Command.RequestDeviceInfo, 0x81, 0x81, 0x00,
            })));
            Assert.That(enc.checksum, Is.EqualTo(0x01));
        }

        [Test]
        public void EncodeMapleBusGetConditionFrame()
        {
            var f = new MapleBusGetConditionFrame()
            {
                function = 0x11223344,
            };
            AddressX8181(f);

            var enc = cut.Encode(f);

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[]
            {
                (byte)MapleConsts.Command.GetCondition, 0x81, 0x81, 0x01,
                0x11, 0x22, 0x33, 0x44
            })));
            Assert.That(enc.checksum, Is.EqualTo(0x4c));
        }

        [Test]
        public void EncodeMapleBusBlockReadFrame()
        {
            var f = new MapleBusBlockReadFrame()
            {
                function = 0x11111111,
                partition = 0x01,
                phase = 0x02,
                block = 0x5678,
            };

            AddressX8181(f);

            var enc = cut.Encode(f);

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[]
            {
                (byte)MapleConsts.Command.BlockRead, 0x81, 0x81, 0x02,
                0x11, 0x11, 0x11, 0x11,
                0x01, 0x02, 0x56, 0x78,
            })));
            Assert.That(enc.checksum, Is.EqualTo(0x24));
        }

        [Test]
        public void EncodeMapleBusBlockWriteFinishFrameFrame()
        {
            var f = new MapleBusBlockWriteFinishFrame()
            {
                function = 0x11111111,
                partition = 0x44,
                phase = MapleBusBlockWriteFinishFrame.FinishPhase,
                block = 0x5566,
            };

            AddressX8181(f);

            var enc = cut.Encode(f);

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[]
            {
                (byte)MapleConsts.Command.BlockWriteFinish, 0x81, 0x81, 0x02,
                0x11, 0x11, 0x11, 0x11,
                0x44, 0x04, 0x55, 0x66,
            })));
            Assert.That(enc.checksum, Is.EqualTo(0x7c));
        }

        [Test]
        public void EncodeMapleBusBlockWriteFrame()
        {
            var f = new MapleBusBlockWriteFrame()
            {
                function = 0x11111111,
                partition = 0x01,
                phase = 0x02,
                block = 0x5678,
                data = new byte[]
                {
                    0x10, 0x20, 0x30, 0x40
                },
            };

            AddressX8181(f);

            var enc = cut.Encode(f);

            Assert.That(Misc.ToHex(enc.data), Is.EqualTo(Misc.ToHex(new byte[]
            {
                (byte)MapleConsts.Command.BlockWrite, 0x81, 0x81, 0x03,
                0x11, 0x11, 0x11, 0x11,
                0x01, 0x02, 0x56, 0x78,
                0x10, 0x20, 0x30, 0x40
            })));
            Assert.That(enc.checksum, Is.EqualTo(0x62));
        }

        [Test]
        public void DecodeChecksumError()
        {
            var exc = Assert.Throws<MapleCodecException>(() =>
            {
                cut.Decode(new MapleBusRawPacket()
                {
                    data = new byte[] { 0x10, 0x80, 0x08, 0x01 },
                    checksum = 0x66,
                });
            });

            Assert.That(exc.Message, Contains.Substring("0x66").And.ContainsSubstring("0x99"));
        }

        [Test]
        public void DecodeMapleBusDataTransferFrame()
        {
            var dec = cut.Decode(new MapleBusRawPacket()
            {
                data = new byte[]
                {
                    (byte)MapleConsts.Command.DataTransfer, 0x80, 0x08, 0x01,
                    0xaa, 0xbb, 0xcc, 0xdd,
                },
                checksum = 0x81,
            });

            var d = (MapleBusDataTransferFrame)dec;
            Assert.That(Misc.ToHex(d.data), Is.EqualTo("0xAABBCCDD"));
        }

        [Test]
        public void DecodeMapleBusCommandAcknowledgeFrame()
        {
            var dec = cut.Decode(new MapleBusRawPacket()
            {
                data = new byte[]
                {
                    (byte)MapleConsts.Command.CommandAcknowledge, 0x80, 0x08, 0x00
                },
                checksum = 0x8f,
            });

            Assert.That(dec, Is.InstanceOf<MapleBusCommandAcknowledgeFrame>());
        }

        [Test]
        public void DecodeMapleBusCommandNeedsToBeSentAgainFrame()
        {
            var dec = cut.Decode(new MapleBusRawPacket()
            {
                data = new byte[]
                {
                    (byte)(256 + MapleConsts.Command.CommandNeedsToBeSentAgain), 0x80, 0x08, 0x00
                },
                checksum = 0x74,
            });

            Assert.That(dec, Is.InstanceOf<MapleBusCommandNeedsToBeSentAgainFrame>());
        }

        [Test]
        public void DecodeUnknown()
        {
            var dec = cut.Decode(new MapleBusRawPacket()
            {
                data = new byte[] 
                { 
                    0x33, 0x80, 0x08, 0x01,
                    0xaa, 0xbb, 0xcc, 0xdd,
                },
                checksum = 0xba,
            });

            var logger = new MapleFrameDumper(log);
            logger.Dump(dec);

            var unk = (MapleBusUnknownFrame)dec;
            Assert.That(unk.code, Is.EqualTo(0x33));

            AssertEqual(unk.recipient, new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 2,
            }, "recipient");

            AssertEqual(unk.sender, new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 0,
                subPeripheral4 = true,
            }, "sender");

            Assert.That(unk.recipient.portNumber, Is.EqualTo(2));

            // TODO check stuff
            Assert.That(Misc.ToHex(unk.data), Is.EqualTo("0xAABBCCDD"));
        }

        void AssertEqual(MapleBusDecodedFrame.MapleAddress actual, MapleBusDecodedFrame.MapleAddress expected, string what)
        {
            Assert.That(actual.portNumber, Is.EqualTo(expected.portNumber), what + ".portNumber");
            Assert.That(actual.mainPeripheral, Is.EqualTo(expected.mainPeripheral), what + ".mainPeripheral");
            Assert.That(actual.subPeripheral1, Is.EqualTo(expected.subPeripheral1), what + ".subPeripheral1");
            Assert.That(actual.subPeripheral2, Is.EqualTo(expected.subPeripheral2), what + ".subPeripheral2");
            Assert.That(actual.subPeripheral3, Is.EqualTo(expected.subPeripheral3), what + ".subPeripheral3");
            Assert.That(actual.subPeripheral4, Is.EqualTo(expected.subPeripheral4), what + ".subPeripheral4");
            Assert.That(actual.subPeripheral5, Is.EqualTo(expected.subPeripheral5), what + ".subPeripheral5");
        }

        const string DevInfoHkt7700 = "0x0500221C00000001000F06FE0000000000000000FF00447265616D6361737420436F6E74726F6C6C65722020202020202020202050726F6475636564204279206F7220556E646572204C6963656E73652046726F6D205345474120454E5445525052495345532C4C54442E2020202020AE01F401";
        const byte DevInfoHkt7700Checksum = 0x1b;

        [Test]
        public void DecodeMapleBusDeviceInfoFrame()
        {
            var dec = cut.Decode(new MapleBusRawPacket()
            {
                data = Misc.ByteArrayFromHex(DevInfoHkt7700),
                checksum = DevInfoHkt7700Checksum,
            });

            var logger = new MapleFrameDumper(log);
            logger.Dump(dec);

            var unk = (MapleBusDeviceInfoFrame)dec;
            Assert.That(unk.supportedFunctions.Length, Is.EqualTo(1));
            Assert.That(unk.supportedFunctions[0], Is.EqualTo(1));
            Assert.That(unk.functionData.Length, Is.EqualTo(1));
            var fkt0 = (MapleBusDeviceInfoFrame.FunctionDataUnknown)unk.functionData[0];
            Assert.That(fkt0.data, Is.EqualTo(984830));
            Assert.That(fkt0.function, Is.EqualTo(1));
            Assert.That(unk.areaCode, Is.EqualTo(255));
            Assert.That(unk.connectorDirection, Is.EqualTo(0));
            Assert.That(unk.productName, Is.EqualTo("Dreamcast Controller          "));
            Assert.That(unk.productLicense, Is.EqualTo("Produced By or Under License From SEGA ENTERPRISES,LTD.     "));
            Assert.That(unk.standbyPower, Is.EqualTo(430));
            Assert.That(unk.maxPower, Is.EqualTo(500));
        }
    }
}
