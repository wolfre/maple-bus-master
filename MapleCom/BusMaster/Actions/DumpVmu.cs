﻿using System;
using System.IO;
using System.Linq;
using MapleCom;

namespace BusMaster.Actions
{
    public class DumpVmuArgs : VmuArgs
    {
        [Binah.CommandLine("--write-lcd", "Writes a test image to the LCD")]
        public bool writeLcd = true;

        [Binah.CommandLine("--retry-flash-read", "Sometimes the flash is not fast enough to read, retry if a read fails")]
        public bool retryFlashReads = true;

        // NOTE 200 ms comes from some try-n-error with an actual VMU
        [Binah.CommandLine("--min-read-delay", "VMUs tend to bug-out if read too fast, wait at least this time (in ms) between reads.")]
        public UInt16 minReadDelayMs = 200;

        public TimeSpan MinReadDelay { get { return TimeSpan.FromMilliseconds(minReadDelayMs); } }
    }

    public class DumpVmu : VmuAction, Binah.IAction<DumpVmuArgs>
    {
        public DumpVmu(ILog log) : base(log)
        {
        }

        public string Switch { get { return "dump-vmu"; } }

        public string Info { get { return "Dumps a VMU"; } }

        static readonly byte[] rockingPicture = new byte[]
        {
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x22,
            0x00, 0x00, 0x00, 0x01, 0x80, 0x22, 0x00, 0x00,
            0x00, 0x03, 0x40, 0x22, 0x00, 0x00, 0x00, 0x02,
            0x60, 0x22, 0x00, 0x00, 0x00, 0x02, 0x20, 0xe2,
            0x00, 0x00, 0x00, 0x02, 0x21, 0x22, 0x00, 0x00,
            0x00, 0x03, 0x3f, 0x22, 0x00, 0x00, 0x00, 0x01,
            0x33, 0x22, 0x00, 0x00, 0x00, 0x01, 0x31, 0x22,
            0x00, 0x00, 0x00, 0x01, 0x11, 0x22, 0x00, 0x00,
            0x00, 0x01, 0x11, 0x22, 0x00, 0x00, 0x00, 0x01,
            0x11, 0x22, 0x00, 0x00, 0x00, 0x01, 0x80, 0x03,
            0x00, 0x00, 0x00, 0x01, 0x80, 0x01, 0x00, 0x00,
            0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00,
            0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01,
            0x00, 0x00, 0x00, 0x00, 0x80, 0x03, 0x00, 0x00,
            0x00, 0x00, 0xc0, 0x02, 0x00, 0x00, 0x00, 0x00,
            0x40, 0x06, 0x00, 0x00, 0x00, 0x00, 0x40, 0x04,
            0x00, 0x00, 0x00, 0x00, 0x60, 0x0c, 0x00, 0x00,
            0x00, 0x00, 0x20, 0x08, 0x00, 0x00, 0x00, 0x00,
            0x20, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xf8,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        };

        void WriteImageToLcd(IMapleBus bus, DeviceTree tree)
        {
            var lcd = FindFunction(tree, MapleConsts.Function.Lcd);
            if (lcd == null)
            {
                log.That("No LCD found");
                return;
            }

            log.That("Found LCD at " + dumper.ToString(lcd) + ", will now write something to it");

            var resp = bus.DoBusTransaction(new MapleBusBlockWriteFrame()
            {
                sender = AddressConsole,
                recipient = lcd,
                function = MapleConsts.Function.Lcd,
                partition = 0,
                phase = 0,
                block = 0,
                data = rockingPicture,
            });

            dumper.Dump(resp);
        }

        public void Run(DumpVmuArgs args, string[] unconsumend)
        {
            using (var unstableBus = new Stm32UsbSampler(log, args.sampler))
            using (var bus = new UnstableMapleBus(log, unstableBus, args.maxBusRetries))
            {
                var tree = EnumerateDevices(bus);
                PrintTree(tree);

                if (args.writeLcd)
                    WriteImageToLcd(bus, tree);

                var vmu = FindFunction(tree, MapleConsts.Function.MemoryCard);
                if (vmu == null)
                    throw new System.IO.IOException("Unable to find a VMU in controller");

                log.That("Found memory card at " + dumper.ToString(vmu));
                Dump(args, bus, vmu);
            }
        }

        void Dump(DumpVmuArgs args, IMapleBus bus, MapleBusDecodedFrame.MapleAddress vmu)
        {
            var vmuMem = new byte[VmuBlockSize * args.vmuBlocks];

            var lastRead = DateTime.Now - args.MinReadDelay;

            var result = VmuMemory(vmuMem, args.retryFlashReads ? 1 : 0, "dumping", block => 
            {
                var delay = args.MinReadDelay - (DateTime.Now - lastRead);
                if (delay > TimeSpan.Zero)
                    System.Threading.Thread.Sleep(delay);

                lastRead = DateTime.Now;
                return ReadOneBlock(bus, vmu, block, vmuMem); 
            });

            using (var file = args.vmu.Open(FileMode.Create, FileAccess.Write, FileShare.Read))
                file.Write(vmuMem, 0, vmuMem.Length);

            log.That("Written VMU data to '" + args.vmu.FullName + "', md5: " + Md5Hex(vmuMem));

            if (result != VmuMemoryResult.Ok)
            {
                log.That("Dumping the VMU failed");
            }
            else
            {
                log.That("Dumping the VMU was successfull");
            }
        }

        VmuMemoryResult ReadOneBlock(IMapleBus bus, MapleBusDecodedFrame.MapleAddress vmu, UInt16 requestedBlock, byte[] vmuData)
        {
            try
            {
                var resp = bus.DoBusTransaction(new MapleBusBlockReadFrame()
                {
                    sender = AddressConsole,
                    recipient = vmu,
                    function = MapleConsts.Function.MemoryCard,

                    partition = 0,

                    block = requestedBlock,
                    phase = 0,
                });

                const int Header = 8;
                const int BlockResponse = VmuBlockSize + Header;

                Func<string, Exception> up = msg =>
                {
                    dumper.Dump(resp);
                    return new IOException("Block " + requestedBlock + ": " + msg);
                };

                var data = resp as MapleBusDataTransferFrame;
                if (data == null)
                    throw up("Unexpected response");

                if (data.data.Length != BlockResponse)
                    throw up("Unexpected amount of data, expected " + BlockResponse + " but got " + data.data.Length);

                uint function = (uint)data.data[0] << 24 | (uint)data.data[1] << 16 | (uint)data.data[2] << 8 | (uint)data.data[3];
                if (function != MapleConsts.Function.MemoryCard)
                    throw up("Function was " + function);

                var rBlock = data.data[6] << 8 | data.data[7];
                if (rBlock != requestedBlock)
                {
                    log.That("VMU BUG: returned wrong block, expected " + requestedBlock + " but got " + rBlock);
                    return VmuMemoryResult.VmuFailure;
                }

                Array.Copy(data.data, Header, vmuData, requestedBlock * VmuBlockSize, VmuBlockSize);
                return VmuMemoryResult.Ok;
            }
            catch (Exception x)
            {
                log.That(x);
                return VmuMemoryResult.OtherError;
            }
        }



    }
}
