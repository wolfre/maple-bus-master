﻿using System;
using System.IO;
using System.Linq;
using MapleCom;

namespace BusMaster.Actions
{
    public class WriteVmuArgs : VmuArgs
    {
        [Binah.CommandLine("--max-write-retries", "Sometimes the flash fails to write the data, retry if a write fails")]
        public UInt16 maxWriteRetries = 2;

        [Binah.CommandLine("--write-delay", "Writing a flash (phase) takes some time. Wait this ammount of ms between writes.")]
        public uint writeDelayMs = 150;

        [Binah.CommandLine("--vmu-delta-file", "The current memory of the VMU. Using this will only transfere blocks that ar different instead of the whole file.")]
        public FileInfo vmuDeltaFile = null;

        public TimeSpan WriteDelay { get { return TimeSpan.FromMilliseconds(writeDelayMs); } }
    }

    public class WriteVmu : VmuAction, Binah.IAction<WriteVmuArgs>
    {
        public WriteVmu(ILog log) : base(log)
        {
        }

        public string Switch { get { return "write-vmu"; } }

        public string Info { get { return "Write a dump back to a VMU"; } }

        public void Run(WriteVmuArgs args, string[] unconsumend)
        {
            using (var unstableBus = new Stm32UsbSampler(log, args.sampler))
            using (var bus = new UnstableMapleBus(log, unstableBus, args.maxBusRetries))
            {
                var tree = EnumerateDevices(bus);
                PrintTree(tree);

                var vmu = FindFunction(tree, MapleConsts.Function.MemoryCard);
                if (vmu == null)
                    throw new System.IO.IOException("Unable to find a VMU in controller");

                log.That("Found memory card at " + dumper.ToString(vmu));
              
                Write(args, bus, vmu);
            }
        }

        void Write(WriteVmuArgs args, IMapleBus bus, MapleBusDecodedFrame.MapleAddress vmu)
        {
            var vmuMem = ReadVmuFile(args.vmu);
            if (vmuMem.Length / VmuBlockSize != args.vmuBlocks)
                throw new ArgumentException("Vmu file '" + args.vmu.FullName + "' did not contain " + args.vmuBlocks + " blocks of " + VmuBlockSize + " bytes each");

            log.That("Will now write data from '" + args.vmu.FullName + "' to VMU, md5: " + Md5Hex(vmuMem));

            var lastWrite = DateTime.Now - args.WriteDelay;

            Action delayTillNextDeadLine = () =>
            {
                var delay = args.WriteDelay - (DateTime.Now - lastWrite);
                if (delay > TimeSpan.Zero)
                    System.Threading.Thread.Sleep(delay);

                lastWrite = DateTime.Now;
            };

            bool[] blockDoneBitmap = GetToDoBlocks(args, vmuMem);
            var result = VmuMemory(vmuMem, blockDoneBitmap, (int)args.maxWriteRetries, "writing", block =>
            {
                var phaseData = new byte[VmuPhaseSize];
                for (byte phase = 0; phase < PhasesPerBlock; ++phase)
                {
                    Array.Copy(vmuMem, (block * VmuBlockSize) + (phase * VmuPhaseSize), phaseData, 0, phaseData.Length);
                    delayTillNextDeadLine();
                    var success = WritePhase(bus, vmu, block, phase, phaseData);
                    if (success != true)
                        return VmuMemoryResult.VmuFailure;
                }

                // NOTE not documented here http://mc.pp.se/dc/maplebus.html
                //  Check the command list, 13 is absent, I found it here
                //  https://github.com/requeijaum/arduino-maple/blob/616e242b3fbcc9e08303df0bcbe8bb416c3d3727/maple.py#L41
                delayTillNextDeadLine();
                var resp = bus.DoBusTransaction(new MapleBusBlockWriteFinishFrame()
                {
                    sender = AddressConsole,
                    recipient = vmu,
                    function = MapleConsts.Function.MemoryCard,
                    partition = 0,
                    phase = MapleBusBlockWriteFinishFrame.FinishPhase,
                    block = block,
                });

                if (resp is MapleBusCommandAcknowledgeFrame)
                    return VmuMemoryResult.Ok;

                log.That("Finishing block " + block + " failed");
                dumper.Dump(resp);

                return VmuMemoryResult.OtherError;
            });

            if (result != VmuMemoryResult.Ok)
            {
                log.That("Writing the VMU failed");
            }
            else
            {
                log.That("Writing the VMU was successfull");
            }
        }

        bool[] GetToDoBlocks(WriteVmuArgs args, byte[] vmuMem)
        {
            var blockDoneBitmap = new bool[args.vmuBlocks];
            if (args.vmuDeltaFile != null)
            {
                var vmuCurrentMem = ReadVmuFile(args.vmuDeltaFile);
                blockDoneBitmap = GetVmuBlockDiff(vmuCurrentMem, vmuMem);
                log.That("Will only write blocks that differ from '" + args.vmuDeltaFile.FullName + "'");
            }

            return blockDoneBitmap;
        }

        byte[] ReadVmuFile(FileInfo vmu)
        {
            if (vmu == null)
                throw new ArgumentException("Need VMU file");

            vmu.Refresh();
            if (vmu.Exists == false)
                throw new FileNotFoundException(vmu.FullName);

            var vmuData = File.ReadAllBytes(vmu.FullName);

            if (vmuData.Length % VmuBlockSize != 0)
                throw new ArgumentException("Vmu file '" + vmu.FullName + "' was not evenly divisible by VMU blocksize of " + VmuBlockSize);

            return vmuData;
        }



        bool WritePhase(IMapleBus bus, MapleBusDecodedFrame.MapleAddress vmu, ushort block, byte phase, byte[] data)
        {
            if (data.Length != VmuPhaseSize)
                throw new ArgumentException("Expected " + VmuPhaseSize + " bytes for phase " + phase + " in block " + block + " but got " + data.Length);
                
            var resp = bus.DoBusTransaction(new MapleBusBlockWriteFrame()
            {
                sender = AddressConsole,
                recipient = vmu,
                function = MapleConsts.Function.MemoryCard,
                partition = 0,
                phase = phase,
                block = block,
                data = data,
            });

            if (resp is MapleBusCommandAcknowledgeFrame)
                return true;

            if (resp is MapleBusCommandNeedsToBeSentAgainFrame)
            {
                log.That("Write error on block " + block + " phase " + phase);
                return false;
            }

            dumper.Dump(resp);
            throw new IOException("Error when writing block " + block + " phase " + phase);
        }
    }
}
