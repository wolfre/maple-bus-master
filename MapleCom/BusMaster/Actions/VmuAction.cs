﻿using System;
using System.IO;
using System.Linq;
using MapleCom;

namespace BusMaster.Actions
{
    public abstract class SamplerArgs
    {
        [Binah.CommandLine("--sampler-exe", "Location of the sampler exe")]
        public FileInfo sampler = new FileInfo("sampler");

        [Binah.CommandLine("--max-bus-retry", "Max bus retries on errors")]
        public int maxBusRetries = 1;
    }

    public abstract class VmuArgs : SamplerArgs
    {
        [Binah.CommandLine("--vmu-file", "The raw, word-swapped dump of the VMU memory")]
        public FileInfo vmu = new FileInfo("vmu-swapped.raw");

        [Binah.CommandLine("--vmu-blocks", "The number of blocks to dump, each block is 512 bytes of data")]
        public UInt16 vmuBlocks = 256;
    }

    public abstract class VmuAction
    {
        protected readonly ILog log;
        protected readonly MapleFrameDumper dumper;

        protected const UInt16 PhasesPerBlock = 4;
        protected const UInt16 VmuBlockSize = 512;
        protected const UInt16 VmuPhaseSize = VmuBlockSize / PhasesPerBlock;

        protected VmuAction(ILog log)
        {
            this.log = log;
            dumper = new MapleFrameDumper(log);
        }

        protected MapleBusDecodedFrame.MapleAddress AddressConsole
        {
            get
            {
                return new MapleBusDecodedFrame.MapleAddress()
                {
                    portNumber = 0,
                };
            }
        }

        protected class DeviceTree
        {
            public MapleBusDeviceInfoFrame main;
            public MapleBusDeviceInfoFrame sub1;
            public MapleBusDeviceInfoFrame sub2;
            public MapleBusDeviceInfoFrame sub3;
            public MapleBusDeviceInfoFrame sub4;
            public MapleBusDeviceInfoFrame sub5;
        }

        MapleBusDeviceInfoFrame GetNfo(IMapleBus bus, MapleBusDecodedFrame.MapleAddress fromThisDevice)
        {
            // HACK
            fromThisDevice.portNumber = AddressConsole.portNumber;
            var response = bus.DoBusTransaction(new MapleBusRequestDeviceInfoFrame()
            {
                sender = AddressConsole,
                recipient = fromThisDevice,
            });

            var nfoFrame = response as MapleBusDeviceInfoFrame;
            if (nfoFrame == null)
                throw new Exception("Expected " + nameof(MapleBusDeviceInfoFrame) + " but got " + dumper.ToString(response));

            return nfoFrame;
        }

        protected DeviceTree EnumerateDevices(IMapleBus bus)
        {
            var main = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress()
            {
                mainPeripheral = true,
            });

            var tree = new DeviceTree() { main = main };

            // The sender address tells us who is behind the main unit
            if (main.sender.subPeripheral1)
                tree.sub1 = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress() { subPeripheral1 = true });
            if (main.sender.subPeripheral2)
                tree.sub2 = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress() { subPeripheral2 = true });
            if (main.sender.subPeripheral3)
                tree.sub3 = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress() { subPeripheral3 = true });
            if (main.sender.subPeripheral4)
                tree.sub4 = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress() { subPeripheral4 = true });
            if (main.sender.subPeripheral5)
                tree.sub5 = GetNfo(bus, new MapleBusDecodedFrame.MapleAddress() { subPeripheral5 = true });

            return tree;
        }

        protected void PrintTree(DeviceTree tree)
        {
            Action<string, MapleBusDeviceInfoFrame> pp = (what, nfo) =>
            {
                log.That(what + " is " + (nfo == null ? "absent" : "'" + nfo.productName + "'"));
            };

            pp("Main unit", tree.main);
            pp("  + Sub1", tree.sub1);
            pp("  + Sub2", tree.sub2);
            pp("  + Sub3", tree.sub3);
            pp("  + Sub4", tree.sub4);
            pp("  + Sub5", tree.sub5);

            log.That("Full info:");
            dumper.Dump(tree.main);
            if (tree.sub1 != null)
                dumper.Dump(tree.sub1);
            if (tree.sub2 != null)
                dumper.Dump(tree.sub2);
            if (tree.sub3 != null)
                dumper.Dump(tree.sub3);
            if (tree.sub4 != null)
                dumper.Dump(tree.sub4);
            if (tree.sub5 != null)
                dumper.Dump(tree.sub5);
        }

        protected MapleBusDecodedFrame.MapleAddress FindFunction(DeviceTree tree, UInt32 function)
        {
            if (tree.sub1 != null && tree.sub1.supportedFunctions.Any(f => f == function))
                return new MapleBusDecodedFrame.MapleAddress() { subPeripheral1 = true };

            if (tree.sub2 != null && tree.sub2.supportedFunctions.Any(f => f == function))
                return new MapleBusDecodedFrame.MapleAddress() { subPeripheral2 = true };

            if (tree.sub3 != null && tree.sub3.supportedFunctions.Any(f => f == function))
                return new MapleBusDecodedFrame.MapleAddress() { subPeripheral3 = true };

            if (tree.sub4 != null && tree.sub4.supportedFunctions.Any(f => f == function))
                return new MapleBusDecodedFrame.MapleAddress() { subPeripheral4 = true };

            if (tree.sub5 != null && tree.sub5.supportedFunctions.Any(f => f == function))
                return new MapleBusDecodedFrame.MapleAddress() { subPeripheral5 = true };

            return null;
        }

        protected class BlockTimer
        {
            readonly ILog log;
            readonly int totalBlocks;
            readonly string what;
            readonly DateTime start;

            public BlockTimer(ILog log, int totalBlocks, string what = "block")
            {
                this.log = log;
                this.totalBlocks = totalBlocks;
                this.what = what;
                start = DateTime.Now;
            }

            public void LogDone(int block)
            {

                var timeRemain = (DateTime.Now - start).Ticks;
                timeRemain /= block;
                timeRemain *= totalBlocks - block;

                log.That("Done " + what + " " + block + " of " + totalBlocks + " (ETA " + (int)TimeSpan.FromTicks(timeRemain).TotalSeconds + " sec.)");
            }

            public TimeSpan TotalTime { get { return DateTime.Now - start; } }
        }

        protected enum VmuMemoryResult
        {
            Ok,
            OtherError,
            VmuFailure
        }

        protected VmuMemoryResult VmuMemory(byte[] vmuMem, int maxRetry, string what, Func<UInt16, VmuMemoryResult> doOneBlock)
        {
            var dumpOk = new bool[vmuMem.Length / VmuBlockSize];
            return VmuMemory(vmuMem, dumpOk, maxRetry, what, doOneBlock);
        }

        protected VmuMemoryResult VmuMemory(byte[] vmuMem, bool[] blockDoneBitmap, int maxRetry, string what, Func<UInt16,VmuMemoryResult> doOneBlock)
        {
            var blocks = blockDoneBitmap.Length;
            var start = DateTime.Now;

            Func<int[]> getTodoBlocks = () => Enumerable.Range(0, blockDoneBitmap.Length).Where(b => blockDoneBitmap[b] == false).ToArray();

            for (var run = 0; run < (maxRetry + 1); ++run)
            {
                var todoBlocks = getTodoBlocks();

                if (todoBlocks.Length > 0)
                {
                    log.That("Try " + (run + 1) + " in " + what + ", with " + todoBlocks.Length + " blocks remain");

                    var progress = new BlockTimer(log, todoBlocks.Length);

                    for (int i = 0; i < todoBlocks.Length; ++i)
                    {
                        var block = todoBlocks[i];
                        var res = doOneBlock((UInt16)block);
                        blockDoneBitmap[block] = res == VmuMemoryResult.Ok;

                        progress.LogDone(i + 1);
                    }
                }
            }

            var blocksRemain = getTodoBlocks();
            if (blocksRemain.Length == 0)
            {
                log.That("No blocks remain, " + what + " of " + blocks + " was successfull and took " + (DateTime.Now - start).TotalSeconds + " sec.");
                return VmuMemoryResult.Ok;
            }

            log.That("Some blocks remain, " + what + " of " + blocks + " failed after " + (DateTime.Now - start).TotalSeconds + " sec. Here's a list of the remaining blocks: [" + string.Join(", ", blocksRemain.Select(b => "" + b)) + "]");
            return VmuMemoryResult.OtherError;
        }

        protected string Md5Hex(byte[] data)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                var hashBytes = md5.ComputeHash(data);
                return Misc.ToHex(hashBytes, "");
            }
        }

        protected bool[] GetVmuBlockDiff(byte[] vmu1, byte[] vmu2)
        {
            if (vmu1 == null)
                throw new ArgumentNullException(nameof(vmu1));
            if (vmu2 == null)
                throw new ArgumentNullException(nameof(vmu2));
            if (vmu1.Length != vmu2.Length)
                throw new ArgumentException(nameof(vmu1) + " and " + nameof(vmu2) + " differ in size");
            if (vmu1.Length % VmuBlockSize != 0)
                throw new ArgumentException("Vmu memory is not block aligned");

            var blkOk = new bool[vmu1.Length / VmuBlockSize];

            for (int b = 0; b < blkOk.Length; ++b)
            {
                var off = b * VmuBlockSize;
                blkOk[b] = Enumerable.Range(off, VmuBlockSize).All(i => vmu1[i] == vmu2[i]);
            }

            return blkOk;
        }
    }
}
