﻿using System;
using System.Collections.Generic;
using System.Threading;
using MapleCom;

namespace BusMaster.Actions
{
    public class ReadButtonStatesArgs : SamplerArgs
    {
    }

    public class ReadButtonStates : Binah.IAction<ReadButtonStatesArgs>
    {
        private readonly ILog log;

        public ReadButtonStates(ILog log)
        {
            this.log = log;
        }

        public string Switch { get { return "read-button-states"; } }

        public string Info { get { return "reads the button states of an attached controller"; } }

        public void Run(ReadButtonStatesArgs args, string[] unconsumend)
        {
            var dumper = new MapleFrameDumper(log);

            var addressConsole = new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 0,
            };

            var addressController = new MapleBusDecodedFrame.MapleAddress()
            {
                portNumber = 0,
                mainPeripheral = true,
            };

            using (var unstableBus = new MapleCom.Stm32UsbSampler(log, args.sampler))
            using (var bus = new UnstableMapleBus(log, unstableBus, args.maxBusRetries))
            {
                // We have to do at least one of those, otherwise the controller will not answer:
                //   https://www.raphnet.net/programmation/dreamcast_usb/index_en.php
                var info = bus.DoBusTransaction(new MapleBusRequestDeviceInfoFrame() { sender = addressConsole, recipient = addressController });
                dumper.Dump(info);

                var buttonRequest = new MapleBusGetConditionFrame()
                {
                    sender = addressConsole,
                    recipient = addressController,
                    function = MapleCom.MapleConsts.Function.Controller,
                };

                while (true)
                {
                    var buttons = bus.DoBusTransaction(buttonRequest);
                    dumper.Dump(buttons);
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
