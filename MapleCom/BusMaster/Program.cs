﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BusMaster
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            MapleCom.ILog log = null;
            try
            {
                log = new MapleCom.ConsoleLog();

                var app = new Binah.ActionApp(Console.WriteLine) 
                {
                    AppName = "Maple bus master",
                    AppInfo = "A bus master for the SEGA Dreamcast Maple bus."
                };
                app.Add(new Actions.ReadButtonStates(log));
                app.Add(new Actions.DumpVmu(log));
                app.Add(new Actions.WriteVmu(log));

                app.Run(args);

                Environment.ExitCode = 0;
            }
            catch (Exception x)
            {
                if (log != null)
                {
                    log.That(x);
                }
                else
                {
                    EmergencyDump(x);
                }

                Environment.ExitCode = -1;
            }
        }

        static void EmergencyDump(Exception x)
        {
            if (x == null)
                return;

            Console.WriteLine(x.GetType() + " - " + x.Message + "\n" + x.StackTrace);
            EmergencyDump(x.InnerException);
        }


    }
}
