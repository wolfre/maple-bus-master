#include "common.h"
#include "dbg.h"
#include "usb.h"


#include <stdint.h>
#include <string.h>
#include <stm32f1xx.h>

#define _die(x) die(0x2000 | (x))

#ifndef USB_PMA_SIZE
// See Data sheet for STM32F103xxx
#define USB_PMA_SIZE 0x400
#endif

// Sources
// [1] https://os.mbed.com/users/hudakz/code/STM32F103C8T6_Hello/
// [2] https://www.mikrocontroller.net/articles/USB-Tutorial_mit_STM32
//     source from here https://github.com/Erlkoenig90/f1usb/
// [3] STM32F103 data sheet
// [4] STM32F103 reference manual

// <memory-stuff>

// NOTE for some reason the usb sram is mapped with holes to the system bus:
//   out of 4 addressable bytes on the system bus, only the first two are data on the
//   sram, the other two are "void". I couldn't actually find that in the data sheet [3]
//   nor in the ref manual [4]. However looking at the numbers in those two sources
//   (512 bytes of sram take up 1024 byte on the system bus) this seems to be correct.
//   This is nicely explained in [2].

static volatile uint8_t* usb_sram = (uint8_t*)USB_PMAADDR;


static int _alloc_next_sysbus = 0;

static inline void usb_sram_free_all()
{
    _alloc_next_sysbus = 0;
}

static volatile void* usb_sram_alloc(int size)
{
    if( _alloc_next_sysbus > USB_PMA_SIZE )
        _die(0x1);
        
    volatile void* n = usb_sram + _alloc_next_sysbus;
    _alloc_next_sysbus += size * 2;
    
    // NOTE all addresses must be 8-byte aligned on the sysbus (4-byte aligend on the sram)
    while(_alloc_next_sysbus & 0x7)
        ++_alloc_next_sysbus;
    
    return n;
}

static inline uint16_t usb_sysbus_address_to_sram_address(volatile void* sysbus_address)
{
    uint16_t off = sysbus_address - (volatile void*)usb_sram;
    return off / 2;
}

static void usb_copy_to_sram(uint16_t off, const uint8_t* buf, uint16_t len)
{
    // Everything must be 4-byte aligned on sram, so die on odd offsets
    if( off & 1 )
        _die(0x2);
    
    if( (len & 1) != 0 )
        ++len;
    
    uint16_t* b16 = (uint16_t*)buf;
    uint16_t* u16 = (uint16_t*)(usb_sram+(off*2));
    
    for(int i=0; i<(len/2); ++i)
    {
        u16[i*2] = b16[i];
    }
}

static void usb_copy_from_sram(uint8_t* buf, uint16_t len, uint16_t off)
{
    // Everything must be 4-byte aligned on sram, so die on odd offsets
    if( off & 1 )
        _die(0x3);
    
    if( (len & 1) != 0 )
        ++len;
    
    uint16_t* b16 = (uint16_t*)buf;
    uint16_t* u16 = (uint16_t*)(usb_sram+(off*2));
    
    for(int i=0; i<(len/2); ++i)
    {
        b16[i] = u16[i*2];
    }
}

// </memory-stuff>



// These bits are cleared by writing 0, and stay unchanged by writing 1
#define _USB_EPNR_RC_W0   (USB_EP_CTR_RX_Msk | USB_EP_CTR_TX_Msk)
// These bits toggle by writing 1, and stay unchnaged by writing 0
#define _USB_EPNR_TOGGLE  (USB_EP_DTOG_RX_Msk | USB_EPRX_STAT_Msk | USB_EP_DTOG_TX_Msk | USB_EPTX_STAT_Msk)
// These bits are normally writeable
#define _USB_EPNR_RW      (USB_EP_T_FIELD_Msk | USB_EP_KIND_Msk | USB_EPADDR_FIELD)

static uint16_t usb_set_epr(volatile uint16_t* epr, uint16_t mask, uint16_t data) 
{
    uint16_t old = (*epr);
    
    // Clear-only bits
    uint16_t wr0 = (uint16_t)(_USB_EPNR_RC_W0 & (~mask | data));
    // Toggle bits
    uint16_t wr1 = (mask & _USB_EPNR_TOGGLE) & (old ^ data);
    // Normal bits
    uint16_t wr2 = _USB_EPNR_RW & ((old & ~mask) | data);

    // Combine all methods
    *epr = (uint16_t)(wr0 | wr1 | wr2);
    
    return old;
}

#undef _USB_EPNR_RW
#undef _USB_EPNR_TOGGLE
#undef _USB_EPNR_RC_W0


// Endpoints supported by the PHY
#define _USB_PHY_EP 8

typedef struct 
{
    // Start of tx buffer in sram
    uint16_t tx_buffer_address;
    uint16_t _void_0;
    // Size of tx buffer in bytes
    uint16_t tx_buffer_size;
    uint16_t _void_1;
    // Start of rx buffer in sram
    uint16_t rx_buffer_address;
    uint16_t _void_2;
    // Size and fill of rx buffer, see [4] for details
    uint16_t rx_buffer_size;
    uint16_t _void_3;
} usb_epdescriptor_sysbus_layout;

typedef struct 
{
    usb_epdescriptor_sysbus_layout ep[_USB_PHY_EP];
} usb_eptable_sysbus_layout;

typedef struct 
{
    usb_ep_config base;
    volatile uint8_t*  buf_tx_sram;
    volatile uint8_t*  buf_rx_sram;
    volatile uint16_t* epr;
    volatile usb_epdescriptor_sysbus_layout* tbl;
} usb_ep_config_ext;

#define to_ext_config(config)   ((usb_ep_config_ext*)(config))

static volatile usb_eptable_sysbus_layout* ep_table;
static uint8_t next_free_ep = 0;
static usb_ep_config_ext ep_config[_USB_PHY_EP];
static uint8_t scratch_pad[USB_MAX_PKG_SIZE];

static uint16_t to_rx_buffer_size( uint16_t length )
{
    // https://github.com/Erlkoenig90/f1usb/blob/e4c59124cb3ee5183861591dd7ed84101c703790/src/usb.cc#L265
    uint16_t BL_SIZE = !(length <= 62);
    uint16_t NUM_BLOCK = BL_SIZE ? (uint16_t) ((length/32)-1) : (uint16_t) (length/2);
    return (BL_SIZE << 15) | (NUM_BLOCK << 10);
}

static uint32_t ep_type_to_bits(ep_type t)
{
    if( t == ep_type_bulk )
        return USB_EP_BULK;
    
    if( t == ep_type_control )
        return USB_EP_CONTROL;
    
    if( t == ep_type_isochronous )
        return USB_EP_ISOCHRONOUS;
    
    return USB_EP_INTERRUPT;
}

static void usb_init_endpoints()
{
    // NOTE sizeof()/2 coz the struct is defiend with padding, and we need sram size here
    ep_table = (volatile usb_eptable_sysbus_layout*)usb_sram_alloc(sizeof(usb_eptable_sysbus_layout)/2);
    
    // Point USB core to start of EP table. This table always contains all 8 EP entries and can't be shortened.
    // However not all EPs have to be activated of course.
    USB->BTABLE = usb_sysbus_address_to_sram_address(ep_table);
    // New USB devices always have an address of 0 (until a new one is assigned by the host).
    USB->DADDR = USB_DADDR_EF | ( 0 << USB_DADDR_ADD_Pos );
    
    for( int i=0; i<next_free_ep; ++i)
    {
        usb_ep_config_ext* cfg = &(ep_config[i]);
        
        if( cfg->base.reset_handler == NULL )
            _die(0x11);
        if( cfg->base.rx_handler == NULL )
            _die(0x12);
        if( cfg->base.tx_done_handler == NULL )
            _die(0x13);
        
        cfg->buf_tx_sram = (volatile uint8_t*)usb_sram_alloc(cfg->base.tx_buffer_size);
        if( cfg->buf_tx_sram == NULL )
            _die(0x14);
        
        ep_table->ep[i].tx_buffer_address = usb_sysbus_address_to_sram_address(cfg->buf_tx_sram);
        ep_table->ep[i].tx_buffer_size = cfg->base.tx_buffer_size;
        
        cfg->buf_rx_sram = (volatile uint8_t*)usb_sram_alloc(cfg->base.rx_buffer_size);
        if( cfg->buf_rx_sram == NULL )
            _die(0x15);
        
        ep_table->ep[i].rx_buffer_address = usb_sysbus_address_to_sram_address(cfg->buf_rx_sram);
        ep_table->ep[i].rx_buffer_size = to_rx_buffer_size(cfg->base.rx_buffer_size);
        
        cfg->tbl = &(ep_table->ep[i]);
        
        usb_set_epr( \
            cfg->epr, \
            USB_EPRX_STAT_Msk | USB_EP_T_FIELD_Msk | USB_EPADDR_FIELD_Msk | USB_EP_KIND | USB_EPTX_STAT_Msk, \
            USB_EP_RX_NAK | USB_EP_TX_NAK | ep_type_to_bits(cfg->base.type) | ((cfg->base.address & USB_EPADDR_FIELD_Msk) << (USB_EPADDR_FIELD_Pos)));
        
        cfg->base.reset_handler();   
    }
}

static void usb_init_clock()
{
    // Assuming sysclock is 72Mhz, set USB prescaler to 1.5
    RCC->CFGR &= ~RCC_CFGR_USBPRE_Msk;
    
    // enable gpio A clock on APB2
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    
    // enable usb clock on APB1
    RCC->APB1ENR |= RCC_APB1ENR_USBEN;
}

static void usb_init_enable_logic()
{
    // Clear the PDWN bit and just keep the RESET asserted
    USB->CNTR = USB_CNTR_FRES;
    // Now we have to just wait for a certain time for USB to get ready
    sleep_us(5);
    // Turn on USB, enable transfer complete and reset IRQ
    USB->CNTR = USB_CNTR_CTRM | USB_CNTR_RESETM;

    // Keep IRQs disabled for now
    USB->ISTR = 0;
    NVIC_DisableIRQ(USB_LP_CAN1_RX0_IRQn);
    NVIC_ClearPendingIRQ (USB_LP_CAN1_RX0_IRQn);    
}

void usb_init()
{
    usb_init_clock();
    usb_init_enable_logic();
        
    memset(scratch_pad, 0, sizeof(scratch_pad));
    memset(ep_config, 0, sizeof(ep_config));

    ep_config[0].epr = &(USB->EP0R);
    ep_config[1].epr = &(USB->EP1R);
    ep_config[2].epr = &(USB->EP2R);
    ep_config[3].epr = &(USB->EP3R);
    ep_config[4].epr = &(USB->EP4R);
    ep_config[5].epr = &(USB->EP5R);
    ep_config[6].epr = &(USB->EP6R);
    ep_config[7].epr = &(USB->EP7R);
    
    next_free_ep = 0;
    usb_sram_free_all();
}

usb_ep_config* usb_new_ep()
{
    if(next_free_ep >= _USB_PHY_EP )
        return NULL;
    
    usb_ep_config_ext* new_ep = &(ep_config[next_free_ep]);
    ++next_free_ep;
    
    return &(new_ep->base);
}

void usb_connect()
{
    // Clear and enable interrupt source
    USB->ISTR = USB_ISTR_RESET_Msk;
    NVIC_ClearPendingIRQ (USB_LP_CAN1_RX0_IRQn);
    NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
    
    // Set address to 0 and enable function.
    // New USB devices always have an address of 0 (until a new one is assigned by the host).
    USB->DADDR = USB_DADDR_EF | ( 0 << USB_DADDR_ADD_Pos );
    // This will kick off the USB reset IRQ
}

void usb_tx_data(usb_ep_config* config, uint16_t len, const void* data)
{
    usb_ep_config_ext* ext_cfg = to_ext_config(config);
    
    uint16_t sram_off = usb_sysbus_address_to_sram_address(ext_cfg->buf_tx_sram);
    usb_copy_to_sram(sram_off, data, len);
    
    ext_cfg->tbl->tx_buffer_size = len;
    ext_cfg->tbl->tx_buffer_address = usb_sysbus_address_to_sram_address(ext_cfg->buf_tx_sram);
    usb_set_epr(ext_cfg->epr, USB_EPTX_STAT_Msk, USB_EP_TX_VALID);
}

void usb_tx_0(usb_ep_config* config)
{
    usb_ep_config_ext* ext_cfg = to_ext_config(config);
    
    ext_cfg->tbl->tx_buffer_size = 0;
    ext_cfg->tbl->tx_buffer_address = usb_sysbus_address_to_sram_address(ext_cfg->buf_tx_sram);
    
    usb_set_epr(ext_cfg->epr, USB_EPTX_STAT_Msk, USB_EP_TX_VALID);
}

void usb_tx_stall(usb_ep_config* config)
{
    usb_ep_config_ext* ext_cfg = to_ext_config(config);
    usb_set_epr(ext_cfg->epr, USB_EPTX_STAT_Msk, USB_EP_TX_STALL);
}

void usb_tx_nak(usb_ep_config* config)
{
    usb_ep_config_ext* ext_cfg = to_ext_config(config);
    usb_set_epr(ext_cfg->epr, USB_EPTX_STAT_Msk, USB_EP_TX_NAK);
}

void usb_set_device_address(uint8_t address)
{
    USB->DADDR = (uint16_t)(USB_DADDR_EF | address);
}

static void usb_irq_reset()
{
    usb_sram_free_all();
    usb_init_endpoints();
}

// Only these IRQs will be processed by the handler
#define _USB_IRQ_MASK  (USB_ISTR_RESET_Msk | USB_ISTR_CTR_Msk)

// This is overriding the placeholder function put in place by the linker
void USB_LP_IRQHandler()
{
    uint16_t ISTR;
    
    // Process IRQs until all pending ones are done.
    // In case IRQs happen while handling another one, the next loop iteration will take care of this as well.
    while (((ISTR = USB->ISTR) & _USB_IRQ_MASK) != 0)
    {
        // Resets occur regularly until we are connected to a host with an address set. [4]
        if (ISTR & USB_ISTR_RESET) 
        {
            // Clear reset IRQ
            USB->ISTR = USB_ISTR_PMAOVR | USB_ISTR_ERR | USB_ISTR_WKUP | USB_ISTR_SUSP | USB_ISTR_SOF | USB_ISTR_ESOF;
            usb_irq_reset();
        }
        
        // CTR means a transfer was completed correctly [4]
        if (ISTR & USB_ISTR_CTR)
        {   

            // The "direction" of that transfer: false means "IN" transfer (Device->Host), true means "IN" oder "OUT"
            //   Yes that is confusing, see [4]
            uint16_t dir = ISTR & USB_ISTR_DIR; // 0=TX, 1=RX/TX
            // The PHY EP that had the transfer (NOTE PHY register number, not actual EP address)
            uint8_t EP = (ISTR & USB_ISTR_EP_ID) >> USB_ISTR_EP_ID_Pos;
            
            usb_ep_config_ext* cfg = &(ep_config[EP]);
            
            // Clear RX/TX-Flags.
            // In case we get another IRQ while doing this, the HW will not clear those flags
            // and the next roundtrip through this loop will also service that.
            uint16_t orig_epr = usb_set_epr(cfg->epr, USB_EP_CTR_RX_Msk | USB_EP_CTR_TX_Msk, 0);

            if( ISTR & USB_ISTR_ERR )
                dbg_uart_tx( (dir == 0) ? '=' : ':');
            else
                dbg_uart_tx( (dir == 0) ? '<' : '>');
            
            dbg_uart_tx_hex_u16(orig_epr);
            
            if ((dir != 0) && (orig_epr & USB_EP_CTR_RX_Msk))
            {
                uint16_t len = ep_table->ep[EP].rx_buffer_size & USB_COUNT0_RX_0_COUNT0_RX_0;
                if( len > USB_MAX_PKG_SIZE )
                    _die(0x20);
                
                uint16_t sram_off = usb_sysbus_address_to_sram_address(cfg->buf_rx_sram);
                usb_copy_from_sram(scratch_pad, len, sram_off);
                
                cfg->base.rx_handler( \
                    (orig_epr & USB_EP0R_SETUP_Msk) == 0 ? transfer_out : transfer_setup, \
                    len, \
                    scratch_pad);
            }
            
            if (orig_epr & USB_EP_CTR_TX_Msk) 
            {
                cfg->base.tx_done_handler();
            }
            
        }
    }
}

#undef _USB_IRQ_MASK


void usb_rx_ready(usb_ep_config* config, uint16_t max_rx_size)
{
    usb_ep_config_ext* ext_cfg = to_ext_config(config);
    
    if( max_rx_size > ext_cfg->base.rx_buffer_size )
        _die(6);
    
    // ReSetup our buffer for the next RX
    ext_cfg->tbl->rx_buffer_address = usb_sysbus_address_to_sram_address(ext_cfg->buf_rx_sram);
    ext_cfg->tbl->rx_buffer_size = to_rx_buffer_size(max_rx_size);

    // Tell the device that we are ready (VALID) for RX again
    usb_set_epr(ext_cfg->epr, USB_EPRX_STAT_Msk, USB_EP_RX_VALID);
}

#undef _die
