#ifndef _MAIN_H
#define _MAIN_H

// The following section describes the USB interface of this MapleBus Master device
#define MAPLE_BUS_MASTER_INTERFACE_VERSION  1

// Commands are written to the command endpoint.
// Commands start with one byte signaling the command, with optional
// addition arguments to follow (see below). Multi-byte numbers are 
// encoded MSB first, e.g. U16 is 2 bytes [MSB,LSB], etc...

typedef enum
{
    // No additional argument
    // Will provide an info response on the command enpoint
    // The Info response has the following layout:
    // - U8 interface version supported by this MapleBus device.
    //   See  MAPLE_BUS_MASTER_INTERFACE_VERSION
    // - U16 TX buffer size in bytes.
    //   This data can be filled using  command_write_tx_data  command.
    // - U16 RX sample buffer size in bytes.
    //   This data can be read using  command_read_sample_buffer  command.
    command_get_info = 1,
    
    // Args: [Offset:U16]
    // No response on command endpoint.
    // Sets up writes to the TX buffer. OUT transactions to data endpoint will be written to TX buffer.
    // Each OUT transaction to the data endpoint will auto advance the offset for the next transaction.
    // Each byte in the TX buffer is an actual data byte on the maple bus, that can be sent
    // using the  command_execute  command.
    command_write_tx_data = 2,
    
    // Args: [Offset:U16]
    // No response on command endpoint
    // Sets up reads (IN transactions) to data endpoint, provide RX sample buffer.
    // Each IN transaction to the data endpoint will auto advance the offset for the next transaction.
    // The sample buffer is made up of two halves:
    // - The lower halve contains samples from Data 1 line
    // - The upper halve contains samples from Data 5 line
    // Each byte contains 8 samples, with the MSB being the oldes sample.
    // A 1 means the line was high, a 0 means it was low.
    command_read_sample_buffer = 3,
    
    // Args: [Command Length:U16]
    // Executes a command with the provided length from the TX buffer, bits are sent MSB first.
    // Any response is sampled to the RX sample buffer.
    // After execution is done, the command endpoint will provide a 1-byte status response.
    // See  proto_status  for details.
    command_execute = 4,
    
    // No additional argument.
    // No response is provided.
    // Copies the complete content of the RX buffer verbatim to the TX sample buffer.
    // This can be used to do self tests.
    command_copy_tx_to_rx = 5,
} proto_command;


typedef enum
{
    // Everything went fine
    status_ok = 1,
    // Reading a response timed out
    status_timeout = 2,
} proto_status;






#endif
