#include <stm32f1xx.h>
#include "common.h"
#include "dbg.h"


void sleep_us(uint16_t t)
{
    // Manually timed at 72mhz
    uint32_t n = t*9;
    
    for (uint32_t i = 0; i < n; ++i) 
        __NOP ();
}
