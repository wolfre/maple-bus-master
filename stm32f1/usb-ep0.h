#ifndef _USB_EP0_H
#define _USB_EP0_H

#include "usb.h"
#include <stdint.h>

#define USB_EP0_MAX_PKG_SIZE USB_MAX_PKG_SIZE

// See https://www.beyondlogic.org/usbnutshell/usb6.shtml
typedef struct
{
    uint8_t bmRequestType;
    uint8_t bRequest;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
    
} usb_setup_packet;

typedef enum
{
    usb_device_descriptor = 1,
    usb_config_descriptor = 2,
    usb_string_descriptor = 3,
    usb_interface_descriptor = 4,
    usb_endpoint_descriptor = 5,
    usb_device_qualifier_descriptor = 6,
}
usb_decriptor_type;


// Setup the EP0 on a newly allocated endpoint from  usb_new_ep()
// Will reset any internal state, e.g. descriptors added etc
void usb_ep0_setup(usb_ep_config* cfg);

// Add a usb descriptor
void usb_ep0_add_descriptor(usb_decriptor_type type, uint8_t index, uint16_t len, const uint8_t* data);

// Add a string descriptor
//  This is a conveniance function that can be used instead of the generic  usb_ep0_add_descriptor  function above.
//  It spares you to do unicode and header stuff for "just strings".
void usb_ep0_add_string_descriptor(uint8_t index, const char* cstring);

// No config is activated
#define USB_EP0_NO_CFG_ACTIVE -1

// Gets the active config,  USB_EP0_NO_CFG_ACTIVE  if none has been activated by the host
int8_t usb_ep0_cfg_active();

#endif
