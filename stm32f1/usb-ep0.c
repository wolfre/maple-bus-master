#include "usb-ep0.h"
#include "dbg.h"
#include <string.h>

#define _die(x) die(0x3000 | (x))
#define _MAX_DESCRIPTORS 16

typedef enum 
{
    stage_setup,
    stage_data_out,
    stage_data_in,
    // previous stage was  stage_data_out  or  stage_setup (for set address)
    stage_status_out,
    // previous stage was  stage_data_in
    stage_status_in,
} ep0_stage;

static usb_ep_config* ep0;
static uint8_t device_address = 0;
static ep0_stage stage_next = stage_setup;
static int8_t active_cfg = USB_EP0_NO_CFG_ACTIVE;

typedef struct
{
    usb_decriptor_type type;
    uint8_t index;
    uint16_t len;
    const uint8_t* data;
} ep0_descriptor;

static uint8_t descriptors_next = 0;
static ep0_descriptor descriptors[_MAX_DESCRIPTORS];

void usb_ep0_add_descriptor(usb_decriptor_type type, uint8_t index, uint16_t len, const uint8_t* data)
{
    if( descriptors_next >= _MAX_DESCRIPTORS )
        _die(0xf0);
    if( data == NULL )
        _die(0xf1);
    if( len == 0 )
        _die(0xf2);
    if( len > USB_EP0_MAX_PKG_SIZE )
        _die(0xf3);
    
    ep0_descriptor* d = &(descriptors[descriptors_next]);
    ++descriptors_next;
    
    d->type = type;
    d->index = index;
    d->len = len;
    d->data = data;
}

typedef struct
{
    uint8_t index;
    uint16_t len;
    const uint8_t* data;
} ep0_cstring_descriptor;

static uint8_t cstring_descriptors_next = 0;
static ep0_cstring_descriptor cstring_descriptors[_MAX_DESCRIPTORS];
static uint8_t temp_string_descriptor_data[USB_EP0_MAX_PKG_SIZE];
static ep0_descriptor temp_string_descriptor;

void usb_ep0_add_string_descriptor(uint8_t index, const char* cstring)
{
    if( cstring_descriptors_next >= _MAX_DESCRIPTORS )
        _die(0xe0);
    if( cstring == NULL )
        _die(0xe1);
    
    uint16_t len = strlen(cstring);
    if( len*2 >= USB_EP0_MAX_PKG_SIZE-2 )
        _die(0xe2);
    
    ep0_cstring_descriptor* d = &(cstring_descriptors[cstring_descriptors_next]);
    ++cstring_descriptors_next;
    
    d->data = (const uint8_t*)cstring;
    d->len = len;
    d->index = index;
}

static ep0_descriptor* get_temp_string_descriptor(ep0_cstring_descriptor* cstring)
{
    // skip setting the index, it is not needed to actually send the descriptor
    // temp_string_descriptor.index = index;
    uint8_t total_length = cstring->len * 2 + 2;
    temp_string_descriptor.len = total_length;

    temp_string_descriptor_data[0] = total_length; // bLength
    temp_string_descriptor_data[1] = usb_string_descriptor; // bDescriptorType
    
    for(int j=0; j<cstring->len; ++j)
    {
        temp_string_descriptor_data[j*2 + 2] = cstring->data[j];
        temp_string_descriptor_data[j*2 + 3] = 0;
    }
    
    return &temp_string_descriptor;
}

static ep0_descriptor* find_descriptor(uint8_t type, uint8_t index)
{
    for(int i=0; i<descriptors_next; ++i)
    {
        ep0_descriptor* d = &(descriptors[i]);
        if(d->data != NULL && d->type == type && d->index == index )
            return d;
    }
    
    if( type == usb_string_descriptor )
    {
        for(int i=0; i<cstring_descriptors_next; ++i )
        {
            if( cstring_descriptors[i].index == index )
            {
                return get_temp_string_descriptor( &(cstring_descriptors[i]) );
            }
        }
    }
    
    return NULL;
}

static ep0_stage ep0_rx_setup(usb_setup_packet* head)
{
    if( head->bmRequestType == 0x80 && head->bRequest == 6)
    {
        dbg_uart_tx('g');
        dbg_uart_tx_hex_u8(head->wLength);
        
        // GET xxx DESCRIPTOR
        uint8_t type = (head->wValue >> 8) & 0xFF;
        uint8_t index = head->wValue & 0xFF;
        
        ep0_descriptor* d = find_descriptor(type, index);
        if( d != NULL )
        {
            uint16_t l = d->len;
            if( l > head->wLength)
                l = head->wLength;
            usb_tx_data(ep0, l, d->data);
            dbg_uart_tx('!');
            dbg_uart_tx_hex_u8(type);
            dbg_uart_tx_hex_u8(index);
            return stage_data_in;
        }
  
        // We just STALL (yes not NAK) all unsupported descriptors
        usb_tx_stall(ep0);
        usb_rx_ready(ep0, ep0->rx_buffer_size);
        dbg_uart_tx('?');
        dbg_uart_tx_hex_u8(type);
        dbg_uart_tx_hex_u8(index);
        dbg_uart_tx_hex_u16(head->wIndex);
        dbg_uart_tx('\n');
        return stage_setup;
    }
    
    if( head->bmRequestType == 0x80 && head->bRequest == 0 && head->wLength == 2)
    {
        // GET STATUS
        dbg_uart_tx('g');
        dbg_uart_tx('S');
        dbg_uart_tx('T');
        // https://www.beyondlogic.org/usbnutshell/usb6.shtml
        uint16_t get_status_request = 0;
        usb_tx_data(ep0, sizeof(get_status_request), &get_status_request);
        return stage_data_in;
    }
    
    if( head->bmRequestType == 0 && head->bRequest == 5)
    {
        // SET ADDRESS
        device_address = (uint8_t)(head->wValue & 0x007F);
        usb_tx_0(ep0);
        dbg_uart_tx('s');
        dbg_uart_tx_hex_u8( device_address );
        return stage_status_out;
    }
    
    if( head->bmRequestType == 0 && head->bRequest == 9)
    {
        // SET CONFIGURATION
        usb_tx_0(ep0);
        dbg_uart_tx('c');
        active_cfg = (int8_t)(head->wValue & 0x7f);
        dbg_uart_tx_hex_u8( (uint8_t)active_cfg );
        return stage_status_out;
    }
    
    // We just STALL (yes not NAK) all unsupported setup requests
    usb_tx_stall(ep0);
    usb_rx_ready(ep0, ep0->rx_buffer_size);
    dbg_uart_tx('?');
    dbg_uart_tx_hex_u8( head->bmRequestType );
    dbg_uart_tx('-');
    dbg_uart_tx_hex_u8( head->bRequest );
    dbg_uart_tx('-');
    dbg_uart_tx_hex_u16( head->wValue );
    dbg_uart_tx('-');
    dbg_uart_tx_hex_u16( head->wIndex );
    dbg_uart_tx('-');
    dbg_uart_tx_hex_u16( head->wLength );
    dbg_uart_tx('\n');
    return stage_setup;
}

static void ep0_rx(transfer_type type, uint16_t length, uint8_t* data)
{
    dbg_uart_tx('r');
    
    if( stage_next == stage_setup )
    {
        if( type != transfer_setup )
            _die(0x10);
        
        dbg_uart_tx('s');
        
        if( length < sizeof(usb_setup_packet) )
        {
            // ignore crap thats not a setup packet
            // some usb hubs seem to produce this
            usb_tx_stall(ep0);
            usb_rx_ready(ep0, ep0->rx_buffer_size);
            
            dbg_uart_tx('!');
            dbg_uart_tx('\n');
            return;
        }
        
        stage_next = ep0_rx_setup((usb_setup_packet*)data);
        return;
    }
    
    if( stage_next == stage_data_out )
        _die(0x13);
    
    if( stage_next == stage_data_in )
        _die(0x14);
    
    if( stage_next == stage_status_in )
    {
        dbg_uart_tx('S');
        
        if( length != 0 )
        {
            for(int i=0;i<length;++i)
                dbg_uart_tx_hex_u8(data[i]);
            
            _die(0x15);
        }
        
        if( type != transfer_out)
            _die(0x16);
        
        usb_rx_ready(ep0, ep0->rx_buffer_size);
        stage_next = stage_setup;
        
        dbg_uart_tx('\n');
        return;
    }
    
    if( stage_next == stage_status_out )
        _die(0x17);

}

static void ep0_tx_done()
{
    dbg_uart_tx('t');
    
    if( stage_next == stage_setup )
        _die( 0x20 );
    if( stage_next == stage_data_out )
        _die( 0x21 );
        
    if( stage_next == stage_data_in )
    {
        dbg_uart_tx('i');
        usb_rx_ready(ep0, 0);  
        stage_next = stage_status_in;
        return;
    }
    
    if( stage_next == stage_status_in )
        _die( 0x22 );
    
    if( stage_next == stage_status_out )
    {
        if( device_address != 0)
        {
            usb_set_device_address(device_address);
            device_address = 0;
            dbg_uart_tx('!');
        }
        
        usb_rx_ready(ep0, ep0->rx_buffer_size);
        stage_next = stage_setup;
        dbg_uart_tx('\n');
        return;
    }
}

static void ep0_reset()
{
    device_address = 0;
    stage_next = stage_setup;
    active_cfg = USB_EP0_NO_CFG_ACTIVE;
    usb_rx_ready(ep0, ep0->rx_buffer_size);
    dbg_uart_tx_str("\nep0-rst\n");
}

void usb_ep0_setup(usb_ep_config* cfg)
{
    ep0 = cfg;
    ep0->address = 0;
    ep0->type = ep_type_control;
    ep0->tx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep0->rx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep0->rx_handler = &ep0_rx;
    ep0->tx_done_handler = &ep0_tx_done;
    ep0->reset_handler = &ep0_reset;
    
    descriptors_next = 0;
    memset(descriptors, 0, sizeof(descriptors));
    cstring_descriptors_next = 0;
    memset(cstring_descriptors, 0, sizeof(cstring_descriptors));
    temp_string_descriptor.type = usb_string_descriptor;
    temp_string_descriptor.data = temp_string_descriptor_data;
}

int8_t usb_ep0_cfg_active()
{
    return active_cfg;
}

#undef _die
#undef _MAX_DESCRIPTORS
