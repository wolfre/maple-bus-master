#ifndef _MAPLE_H
#define _MAPLE_H

#include <stdint.h>

// Call this to setup things
void maple_init();

// Will send the data out MSB first
void maple_send(uint16_t len, const uint8_t* data);

// NOTE the amount of raw samples, each sample takes 2 bit (2 data lines)
#define MAPLE_SAMPLE_LENGTH 1024*8
uint8_t maple_sample_buffer[MAPLE_SAMPLE_LENGTH];

#define MAPLE_RX_OK       1
#define MAPLE_RX_TIMEOUT  0

// Will wait for a transmission to start and sample the data lines into  maple_sample_buffer
// Will return MAPLE_RX_TIMEOUT on timeout to detect any transmission, or MAPLE_RX_OK otherwise.
// maple_sample_buffer  will hold the data for data 1 and data 5 lines split in the middle.
// So the lower half of the bytes will have data 1 samples and the upper half has data 5 samples.
// Each byte (of samples) contains 8 samples, the oldest is the MSB and the youngest sample the LSB.
uint8_t maple_receive();

void maple_test();

#endif
