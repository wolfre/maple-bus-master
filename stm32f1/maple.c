#include "maple.h"
#include "dbg.h"
#include "common.h"

#include <stm32f1xx.h>
#include <string.h>

#define data5_low() GPIOA->BSRR = GPIO_BSRR_BR2
#define data1_low() GPIOA->BSRR = GPIO_BSRR_BR3

#define data5_hi()  GPIOA->BSRR = GPIO_BSRR_BS2
#define data1_hi()  GPIOA->BSRR = GPIO_BSRR_BS3

#define data_port   (GPIOA->IDR)
#define data5_mask  ((uint32_t)(1 << 2))
#define data1_mask  ((uint32_t)(1 << 3))


#define _cnf_output  0
#define _cnf_input   1
#define _cnf_alt_out 2
#define _mode_pp50mhz 3
#define _mode_in      0

#define _mask    (GPIO_CRL_CNF2_Msk | GPIO_CRL_CNF3_Msk | GPIO_CRL_MODE2_Msk | GPIO_CRL_MODE3_Msk )


static void data_input()
{
    // set pins input floating
    GPIOA->CRL = (GPIOA->CRL & ~_mask) | (uint32_t)( \
        (_cnf_input<<GPIO_CRL_CNF2_Pos) | (_mode_in<<GPIO_CRL_MODE2_Pos) | \
        (_cnf_input<<GPIO_CRL_CNF3_Pos) | (_mode_in<<GPIO_CRL_MODE3_Pos) \
        );
}

static void data_output()
{
    data1_hi();
    data5_hi();
    
    // set pins output push-pull with 50mhz
    GPIOA->CRL = (GPIOA->CRL & ~_mask) | (uint32_t)( \
        (_cnf_output<<GPIO_CRL_CNF2_Pos) | (_mode_pp50mhz<<GPIO_CRL_MODE2_Pos) | \
        (_cnf_output<<GPIO_CRL_CNF3_Pos) | (_mode_pp50mhz<<GPIO_CRL_MODE3_Pos) \
        );
}

static void setup_io()
{
    // enable gpio A, gpio B and SPI1 clock on APB2
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_SPI1EN;
    // enable SPI2 clock on APB1
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

    data_input();
    
    // map alt functions for SPI1 on GPIO A:
    //  A5 = CLK  output
    //  A6 = MISO (input)
    //  NOTE NSS and MOSI are not mapped and not needed
    
    GPIOA->CRL &= ~(\
        GPIO_CRL_CNF5_Msk | GPIO_CRL_MODE5_Msk | \
        GPIO_CRL_CNF6_Msk | GPIO_CRL_MODE6_Msk  \
        );
    
    GPIOA->CRL |= \
        (_cnf_alt_out <<GPIO_CRL_CNF5_Pos) | (_mode_pp50mhz <<GPIO_CRL_MODE5_Pos) | \
        (_cnf_input   <<GPIO_CRL_CNF6_Pos) | (_mode_in      <<GPIO_CRL_MODE6_Pos);

    // map alt functions for SPI2 on GPIO B:
    //  B13 = CLK  input
    //  B15 = MOSI (input)
    //  NOTE NSS and MISO are not mapped and not needed
    GPIOB->CRH &= ~(\
        GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk | \
        GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk  \
        );
    
    GPIOB->CRH |= \
        (_cnf_input <<GPIO_CRH_CNF13_Pos) | (_mode_in <<GPIO_CRH_MODE13_Pos) | \
        (_cnf_input <<GPIO_CRH_CNF15_Pos) | (_mode_in <<GPIO_CRH_MODE15_Pos);
    
    // SPI1 (master mode) = DATA1 and SPI2 (slave mode) = DATA5
    // SPI1 clock drives SPI2 so sampling is in sync
    
    // BUG UNDOCUMENTED HARDWARE BUG!
    //   As long as USB is off everything is fine, but if USB is enabled and fired some IRQs the SPI1 hangs.
    //   I couldn't find official doc on this, but here's how I got the idea:
    //   Looking at "STM32F10xxC/D/E Errata sheet" "April 2020 ES0340 Rev 16" there's a lot of talk about SPI1
    //   together with USARTx. Things seem to evolve around the "Multi Master Mode" of SPI, which can be
    //   be disabled by setting SSOE in CR2. So I tried this and it does work (even if the output pin is not
    //   mapped to ALT function). With this set (MMM disabled) things work as exepcted and SPI1 does not hang.
    SPI1->CR2 = SPI_CR2_SSOE;
    
    SPI1->CR1 = \
        (SPI_CR1_DFF) | \
        (SPI_CR1_SPE) | \
        (2 << SPI_CR1_BR_Pos) | \
        (SPI_CR1_MSTR) ;
    
    // SPI2 is in slave mode, with SW slave select line (SSM)
    SPI2->CR1 = \
        (SPI_CR1_DFF) | \
        (SPI_CR1_SSM) | \
        (SPI_CR1_SPE) ;
}

#undef _mask
#undef _mode_in
#undef _mode_pp50mhz
#undef _cnf_alt_out
#undef _cnf_input
#undef _cnf_output



static void delay_half()
{
    // NOTE this should be slightly longer than 120ns based on original HW
    // We are taking it quite slow here, as the bus is sync we can do this
    // and so reduce the chances of transmission errors etc.
    for(int i=0;i<20;++i)
        __NOP();
}

static void delay_bit()
{
    // NOTE this should be slightly longer than 250ns based on original HW
    delay_half();
    delay_half();
}

static void delay_setup()
{
    // NOTE this should be a bit under 100ns based on original HW
    // See nove above on why we are taking it slow.
    for(int i=0;i<20;++i)
        __NOP();
}



void maple_init()
{
    setup_io();
        
    for(int i=0; i<MAPLE_SAMPLE_LENGTH; ++i)
        maple_sample_buffer[i] = 0xff; // NOTE the line idle state is high on both data lines
    
    dbg_uart_tx_str("maple-init\n");
}

static void send_lead_in()
{
    data1_low();
    delay_half();
    data5_low();
    
    delay_bit();
    data5_hi();
    delay_half();
    data5_low();
    
    delay_bit();
    data5_hi();
    delay_half();
    data5_low();
    
    delay_bit();
    data5_hi();
    delay_half();
    data5_low();
    
    delay_bit();
    data5_hi();
    delay_half();
    data1_hi();
    delay_half();
    data5_low();
    delay_half();
}

static void send_lead_out()
{
    delay_half();
    
    data5_hi();
    delay_half();
    data5_low();
    delay_half();
    
    data1_low();
    delay_bit();
    data1_hi();
    delay_half();
    
    data1_low();
    delay_bit();
    data1_hi();
    delay_half();
    
    data5_hi();
}

void send_u8_msb_first(uint8_t d)
{
    // NOTE d1=hi d5=low, see http://mc.pp.se/dc/maplewire.html
    
    for( uint8_t i = 0; i<4; ++i)
    {
        delay_setup();
        
        if( d & 0x80 )
            data5_hi();
        
        delay_setup();
        
        data1_low();
        
        delay_bit();
        
        data5_hi();
        
        delay_setup();
        
        if( d & 0x40 )
            data1_hi();
        
        delay_setup();
        
        data5_low();
        
        delay_bit();
        
        data1_hi();
        
        d <<= 2;
    }
}

void maple_send(uint16_t len, const uint8_t* data)
{
    data_output();
    send_lead_in();
    
    for(uint16_t i=0;i<len;++i)
    {
        send_u8_msb_first(data[i]);
        
        // When reading blocks from VMU memory, every about 16 bytes there's a pause.
        // When we send larger chunks of data it is unclear if this is also expected of us.
        // For safety and paranoia lets do this too.
        if( (i & 0x000f) == 0xf )
            sleep_us(100);
    }
    
    send_lead_out();
    data_input();
}

static void swap_buffer_u16()
{
    // NOTE when addressing the memory as 16 bit words (as we do when sampling things),
    //   the byte order is swapped ... so swap it back ...
    for( uint16_t i=0; i<(MAPLE_SAMPLE_LENGTH/sizeof(uint16_t)); ++i)
    {
        uint8_t t = maple_sample_buffer[i*2];
        maple_sample_buffer[i*2] = maple_sample_buffer[i*2 + 1];
        maple_sample_buffer[i*2 + 1] = t;
    }
}

uint8_t maple_receive()
{
    if( (data_port & (data1_mask | data5_mask)) != (data1_mask | data5_mask) )
        return MAPLE_RX_TIMEOUT; // We missed the start

    uint16_t* d1 = (uint16_t*)maple_sample_buffer;
    uint16_t* d5 = (uint16_t*)(maple_sample_buffer + MAPLE_SAMPLE_LENGTH/2);

    uint16_t tmo = 1000;
    uint16_t i = 0;
    
    // fill entry 0 in fifo
    SPI1->DR = 0;
    // with for fifo to be "taken" and start sampling (transmitting it)
    while((SPI1->SR & SPI_SR_TXE) == 0);
    
    while(i < (MAPLE_SAMPLE_LENGTH/(2*sizeof(uint16_t))))
    {
        // Fill up TX fifo of SPI master to generate next 16 sampling clocks
        //   The fifo is exactly one entry, hence one loop means we have a
        //   free entry, as we did wait for the previous transaction to finish.
        //   It is finished as we waited for the incoming data below.
        SPI1->DR = 0;
        
        // When we are done with one word of sample,
        //   the next word is being sampled as we already filled the TX fifo above
        //   We now have time to process the data till the next word is sent out
        while((SPI1->SR & SPI_SR_RXNE) == 0);
        uint16_t tmp_d1 = SPI1->DR;

        while((SPI2->SR & SPI_SR_RXNE) == 0);
        uint16_t tmp_d5 = SPI2->DR;
        
        if( i == 0)
        {
            if( tmp_d1 != 0xffff || tmp_d5 != 0xffff )
            {
                d1[i] = tmp_d1;
                d5[i] = tmp_d5;
                ++i;
                dbg_hi();
            }
            else
            {
                --tmo;
                if( tmo == 0)
                    return MAPLE_RX_TIMEOUT;
            }
        }
        else
        {
            d1[i] = tmp_d1;
            d5[i] = tmp_d5;
            
            if( tmp_d1 == 0xffff && tmp_d5 == 0x0000 && d1[i-1] == 0xffff && d5[i-1] == 0x0000 )
            {
                // NOOP on bus, don't store sample, this is an importand space saving operation:
                //   When reading long aswers from VMU (e.g. block read) those come in bursts. 
                //   Between the burtst the bus is in the above detected noop state. The bursts
                //   (data) make up just about 50% of the bus time, so it really pays off to filter 
                //   them out. It basically halves our needed buffer space. This leaves us spare 
                //   for other things, and speeds up transmission via USB as well (less data to transmit)
            }
            else
            {
                ++i;
            }
        }
    }
    
    swap_buffer_u16();
    
    dbg_low();
    return MAPLE_RX_OK;
}

static const uint8_t test_status_request_data[] = { 0x00, 0x00, 0x20, 0x01, 0x01 };

void maple_test()
{
    maple_send(sizeof(test_status_request_data), test_status_request_data);
    uint8_t r = maple_receive();
    
    dbg_uart_tx_str("RX=");
    dbg_uart_tx_hex_u8(r);
    dbg_uart_tx('\n');
}
