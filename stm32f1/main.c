#include <string.h>
#include <stdint.h>
#include <stm32f1xx.h>

#include "common.h"
#include "dbg.h"
#include "usb.h"
#include "usb-ep0.h"
#include "maple.h"
#include "main.h"

static void ext_crystal_8mhz_72mhz()
{
    // See Figure 2. Clock tree  in Datasheet
    // See 3.1 Flash access control register  in memory programming manual


    // Enable flash prefetch
    FLASH->ACR |= FLASH_ACR_PRFTBE;
    // Set flash wait states to 4, which is needed for 72 mhz cpu operation
    FLASH->ACR |= 2 << FLASH_ACR_LATENCY_Pos;
    
    // Switch on HSE (aka crystal) and wait for it to start up
    RCC->CR |= RCC_CR_HSEON;
    while( ( RCC->CR & RCC_CR_HSERDY) == 0 );
    
    uint32_t cfgr = RCC->CFGR;
    // PLLXTPRE = dont divide by 2 (8Mhz crystal / 1 = 8Mhz)
    cfgr &= ~RCC_CFGR_PLLXTPRE_Msk;
    
    // PLLMUL = 9 (9*8Mhz==72MHz)
    cfgr &= ~RCC_CFGR_PLLMULL_Msk;
    cfgr |= RCC_CFGR_PLLMULL9;
    
    // PLLSCR = HSE
    cfgr |= 1 << RCC_CFGR_PLLSRC_Pos;
    
    // AHB  prescaler = /1
    cfgr &= ~RCC_CFGR_HPRE_Msk;
    cfgr |= RCC_CFGR_HPRE_DIV1;
    
    // APB2 prescaler = /1
    cfgr &= ~RCC_CFGR_PPRE2_Msk;
    cfgr |= RCC_CFGR_PPRE2_DIV1;
    
    // APB1 prescaler = /2
    cfgr &= ~RCC_CFGR_PPRE1_Msk;
    cfgr |= RCC_CFGR_PPRE1_DIV2;
    
    // "commit" the above settings
    RCC->CFGR = cfgr;
    
    // startup pll and wait for it to be stable
    RCC->CR |= RCC_CR_PLLON;
    while( ( RCC->CR & RCC_CR_PLLRDY) == 0 );
    
    // Switch sysclock to PLLCLK
    cfgr = RCC->CFGR;
    cfgr &= ~RCC_CFGR_SW_Msk;
    cfgr |= RCC_CFGR_SW_PLL;
    RCC->CFGR = cfgr;
    
    // Wait for switch to complete
    while( ( RCC->CFGR & RCC_CFGR_SWS_Msk) != (RCC_CFGR_SWS_PLL) );
}


#define _idx_str_lang 0
static const uint8_t str_language[] = 
{
    4,
    usb_string_descriptor,
    // English only
    0x09, 0x04, 
};

#define _addr_commnad 1
#define _addr_data  2

#define _idx_str_manufactur 1
#define _idx_str_product 2
#define _idx_str_serial 3

// https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt
#define _usb_vid 0x16c0
#define _usb_pid 0x05dc

#define _usb_device_major 1
#define _usb_device_minor 0

static const uint8_t device_descriptor[] = 
{
    18,   // bLength
    1,    // bDescriptorType
    0x00, 0x02, // bcdUSB
    0xFF, // bDeviceClass
    0xFF, // bDeviceSubClass
    0xFF, // bDeviceProtocol
    USB_EP0_MAX_PKG_SIZE, // bMaxPacketSize0
    _usb_vid & 0xff, (_usb_vid>>8) & 0xff, // idVendor
    _usb_pid & 0xff, (_usb_pid>>8) & 0xff, // idProduct
    _usb_device_minor, _usb_device_major,  // bcdDevice
    _idx_str_manufactur, // iManufacturer
    _idx_str_product,    // iProduct
    _idx_str_serial,     // iSerialNumber
    1 // bNumConfigurations
};


#define _len_desc_cfg   9
#define _len_desc_iface 9
#define _len_desc_ep 7
#define _num_logic_ep 4
static const uint8_t config_descriptor[] = 
{
    _len_desc_cfg, // bLength
    usb_config_descriptor,   // bDescriptorType
    _len_desc_cfg + _len_desc_iface + (_len_desc_ep * _num_logic_ep), 0, // wTotalLength
    1, // bNumInterfaces
    1, // bConfigurationValue
    0, // iConfiguration
    (1<<7 | 1<<6), // bmAttributes (we are bus powered)
    250, // bMaxPower Maximum Power Consumption in 2mA units -> 500mA we could have a puru-puru pack installed ...
    
    // Interface 0 descriptor
    _len_desc_iface, // bLength
    usb_interface_descriptor, // bDescriptorType
    0, // bInterfaceNumber
    0, // bAlternateSetting
    _num_logic_ep, // bNumEndpoints
    0xff, // bInterfaceClass
    0xff, // bInterfaceSubClass
    0xff, // bInterfaceProtocol
    0, // iInterface
    
    // EP1 is the RX command Bulk OUT endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (0<<7)|(_addr_commnad<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP2 is the TX status Bulk IN endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (1<<7)|(_addr_commnad<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP3 is the RX data Bulk OUT endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (0<<7)|(_addr_data<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP4 is the TX data Bulk IN endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (1<<7)|(_addr_data<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
};



static usb_ep_config* ep_command;
static usb_ep_config* ep_data;

// <ep-management>

static void ready_for_next_cmd()
{
    usb_rx_ready(ep_command, ep_command->rx_buffer_size);
    usb_tx_nak(ep_command);
}

static void no_data_tx()
{
    usb_tx_nak(ep_data);
}

static void max_data_rx()
{
    usb_rx_ready(ep_data, ep_data->rx_buffer_size);
}

// </ep-management>

// <data-management>

static uint16_t data_tx_off = 0;
static uint16_t data_rx_off = 0;

#define maple_buffer_length  1024
static uint8_t maple_buffer[maple_buffer_length];

static inline uint16_t decode_msb_first_u16(uint8_t* d)
{
    return (((uint16_t)d[0]) << 8) | (uint16_t)d[1];
}

static uint16_t data_read_next()
{
    uint16_t r = sizeof(maple_sample_buffer) - data_rx_off;
    if( r > ep_data->tx_buffer_size )
        r = ep_data->tx_buffer_size;
    
    if( r == 0)
    {
        no_data_tx();
    }
    else
    {
        usb_tx_data(ep_data, r, maple_sample_buffer + data_rx_off);
        data_rx_off += r;
    }
    
    return r;
}

static uint16_t data_write_next(uint16_t length, uint8_t* data)
{
    max_data_rx();
    
    if( length < 1 )
        return 0;
    
    uint16_t w = sizeof(maple_buffer) - data_tx_off;
    if( w > length )
        w = length;
    
    if( w > 0)
    {
        memcpy(maple_buffer + data_tx_off, data, w);
        data_tx_off += w;
    }

    return w;
}


// </data-management>


// <commands>

typedef void (*cmd_hdl)(uint16_t, uint8_t*);

#define _max_cmd 8
static cmd_hdl valid_cmd[_max_cmd];

static void cmd_get_info(uint16_t length, uint8_t* data)
{
    dbg_uart_tx_str("nfo");
    
    uint8_t nfo[] = 
    {
        MAPLE_BUS_MASTER_INTERFACE_VERSION,
        (sizeof(maple_buffer) >> 8) & 0xff, sizeof(maple_buffer) & 0xff,
        (MAPLE_SAMPLE_LENGTH >> 8) & 0xff, MAPLE_SAMPLE_LENGTH & 0xff,
    };
    
    usb_tx_data(ep_command, sizeof(nfo), nfo);
    return;
}

static void cmd_write_tx_data(uint16_t length, uint8_t* data)
{
    dbg_uart_tx_str("wtx ");
    
    data_tx_off = length < 2 ? 0 : decode_msb_first_u16(data);
    
    dbg_uart_tx_hex_u16(data_tx_off);
}

static void cmd_read_sample_buffer(uint16_t length, uint8_t* data)
{
    dbg_uart_tx_str("rsb ");
    
    data_rx_off = length < 2 ? 0 : decode_msb_first_u16(data);
    
    dbg_uart_tx_hex_u16(data_rx_off);
    uint16_t r = data_read_next();
    dbg_uart_tx(' ');
    dbg_uart_tx_hex_u16(r);
}

static void cmd_execute(uint16_t length, uint8_t* data)
{
    dbg_uart_tx_str("ex ");
    
    if( length < 2 )
    {
        dbg_uart_tx('?');
        return;
    }
    
    uint16_t len = decode_msb_first_u16(data);

    dbg_uart_tx_hex_u16(len);

    if( len > sizeof(maple_buffer) )
        len = sizeof(maple_buffer);
    
    maple_send(len, maple_buffer);
    uint8_t status = maple_receive() == MAPLE_RX_TIMEOUT ? status_timeout : status_ok;

    usb_tx_data(ep_command, 1, &status);
    
    dbg_uart_tx('=');
    dbg_uart_tx_hex_u8(status);
    
    return;
}

static void cmd_copy_tx_to_rx(uint16_t length, uint8_t* data)
{
    dbg_uart_tx_str("tx2rx ");
    
    memcpy(maple_sample_buffer, maple_buffer, sizeof(maple_buffer));
    
    dbg_uart_tx_hex_u16(sizeof(maple_buffer));
}

// </commands>

// <ep-callbacks>

static void ep_command_rx(transfer_type type, uint16_t length, uint8_t* data)
{
    // First invalidate any data ready to be sent and setup for next command
    ready_for_next_cmd();
    
    dbg_uart_tx_str("cmd(");
    
    if( length < 1 )
    {
        dbg_uart_tx_str("n/a)\n");
        return;
    }
    
    uint8_t cmd = data[0];
    
    dbg_uart_tx_hex_u8(cmd);
    dbg_uart_tx(')');
    dbg_uart_tx(' ');
    
    if( cmd >= _max_cmd )
    {
        dbg_uart_tx('?');
        dbg_uart_tx('\n');
        return;
    }
    
    cmd_hdl c = valid_cmd[cmd];
    
    if( c == NULL )
    {
        dbg_uart_tx('I');
        dbg_uart_tx('\n');
        return;
    }
    
    c(length-1, data+1);
    dbg_uart_tx('\n');   
}

static void ep_command_tx_done()
{
    // NOOP
    dbg_uart_tx_str("ep CMD tx\n");
}

static void ep_command_reset()
{
    ready_for_next_cmd();
    dbg_uart_tx_str("ep CMD rst\n");
}

static void ep_data_reset()
{
    no_data_tx();
    max_data_rx();
    dbg_uart_tx_str("ep DATA rst\n");
}


static void ep_data_rx(transfer_type type, uint16_t length, uint8_t* data)
{
    dbg_uart_tx('w');
    dbg_uart_tx('+');
    uint16_t w = data_write_next(length, data);
    dbg_uart_tx_hex_u16(w);
    dbg_uart_tx('\n');
}

static void ep_data_tx_done()
{
    dbg_uart_tx('r');
    dbg_uart_tx('+');
    uint16_t r = data_read_next();
    dbg_uart_tx_hex_u16(r);
    dbg_uart_tx('\n');
}

// </ep-callbacks>

static void app_loop()
{
    while(1)
    {
        // NOOP
    }
}

static void command_hdl_init()
{
    memset(valid_cmd, 0, sizeof(valid_cmd));
    valid_cmd[command_get_info] = &cmd_get_info;
    valid_cmd[command_write_tx_data] = &cmd_write_tx_data;
    valid_cmd[command_read_sample_buffer] = &cmd_read_sample_buffer;
    valid_cmd[command_execute] = &cmd_execute;
    valid_cmd[command_copy_tx_to_rx] = &cmd_copy_tx_to_rx;
}

#define _unique_head_len 3
#define _unique_tail_len 1
#define _unique_serial_len 8
static char unique_serial[_unique_head_len + _unique_serial_len + _unique_serial_len];

// see reference manual "Unique device ID register (96 bits)"
#define _unique_device_id_len (96/8)
const uint8_t* device_id = (const uint8_t*)UID_BASE;

static void to_hex_string(char* hex_here, uint8_t* data, uint16_t data_len )
{
    for( int i=0; i<(data_len*2); ++i)
    {
        uint8_t n = data[i/2];
        
        if( (i & 0x1) == 0)
        {
            n >>= 4;
        }
        
        n &= 0x0f;
        
        hex_here[i] = ( n < 10 ) ? '0' + n : 'a' + (n-10);
    }
}

static void unique_serial_init()
{
    unique_serial[0] = 'M'; // Maple bus adapter
    unique_serial[1] = '1'; // Model 1
    unique_serial[2] = '-';
    
    uint8_t id_buf[_unique_serial_len/2];
    memset( id_buf, 0, sizeof(id_buf));
    
    for(int i=0; i<_unique_device_id_len; ++i)
    {
        id_buf[i%sizeof(id_buf)] ^= device_id[i];
    }
    
    to_hex_string( unique_serial + _unique_head_len, id_buf, sizeof(id_buf) );
    
    unique_serial[sizeof(unique_serial) - 1] = 0;
}

int main()
{
    ext_crystal_8mhz_72mhz();
    unique_serial_init();
    dbg_init();
    dbg_uart_tx_str("\n\nMain ");
    dbg_uart_tx_str(unique_serial);
    dbg_uart_tx('\n');
    
    maple_init();
    command_hdl_init();
    
    
    usb_init();
        
    usb_ep_config* ep0 = usb_new_ep();
    
    if( ep0 == NULL )
        die(5);
    
    usb_ep0_setup(ep0);
    usb_ep0_add_descriptor( usb_device_descriptor, 0, sizeof(device_descriptor), device_descriptor);
    usb_ep0_add_descriptor( usb_config_descriptor, 0, sizeof(config_descriptor), config_descriptor);
    usb_ep0_add_descriptor( usb_interface_descriptor, 0, sizeof(_len_desc_iface), config_descriptor + _len_desc_cfg);
    
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 0, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*0));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 1, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*1));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 2, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*2));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 3, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*3));
    
    usb_ep0_add_descriptor( usb_string_descriptor, _idx_str_lang, sizeof(str_language), str_language);
    // NOTE EMAIL must be define by who ever is compiling this, see Makefile
    usb_ep0_add_string_descriptor(_idx_str_manufactur, EMAIL);
    usb_ep0_add_string_descriptor(_idx_str_product, "MapleUSB");
    usb_ep0_add_string_descriptor(_idx_str_serial, unique_serial);
    
    
    // NOTE we are implementing the 4 logical enpoints as 2 physical (bidirectional) endpoints
    ep_command = usb_new_ep();
    if( ep_command == NULL )
        die(6);
    
    ep_data = usb_new_ep();
    if( ep_data == NULL )
        die(7);
    
    ep_command->address = _addr_commnad;
    ep_command->reset_handler = &ep_command_reset;
    ep_command->rx_handler = &ep_command_rx;
    ep_command->tx_done_handler = &ep_command_tx_done;
    ep_command->rx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep_command->tx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep_command->type = ep_type_bulk;
    

    ep_data->address = _addr_data;
    ep_data->reset_handler = &ep_data_reset;
    ep_data->rx_handler = &ep_data_rx;
    ep_data->tx_done_handler = &ep_data_tx_done;
    ep_data->rx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep_data->tx_buffer_size = USB_EP0_MAX_PKG_SIZE;
    ep_data->type = ep_type_bulk;
    
    
    usb_connect();
    
    while( usb_ep0_cfg_active() == USB_EP0_NO_CFG_ACTIVE );
    dbg_uart_tx_str("Config activated, go for app!\n");

    app_loop();
    
    return 0;
}
