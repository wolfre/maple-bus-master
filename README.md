# A Dreamcast Maple Bus to USB adapter

This project would not have been possible without prior work by:
- [Marcus Comstedt](http://mc.pp.se/dc/)
- [Dmitry Grinberg](http://dmitry.gr/index.php?r=05.Projects&proj=25.%20VMU%20Hacking) 
- [Nicholas FitzRoy-Dale](http://code.lardcave.net/2018/10/15/1/)
- [Niklas Gürtler](https://www.mikrocontroller.net/articles/USB-Tutorial_mit_STM32)

```
This project is in no way whatsoever associated with, or endorsed by, Sega. 
"Sega" and "Dreamcast" are trademarks of Sega Enterprises.
```

I wanted to manage and backup my VMUs.
There are options such as using [DreamShell](http://www.dc-swat.ru/page/dreamshell/) with a custom serial cable.
However I was intrigued by the low level Maple bus and wanted to play around with it.
Implementing something that send Maple bus packets / frames is rather easy, but receiving them is much harder than it first looks.
I tried a lot of things and failed, you can have a look [over in the attic](attic) for notes on this.

The implementation that works, and is in this repository is made of of three parts:
- An STM32F1 micro controller that does the low level sending and receiving.
  Data is exchanged with a host PC via USB.
  This controller implements the realtime critical part of the whole affair.
  The code is [over here](stm32f1) and is written for a ["Blue-Pill" board](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill.html).
  These boards can be found for around 5€ or less on eBay.
- A user space application running on the host PC, talking to the STM32 via USB ([libusb](https://libusb.info)).
  It offers a command interface on STDOUT / STDIN and is written in plain old c.
  It also does not do any kind of decoding or encoding logic, it just is a convenient way to talk to the STM32.
  This [*sampler*](usb-sampler) is not realtime critical and can run on any (Linux) computer with a USB host.
- A user space application runinng on the host PC, using the sampler to actually do something on the Maple bus.
  This can be found in the [MapleCom](MapleCom) folder and is written in C#/.NET.
  It targets .NET Framework 4.5 and runs well on Mono, however it should not contain any platform specific things, and so should run basically everywhere.
  This command line app does all the encoding and decoding of messages / frames and implements all the logic for acting as a Maple bus master.
  The above mentioned sampler outputs raw samples for responses recorded by the STM32.
  These are then also decoded by the .NET app into actual bit and bytes.

# Building one your self

*NOTE work in progress ...*

You need:
- an STM32 Blue-Pill board (or something similar like an [F103 Nucleo board](https://www.st.com/en/evaluation-tools/nucleo-f103rb.html))
- an st-link programmer / debugger (usually sold alongside Blue-Pill boards on eBay)
- a Dreamcast controller extension cable to connect the STM32 to your controller
- 2x 10k ohm resistors
- a bit of soldering experience, and tools like multimeter etc.
- somewhere to mount all of this, e.g. a bread broad, or possibly a [perfboard](https://en.wikipedia.org/wiki/Perfboard)

Very short build guide:
- Hardware (see also [here](stm32f1/maple.c)). 
  NOTE there does not seem to be a common color code with those Dreamcast extension cables, check that you wire up the right cable with a multimeter!
  - wire up the Data 1 line to GPIOA 3 and SPI1 MISO (GPIOA 6)
  - wire up the Data 5 line to GPIOA 2 and SPI2 MOSI (GPIOB 15)
  - directly connect SPI1 clock output (GPIOA 5) to SPI2 clock input (GPIOB 13)
  - wire up a 10k resistor from each of the data lines to 3.3v (pull-up)
  - wire up 5v and ground to the respective lines on the Dreamcast connector
- Software
  - Install the [stlinkt toolset](https://github.com/stlink-org/stlink).
    Connect the st-link programmer to your PC and the Blue-Pill to the programmer.
    Go into the *stm32f1* dir and run `make flash EMAIL="your@mail.here"`.
    You need to fill in your mail address which will become the USB manufacturer string.
    For details [see PID 0x05dc here](https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt).
  - Build the sampler: go into the *usb-sampler* dir and run `make` (make sure to have the libusb dev package installed).
  - Build the bus master app: open the *MapleCom/MapleCom.sln* in MonoDevelop or VisualStudio and build solution.
    It depends on [Binah](https://gitlab.com/wolfre/binah) for command line parsing, so put that package in your local nuget cache.

# Usage
 
With the build output from *MapleCom* and the *sampler* in one folder you can get some help:

```bash
mono BusMaster.exe
```

To dump a complete 128k VMU you can do (should take around 50 seconds):

```bash
mono BusMaster.exe dump-vmu --vmu-file my-vmu.raw
```

To write a complete 128k VMU back you can do (should take around 200 seconds):

```bash
mono BusMaster.exe write-vmu --vmu-file my-vmu.raw
```

Writing is slow and will also wear out the flash a bit more than needed.
You can speed this up by only writing blocks that actually changed.
So lets say you want to manipulate some save game and write back only the changes, you could do this:
- dump the VMU to *my-vmu-orig.raw*
- create a copy of that file to *my-vmu-changed.raw*
- manipulate / edit *my-vmu-changed.raw* however you like
- write back only the changes from *my-vmu-changed.raw*

To only write changes you do:

```bash
mono BusMaster.exe write-vmu --vmu-file my-vmu-changed.raw --vmu-delta-file my-vmu-orig.raw
```

NOTE: *BusMaster.exe* does not care what changed from *my-vmu-orig.raw* -> *my-vmu-changed.raw*.
It will simple look for any change and only wrote those to the VMU.
So you can also use this to flash a completely different VMU image:
- *--vmu-file* is then the image of some other VMU you want to flash
- *--vmu-delta-file* is then the current VMU state

This will most likely not be able to skip many parts, as the two VMU images are really different.
However even skipping like 20% will speed things up and result in less flash wear.

