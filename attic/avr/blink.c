#include <stdint.h>
#include <avr/io.h>

#define DDR_SPI  DDRB
#define PORT_SPI PORTB
#define DD_MOSI 3
#define DD_SCK  5
#define DD_SS   2

#define nop() asm volatile("nop")



void init_spi_master()
{
    // Set MOSI and SCK output, all others input 
    DDR_SPI = (1<<DD_MOSI)|(1<<DD_SCK)|(1<<DD_SS);
    
    
    //SPSR |= (1<<SPI2X);
    // Enable SPI, Master
    SPCR = (1<<SPE) | (1<<MSTR) | (0<<SPR1) | (0<<SPR0) | (1<<CPHA);
}

void send_test_pattern()
{
    uint8_t buf = 0x81;
    SPDR = 0;
    
    while(1)
    {
        while(!(SPSR & (1<<SPIF)));
        
        PORT_SPI ^= 1<<DD_SS;
        SPDR = buf;
        
        buf ^= 0xff;
    }
}

#define DATA1  0
#define DATA5  1
#define DBG    2
#define CNT_D5     TCNT0
#define CNT_D1     TCNT1L
#define MAPLE_PIN  PINC

#define dbg_low()     PORTC = 0
#define dbg_hi()      PORTC = 1<<DBG
#define dbg_toggle()  { dbg_low(); dbg_hi(); }

void debug_toggle()
{
    DDRC = 1<<DBG;
    while(1)
    {
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
        dbg_toggle();
    }
}

void debug_toggle_u8_lsb_first(uint8_t d)
{
    for(uint8_t i=0; i<8; ++i)
    {
        dbg_hi();
        if( d&1 )
        {
            nop();
            nop();
            nop();
            nop();
            nop();
            nop();
            nop();
        }
        dbg_low();
        d>>=1;
    }
}

uint8_t mem[512];

void u8_send(uint8_t d)
{
    while(!(SPSR & (1<<SPIF)));
    PORT_SPI ^= 1<<DD_SS;
    SPDR = d;
}

void wait_for_blank_maple()
{
    uint8_t start_sync = 0;
    while(start_sync != 50)
    {
        if( (MAPLE_PIN & ((1<<DATA1)|(1<<DATA5)) ) == ((1<<DATA1)|(1<<DATA5)) )
        {
            ++start_sync;
        }
        else
        {
            start_sync = 0;
        }
    }
}

void send_mapel_4()
{
    DDRC = 1<<DBG;
    PORTC= 0;
    
    // http://mc.pp.se/dc/maplewire.html
    
    while(1)
    {
        uint8_t* d = mem - 1;
        
        wait_for_blank_maple();

        while( PINC & (1<<DATA1) ) ;
        dbg_hi();
        
        do
        {
            ++d;
            (*d) = PINC;
            (*d) <<=2;
            
            (*d) |= PINC & 3;
            (*d) <<=2;
            
            (*d) |= PINC & 3;
            (*d) <<=2;
            
            (*d) |= PINC & 3;
            dbg_low();
        }
        while((*d)!=0xff);
        
        
    }
}

void send_mapel_3()
{
    // setup counters to count data pins falling edges
    TCCR0= (1<<CS02) | (1<<CS01) | (0<<CS00);
    TCCR1B= (1<<CS12) | (1<<CS11) | (0<<CS10);
    DDRC = 1<<DBG;
    
    while(1)
    {
        uint8_t tmp = 0;
        wait_for_blank_maple();
        
        CNT_D1 = 0;
        CNT_D5 = 0;
        
        while( CNT_D1 == 0);
        CNT_D1 = 0;
        while( CNT_D5 != 5);
        CNT_D5 = 0;
        
        
        while(1)
        {
            dbg_low();
            
            if( CNT_D1 & 1 )
            {
                dbg_hi();
            }
        }
        
        /*
        
        
        
        while( CNT_D1 == 0);
        
        dbg_hi();
        if( MAPLE_PIN & (1<<DATA5) )
        {
            tmp |= 1;
        }
        
        CNT_D1 = 0;
        
        //tmp <<= 1;
        
        
        while( CNT_D5 == 0);
        
        dbg_low();
        if( MAPLE_PIN & (1<<DATA1) )
        {
            tmp |= 1;
        }
        
        CNT_D5 = 0;
        
        //tmp <<= 1;
        
        */
        
        //debug_toggle_u8_lsb_first(tmp>>1);
    }
    

    
}


void send_mapel_2()
{
    DDRC = 1<<DBG;
    PORTC= 0;
    
    // http://mc.pp.se/dc/maplewire.html
    
    while(1)
    {
        wait_for_blank_maple();

        
        while( PINC & (1<<DATA1) ) ;
        
        
        dbg_hi();
        
        
        
        uint8_t tmp = 0;
        uint16_t i = 0xffff;
        
        while(tmp != 0xff)
        {
            tmp = PINC;
            tmp <<=2;
            
            tmp |= PINC & 3;
            tmp <<=2;
            
            ++i;
            
            tmp |= PINC & 3;
            tmp <<=2;
            
            tmp |= PINC & 3;
            mem[i] = tmp;
        }
        
        SPDR = 0;
        
        for( uint16_t j = 0; j < 100; ++j )
            u8_send(0);
        
        u8_send(0x55);
        u8_send((i>>8)&0xff);
        u8_send(i&0xff);
        u8_send(0x55);
        u8_send(0);
        
        for( uint16_t j = 0; j <= i; ++j )
        {
            u8_send(mem[j]);
        }
        
        dbg_low();
    }
}

void send_mapel()
{
    DDRC = 1<<DBG;
    PORTC= 0;

    uint8_t last_state = PINC & 0x3;
    uint8_t current = last_state;
    

    // NOTE sampling is fast enough, but TX via SPI/I2S isn't ...
    //   At 18MHz we are about 70% realtime ...
    
    while(1)
    {
        PORTC ^= 1<<DBG;
        
        while(last_state == current )
            current = PINC & 0x3;
        
        uint8_t buf = current;
        last_state = current;
        
        
        while(last_state == current )
            current = PINC & 0x3;
        
        buf <<= 2;
        buf |= current;
        last_state = current;
        
        while(last_state == current )
            current = PINC & 0x3;
        
        PORTC = 0;
        
        buf <<= 2;
        buf |= current;
        last_state = current;
        
        while(last_state == current )
            current = PINC & 0x3;
        
        buf <<= 2;
        buf |= current;
        last_state = current;
        
        PORT_SPI ^= 1<<DD_SS;
        SPDR = buf;
    }
}

int main()
{
    init_spi_master();

    //debug_toggle();
    send_mapel_4();
    
}
