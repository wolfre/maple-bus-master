#include "common.h"
#include "pwm.h"
#include "bcm-hw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// usleep
#include <unistd.h>

static void setup_pwm(pwm* me)
{
    // disable all pwm channels, see CTL register description BCM2835 Peripherials
    me->pwm_reg->CTL = 0;
    
    // needs some time until the PWM module gets disabled, without the delay the PWM module supposedly crashes
    usleep(100); 

    // Clear status
    me->pwm_reg->STA = 0;
    me->pwm_reg->STA |= (1<<9);
    me->pwm_reg->STA = 0;
    
    // 50% duty cycle
    me->pwm_reg->RNG1 = 10;
    me->pwm_reg->DAT1 = 5;
}


pwm* pwm_new(uint8_t gpio_port, uint32_t frequency_hz)
{
    pwm* o = malloc(sizeof(pwm));
    memset(o, 0, sizeof(pwm));
    
    
    o->gpio_port = gpio_port;
    o->frequency_hz = frequency_hz;
    
    if( gpio_port != 12 )
    {
        die_0("Only GPIO 12 is supported");
    }
    
    o->gpio_reg  = bcm_register_map(BCM_REG_GPIO_OFF, sizeof(bcm_reg_gpio));
    o->pwm_reg   = bcm_register_map(BCM_REG_PWM_OFF, sizeof(bcm_reg_pwm));
    o->clock_reg = bcm_register_map(BCM_REG_CLOCK_OFF, sizeof(bcm_reg_clock_control));
    
    
    bcm_set_gpio_function(o->gpio_reg, gpio_port, GPIO_PIN_FUNCTION_ALT0);
    
    // We need 2 clock cycles per PWM cycle, one for high one for low, aka 50% duty cycle
    bcm_start_clock_plld("PWM", &(o->clock_reg->CM_PWMCTL), &(o->clock_reg->CM_PWMDIV), frequency_hz * 2);
    
    setup_pwm(o);
    
    return o;
}

void pwm_delete(pwm* me)
{
    pwm_stop(me);
    bcm_stop_clock("PWM",  &(me->clock_reg->CM_PWMCTL), &(me->clock_reg->CM_PWMDIV) );
    bcm_set_gpio_function(me->gpio_reg, me->gpio_port, GPIO_PIN_FUNCTION_INPUT);
    bcm_register_unmap(me->clock_reg, sizeof(bcm_reg_clock_control));
    bcm_register_unmap(me->pwm_reg, sizeof(bcm_reg_pwm));
    bcm_register_unmap(me->gpio_reg, sizeof(bcm_reg_gpio));
    free(me);
}

void pwm_start(pwm* me)
{
    me->pwm_reg->CTL = (1<<3) | 1;
}

void pwm_stop(pwm* me)
{
    me->pwm_reg->CTL = 0;
}

