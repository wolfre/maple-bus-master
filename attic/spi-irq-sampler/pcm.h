#ifndef _PCM_H
#define _PCM_H

#include "bcm-registers.h"
#include <stdint.h>

typedef struct
{
    volatile bcm_reg_gpio* gpio_reg;
    volatile bcm_reg_pcm* pcm_reg;
    volatile bcm_reg_clock_control* clock_reg;
    uint64_t fill_call_count;
    uint64_t fill_action_count;
}
pcm;

pcm* pcm_new();
void pcm_delete(pcm* object);

void pcm_fill_tx(pcm* object, uint8_t ch0, uint8_t ch1);
double pcm_get_idle_time(pcm* object);

typedef struct
{
    uint8_t fill;
    uint32_t frames[64];
}
pcm_fifo;

void pcm_read_fifo(pcm* object, pcm_fifo* fifo);


#endif
