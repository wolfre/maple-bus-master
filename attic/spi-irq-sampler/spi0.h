#ifndef _SPI0_H
#define _SPI0_H

#include "bcm-registers.h"
#include <stdint.h>

typedef struct
{
    volatile bcm_reg_gpio* gpio_reg;
    volatile bcm_reg_spi0* spi_reg;
}
spi0;


spi0* spi0_new();
void spi0_delete(spi0* object);

void spi0_send_one_fill(spi0* object, uint32_t pattern);



#endif
