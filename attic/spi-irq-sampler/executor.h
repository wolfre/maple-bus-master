#ifndef _EXECUTOR_H
#define _EXECUTOR_H

#include <stdio.h>
#include "bitbang-maple.h"

typedef struct
{
    FILE* input;
    FILE* output;
    bitbang_maple* maple;
    uint8_t* buffer;
}
executor;


executor* executor_new(FILE* input, FILE* output);
void executor_delete(executor* object);

void executor_run(executor* object);


#endif
