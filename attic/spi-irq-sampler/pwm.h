#ifndef _PWM_H
#define _PWM_H

#include "bcm-registers.h"
#include <stdint.h>

typedef struct
{
    uint8_t  gpio_port;
    uint32_t frequency_hz;
    
    volatile bcm_reg_gpio* gpio_reg;
    volatile bcm_reg_pwm* pwm_reg;
    volatile bcm_reg_clock_control* clock_reg;
}
pwm;


pwm* pwm_new(uint8_t gpio_port, uint32_t frequency_hz);
void pwm_delete(pwm* object);

void pwm_start(pwm* object);
void pwm_stop(pwm* object);


#endif
