
#include "common.h"
#include "bcm-hw.h"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/mman.h>

#include <sys/stat.h>

#include <unistd.h>

// Lots stolen from here https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjqvNOhuYTtAhU7DWMBHfCBDPoQFjAGegQIBxAC&url=http%3A%2F%2Fwww.frank-buss.de%2Fraspberrypi%2Fpwm.c&usg=AOvVaw3IY20FlbNkSdyb5pURFalx
// https://github.com/hzeller/rpi-gpio-dma-demo/blob/master/gpio-dma-test.c

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

#define _SOC_RANGES "/proc/device-tree/soc/ranges"


static off_t bcm_register_base()
{
    int fd = open(_SOC_RANGES, O_RDONLY|O_SYNC);
    if (fd < 0)
    {
        die_0("can't open " _SOC_RANGES);
    }
 
    uint8_t buf[8];
    
    //ssize_t read(int fd, void *buf, size_t count);
    if( read( fd, buf, sizeof(buf) ) != sizeof(buf) )
    {
        close(fd);
        die_0("failed to read register offsets from " _SOC_RANGES);
    }
    
    close(fd);
    
    // the second word is what we are looking for, its stored MSB first: 
    // "???? ???? 3f00 0000" -> 0x3f000000
    
    off_t base = 0;
    
    base |= buf[4];
    base <<=8;
    base |= buf[5];
    base <<=8;
    base |= buf[6];
    base <<=8;
    base |= buf[7];
    
    return base;
}

static size_t round_to_page_size(size_t size)
{
    size /= PAGE_SIZE;
    size += 1;
    size *= PAGE_SIZE;
    return size;
}

volatile void * bcm_register_map(off_t register_offset, size_t size_of_register)
{
    int mem_fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (mem_fd < 0)
    {
        die_0("can't open /dev/mem: You need to run this as root!");
    }

    
    off_t address = bcm_register_base() + register_offset;
    
    uint32_t *result = (uint32_t*) mmap(
        NULL,                  // Any adddress in our space will do
        round_to_page_size(size_of_register),
        PROT_READ|PROT_WRITE,  // Enable r/w on GPIO registers.
        MAP_SHARED,
        mem_fd,                // File to map
        address                // Address of bcm register
    );
    
    close(mem_fd);

    if (result == MAP_FAILED)
    {
        fprintf(stderr, "mmap returned %p\n", result);
        die_0("mmap error");
    }
    
    return result;
}

void bcm_register_unmap(volatile void * reg, size_t size_of_register)
{
    // https://nelkinda.com/blog/suppress-warnings-in-gcc-and-clang/#d11e283
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    
    // https://linux.die.net/man/2/munmap
    if( munmap(reg, round_to_page_size(size_of_register)) == 0)
        return;
    
    #pragma GCC diagnostic pop
    
    die_0("munmap error");
}

#define print_reg_kv_32(name,value) printf("    \"%s\":\"0x%08x\"\n", name, value);

void bcm_reg_pwm_print(volatile bcm_reg_pwm * reg, const char * name)
{

    printf("\"%s\":{\n", name);
    
    print_reg_kv_32("CTL", reg->CTL);
    print_reg_kv_32("STA", reg->STA);
    print_reg_kv_32("DMAC", reg->DMAC);
    print_reg_kv_32("_unknown_0x0c", reg->_unknown_0x0c);
    print_reg_kv_32("RNG1", reg->RNG1);
    print_reg_kv_32("DAT1", reg->DAT1);
    print_reg_kv_32("FIF1", reg->FIF1);
    print_reg_kv_32("_unknown_0x1c", reg->_unknown_0x1c);
    print_reg_kv_32("RNG2", reg->RNG2);
    print_reg_kv_32("DAT2", reg->DAT2); 
    
    printf("}\n");
}

void bcm_reg_pcm_print(volatile bcm_reg_pcm * reg, const char * name)
{
    printf("\"%s\":{\n", name);
    
    print_reg_kv_32("CS_A", reg->CS_A);
    print_reg_kv_32("FIFO_A", reg->FIFO_A);
    print_reg_kv_32("MODE_A", reg->MODE_A);
    print_reg_kv_32("RXC_A", reg->RXC_A);
    print_reg_kv_32("TXC_A", reg->TXC_A);
    print_reg_kv_32("DREQ_A", reg->DREQ_A);
    print_reg_kv_32("INTEN_A", reg->INTEN_A);
    print_reg_kv_32("INTSTC_A", reg->INTSTC_A);
    print_reg_kv_32("GRAY", reg->GRAY);
    
    printf("}\n");
}

#undef print_reg_kv_32

void bcm_set_gpio_function(volatile bcm_reg_gpio * gpio, uint8_t gpio_port, uint8_t function)
{
    // see "Table 6-2 – GPIO Alternate function select register 0" from "BCM2835 Peripherials"
    // each GPFSELx register has 10 GPIOs with 3 bits each starting from GPIO0 at bit 0 of GPFSEL0
    
    if( gpio_port > 49 )
    {
        die_0("Max GPIO port is 49");
    }
    
    int gpfsel = gpio_port / 10;
    int entry = gpio_port % 10;
    
    volatile uint32_t* reg = &(gpio->GPFSEL0);
    reg += gpfsel;
    
    // For some reason the internet says we have to always set to input before we can do anything else with the pin ...
    (*reg) = (*reg) & ~((0x7) << (entry*3));
    // Now set the function we actually wanted
    (*reg) = (*reg) | (function & 0x7) << (entry*3);
}

double bcm_get_plld_divider(uint32_t target_freq_hz)
{
    double div = BCM_CLOCK_PLLD_HZ;
    div /= (double)target_freq_hz;
    return div;
}

uint32_t bcm_to_clock_divider(double divider)
{
    uint32_t divider_reg = (uint32_t)divider;
    divider -= divider_reg;
    divider *= 1024.0;
    divider_reg <<= 12;
    divider_reg |= (uint32_t)divider;
    
    return divider_reg;
}

void bcm_start_clock_plld(const char * name, volatile uint32_t * reg_clock_ctl, volatile uint32_t * reg_clock_div, uint32_t frequency_hz)
{
    bcm_stop_clock(name, reg_clock_ctl, reg_clock_div);
    
    double div = bcm_get_plld_divider(frequency_hz);
    (*reg_clock_div) = BCM_REG_CLOCK_PASSWORD | bcm_to_clock_divider(div);
    (*reg_clock_ctl) = BCM_REG_CLOCK_PASSWORD | BCM_REG_CLOCK_SRC_PLLD;
    
    //printf("Will start PWM clock with 0x%08x\n", (*reg_clock_ctl));

    // Enable clock.
    // NOTE this will actually block forever / never turn on in case the GPIO pin this PWM goes to
    //   is not set to the correct ALT function BEFORE doing this ... no clue why
    (*reg_clock_ctl) = BCM_REG_CLOCK_PASSWORD | BCM_REG_CLOCK_SRC_PLLD | (1<<BCM_REG_CLOCK_ENABLE);

    // Wait for busy flag to turn on.
    while( ((*reg_clock_ctl) & (1 << BCM_REG_CLOCK_BUSY)) == 0 ) usleep(100);
        
    printf("%s clock started with 0x%08x\n", name, (*reg_clock_ctl));
}

void bcm_stop_clock(const char * name, volatile uint32_t * reg_clock_ctl, volatile uint32_t * reg_clock_div)
{   
    if( (*reg_clock_ctl) & (1 << BCM_REG_CLOCK_BUSY) )
    {
        printf("%s clock is busy will stop it\n", name);
        
        // Turn off enable flag.
        (*reg_clock_ctl) = BCM_REG_CLOCK_PASSWORD | u32_bit_clear((*reg_clock_ctl), BCM_REG_CLOCK_ENABLE);
        // Wait for busy flag to turn off.
        while((*reg_clock_ctl) & (1<<BCM_REG_CLOCK_BUSY));
    }

    printf("%s clock stopped\n", name);
}

#undef _SOC_RANGES
#undef PAGE_SIZE
#undef BLOCK_SIZE
