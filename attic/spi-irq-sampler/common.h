#ifndef _COMMON_H
#define _COMMON_H

#include <stdio.h>

#define die_0(msg)  { perror(msg); exit(-1); }
#define die_1(fmt, arg1)  { fprintf(stderr, fmt "\n", arg1); exit(-1); }


#define GPIO_AVR_CLOCK 12
#define GPIO_DATA_1 23
#define GPIO_DATA_5 24

#endif
