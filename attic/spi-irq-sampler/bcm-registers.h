#ifndef _BCM_REGISTERS_H
#define _BCM_REGISTERS_H

#include <stdint.h>

// NOTE your Raspberry PI peripherials base address varies depending on the exact model.
//   A lot of statements on the internet are misleading or plain wrong. However
//   you can ask your system about this, have a look at /proc/device-tree/soc/ranges
//   The second 4-byte word will contain the base address you are looking for, like so
//   cat /proc/device-tree/soc/ranges | head -c 8 | tail -c 4 | xxd
//   All the offsets in here are an offset to this very address.
//   You may memory map such an base+offset, and cast it into the respective register
//   structure. Make sure to use volatile pointer for these addesses.

// <gpio source="BCM2835 Peripherials section 6.1">

#define BCM_REG_GPIO_OFF 0x200000

typedef struct
{
    uint32_t GPFSEL0;
    uint32_t GPFSEL1;
    uint32_t GPFSEL2;
    uint32_t GPFSEL3;
    uint32_t GPFSEL4;
    uint32_t GPFSEL5;
    uint32_t _reserved_0x18;
    uint32_t GPSET0;
    uint32_t GPSET1;
    uint32_t _reserved_0x24;
    uint32_t GPCLR0;
    uint32_t GPCLR1;
    uint32_t _reserved_0x30;
    uint32_t GPLEV0;
    uint32_t GPLEV1;
    uint32_t _reserved_0x3c;
    uint32_t GPEDS0;
    uint32_t GPEDS1;
    uint32_t _reserved_0x48;
    uint32_t GPREN0;
    uint32_t GPREN1;
    uint32_t _reserved_0x54;
    uint32_t GPFEN0;
    uint32_t GPFEN1;
    uint32_t _reserved_0x60;
    uint32_t GPHEN0;
    uint32_t GPHEN1;
    uint32_t _reserved_0x6c;
    uint32_t GPLEN0;
    uint32_t GPLEN1;
    uint32_t _reserved_0x78;
    uint32_t GPAREN0;
    uint32_t GPAREN1;
    uint32_t _reserved_0x84;
    uint32_t GPAFEN0;
    uint32_t GPAFEN1;
    uint32_t _reserved_0x90;
    uint32_t GPPUD;
    uint32_t GPPUDCLK0;
    uint32_t GPPUDCLK1;
    uint32_t _reserved_0x40;
    uint32_t _test_0xb0;
}
bcm_reg_gpio;

// Values for GPFSELxx see BCM2835 Peripherials table 6-3
#define GPIO_PIN_FUNCTION_INPUT  0
#define GPIO_PIN_FUNCTION_OUTPUT 1
#define GPIO_PIN_FUNCTION_ALT5   2
#define GPIO_PIN_FUNCTION_ALT4   3
#define GPIO_PIN_FUNCTION_ALT0   4
#define GPIO_PIN_FUNCTION_ALT1   5
#define GPIO_PIN_FUNCTION_ALT2   6
#define GPIO_PIN_FUNCTION_ALT3   7

// </gpio>

// <pwm source="BCM2835 Peripherials section 9.6">

#define BCM_REG_PWM_OFF 0x20C000
 
typedef struct
{
    uint32_t CTL;  // PWM Control
    uint32_t STA;  // PWM Status
    uint32_t DMAC; // PWM DMA Configuration
    // Undocumented, readonly and contains 0x70776d30 on a BCM2835 
    //  hints found here https://www.raspberrypi.org/forums/viewtopic.php?t=243166
    uint32_t _unknown_0x0c;
    uint32_t RNG1; // PWM Channel 1 Range
    uint32_t DAT1; // PWM Channel 1 Data
    uint32_t FIF1; // PWM FIFO Input
    // Undocumented, readonly and contains 0x70776d30 on a BCM2835 
    //  hints found here https://www.raspberrypi.org/forums/viewtopic.php?t=243166
    uint32_t _unknown_0x1c;
    uint32_t RNG2; // PWM Channel 2 Range
    uint32_t DAT2; // PWM Channel 2 Data
}
bcm_reg_pwm;

// </pwm>

// <pcm source="BCM2835 Peripherials section 8">

#define BCM_REG_PCM_OFF 0x203000

typedef struct
{
    uint32_t CS_A;     // PCM Control and Status
    uint32_t FIFO_A;   // PCM FIFO Data
    uint32_t MODE_A;   // PCM Mode
    uint32_t RXC_A;    // PCM Receive Configuration
    uint32_t TXC_A;    // PCM Transmit Configuration
    uint32_t DREQ_A;   // PCM DMA Request Level
    uint32_t INTEN_A;  // PCM Interrupt Enables
    uint32_t INTSTC_A; // PCM Interrupt Status & Clear
    uint32_t GRAY;     // PCM Gray Mode Control
}
bcm_reg_pcm;


#define BCM_PCM_CS_A_STBY   25
#define BCM_PCM_CS_A_SYNC   24
#define BCM_PCM_CS_A_RXSEX  23
#define BCM_PCM_CS_A_RXF    22
#define BCM_PCM_CS_A_TXE    21
#define BCM_PCM_CS_A_RXD    20
#define BCM_PCM_CS_A_TXD    19
#define BCM_PCM_CS_A_RXR    18
#define BCM_PCM_CS_A_TXW    17
#define BCM_PCM_CS_A_RXERR  16
#define BCM_PCM_CS_A_TXERR  15
#define BCM_PCM_CS_A_RXSYNC 14
#define BCM_PCM_CS_A_TXSYNC 13
#define BCM_PCM_CS_A_DMAEN   9
#define BCM_PCM_CS_A_RXTHR   7
#define BCM_PCM_CS_A_TXTHR   5
#define BCM_PCM_CS_A_RXCLR   4
#define BCM_PCM_CS_A_TXCLR   3
#define BCM_PCM_CS_A_TXON    2
#define BCM_PCM_CS_A_RXON    1
#define BCM_PCM_CS_A_EN      0

#define BCM_PCM_MODE_A_CLK_DIS 28
#define BCM_PCM_MODE_A_PDMN    27
#define BCM_PCM_MODE_A_PDME    26
#define BCM_PCM_MODE_A_FRXP    25
#define BCM_PCM_MODE_A_FTXP    24
#define BCM_PCM_MODE_A_CLKM    23
#define BCM_PCM_MODE_A_CLKI    22
#define BCM_PCM_MODE_A_FSM     21
#define BCM_PCM_MODE_A_FSI     20
#define BCM_PCM_MODE_A_FLEN    10
#define BCM_PCM_MODE_A_FSLEN    0

#define BCM_PCM_RXC_A_CH1WEX   31
#define BCM_PCM_RXC_A_CH1EN    30
#define BCM_PCM_RXC_A_CH1POS   20
#define BCM_PCM_RXC_A_CH1WID   16
#define BCM_PCM_RXC_A_CH2WEX   15
#define BCM_PCM_RXC_A_CH2EN    14
#define BCM_PCM_RXC_A_CH2POS    4
#define BCM_PCM_RXC_A_CH2WID    0

#define BCM_PCM_TXC_A_CH1WEX   31
#define BCM_PCM_TXC_A_CH1EN    30
#define BCM_PCM_TXC_A_CH1POS   20
#define BCM_PCM_TXC_A_CH1WID   16
#define BCM_PCM_TXC_A_CH2WEX   15
#define BCM_PCM_TXC_A_CH2EN    14
#define BCM_PCM_TXC_A_CH2POS    4
#define BCM_PCM_TXC_A_CH2WID    0


// </pcm>

// <dma>

#define BCM_REG_DMA0_BASE_OFF  0x007000
#define BCM_REG_DMA15_BASE_OFF 0xE05000

#define BCM_REG_DMA_CH_00_OFF (BCM_REG_DMA0_BASE_OFF + 0x000)
#define BCM_REG_DMA_CH_01_OFF (BCM_REG_DMA0_BASE_OFF + 0x100)
#define BCM_REG_DMA_CH_02_OFF (BCM_REG_DMA0_BASE_OFF + 0x200)
#define BCM_REG_DMA_CH_03_OFF (BCM_REG_DMA0_BASE_OFF + 0x300)
#define BCM_REG_DMA_CH_04_OFF (BCM_REG_DMA0_BASE_OFF + 0x400)
#define BCM_REG_DMA_CH_05_OFF (BCM_REG_DMA0_BASE_OFF + 0x500)
#define BCM_REG_DMA_CH_06_OFF (BCM_REG_DMA0_BASE_OFF + 0x600)
#define BCM_REG_DMA_CH_07_OFF (BCM_REG_DMA0_BASE_OFF + 0x700)
#define BCM_REG_DMA_CH_08_OFF (BCM_REG_DMA0_BASE_OFF + 0x800)
#define BCM_REG_DMA_CH_09_OFF (BCM_REG_DMA0_BASE_OFF + 0x900)
#define BCM_REG_DMA_CH_10_OFF (BCM_REG_DMA0_BASE_OFF + 0xa00)
#define BCM_REG_DMA_CH_11_OFF (BCM_REG_DMA0_BASE_OFF + 0xb00)
#define BCM_REG_DMA_CH_12_OFF (BCM_REG_DMA0_BASE_OFF + 0xc00)
#define BCM_REG_DMA_CH_13_OFF (BCM_REG_DMA0_BASE_OFF + 0xd00)
#define BCM_REG_DMA_CH_14_OFF (BCM_REG_DMA0_BASE_OFF + 0xe00)
#define BCM_REG_DMA_CH_15_OFF (BCM_REG_DMA15_BASE_OFF + 0xe00)

// A "Control Block Data Structure"
typedef struct
{
    uint32_t TI;        // Transfer Information
    uint32_t SOURCE_AD; // Source Address
    uint32_t DEST_AD;   // Destination Address
    uint32_t TXFR_LEN;  // Transfer Length
    uint32_t STRIDE;    // 2D Mode Stride
    uint32_t NEXTCONBK; // Next Control Block Address
    uint32_t N_A_06;    // Reserved – set to zero.
    uint32_t N_A_07;    // Reserved – set to zero.
}
bcm_reg_dma_control_block;

typedef struct
{
    uint32_t CS; // Control and Status
    uint32_t CONBLK_AD; // Control Block Address
    
    uint32_t TI;        // Transfer Information (readonly copy)
    uint32_t SOURCE_AD; // Source Address       (readonly copy)
    uint32_t DEST_AD;   // Destination Address  (readonly copy)
    uint32_t TXFR_LEN;  // Transfer Length      (readonly copy)
    uint32_t STRIDE;    // 2D Mode Stride       (readonly copy)
    uint32_t NEXTCONBK; // Next Control Block Address  (readonly copy)
    
    uint32_t DEBUG; // Control Block Address
}
bcm_reg_dma_channel;

typedef struct
{
    uint32_t CS; // Control and Status
    uint32_t CONBLK_AD; // Control Block Address
    
    uint32_t TI;        // Transfer Information (readonly copy)
    uint32_t SOURCE_AD; // Source Address       (readonly copy)
    uint32_t DEST_AD;   // Destination Address  (readonly copy)
    uint32_t TXFR_LEN;  // Transfer Length      (readonly copy)
    uint32_t _reserved_0x18; // Light channels do not have a STRIDE / 2D feature
    uint32_t NEXTCONBK; // Next Control Block Address  (readonly copy)
    
    uint32_t DEBUG; // Control Block Address
}
bcm_reg_dma_channel_light;

typedef struct
{
    bcm_reg_dma_channel channel00;
    uint32_t _unknown_0x024[55];
    
    bcm_reg_dma_channel channel01;
    uint32_t _unknown_0x124[55];
    
    bcm_reg_dma_channel channel02;
    uint32_t _unknown_0x224[55];
    
    bcm_reg_dma_channel channel03;
    uint32_t _unknown_0x324[55];
    
    bcm_reg_dma_channel channel04;
    uint32_t _unknown_0x424[55];
    
    bcm_reg_dma_channel channel05;
    uint32_t _unknown_0x524[55];
    
    bcm_reg_dma_channel channel06;
    uint32_t _unknown_0x624[55];
    
    bcm_reg_dma_channel_light channel07;
    uint32_t _unknown_0x724[55];
    
    bcm_reg_dma_channel_light channel08;
    uint32_t _unknown_0x824[55];
    
    bcm_reg_dma_channel_light channel09;
    uint32_t _unknown_0x924[55];
    
    bcm_reg_dma_channel_light channel10;
    uint32_t _unknown_0xa24[55];
    
    bcm_reg_dma_channel_light channel11;
    uint32_t _unknown_0xb24[55];
    
    bcm_reg_dma_channel_light channel12;
    uint32_t _unknown_0xc24[55];
    
    bcm_reg_dma_channel_light channel13;
    uint32_t _unknown_0xd24[55];
    
    bcm_reg_dma_channel_light channel14;
    uint32_t _unknown_0xe24[55];
    
    uint32_t _unknown_0xf00[56];
    uint32_t INT_STATUS; // Interrupt status of each DMA channel
    uint32_t _unknown_0xf04[3];
    uint32_t ENABLE;     // Global enable bits for each DMA channel
}
bcm_reg_dma;

// </dma>

// <spi0>

#define BCM_REG_SPI0_OFF 0x204000

typedef struct
{
    uint32_t CS;   // SPI Master Control and Status
    uint32_t FIFO; // SPI Master TX and RX FIFOs
    uint32_t CLK;  // SPI Master Clock Divider
    uint32_t DLEN; // SPI Master Data Length
    uint32_t LTOH; // SPI LOSSI mode TOH
    uint32_t DC;   // SPI DMA DREQ Controls
}
bcm_reg_spi0;

// </spi0>

// <timer source="12 System Timer">

#define BCM_REG_TIMER_OFF 0x003000

typedef struct
{
    uint32_t CS;  // System Timer Control/Status
    uint32_t CLO; // System Timer Counter Lower 32 bits
    uint32_t CHI; // System Timer Counter Higher 32 bits
    uint32_t C0;  // System Timer Compare 0
    uint32_t C1;  // System Timer Compare 1
    uint32_t C2;  // System Timer Compare 2
    uint32_t C3;  // System Timer Compare 3
}
bcm_reg_timer;

// </timer>

// <clock 
//   source="https://pdfslide.us/documents/bcm2835-audio-clocks.html"
//   alt="https://www.google.com/search?q=%22BCM2835+Audio+%26+PWM+clocks%22&oq=%22BCM2835+Audio+%26+PWM+clocks%22"
//   alt="https://www.raspberrypi.org/forums/viewtopic.php?t=37770"
//   >


#define BCM_REG_CLOCK_OFF 0x101000

typedef struct
{
    uint32_t _unknown_0x00[38];
    uint32_t CM_PCMCTL;
    uint32_t CM_PCMDIV;
    uint32_t CM_PWMCTL;
    uint32_t CM_PWMDIV;
}
bcm_reg_clock_control;

#define BCM_REG_CLOCK_PASSWORD 0x5a000000 

#define BCM_REG_CLOCK_SRC_GND        0
#define BCM_REG_CLOCK_SRC_OSCILLATOR 1
#define BCM_REG_CLOCK_SRC_TESTDEBUG0 2
#define BCM_REG_CLOCK_SRC_TESTDEBUG1 3
#define BCM_REG_CLOCK_SRC_PLLA       4
#define BCM_REG_CLOCK_SRC_PLLC       5
#define BCM_REG_CLOCK_SRC_PLLD       6
#define BCM_REG_CLOCK_SRC_HDMI_AUX   7

#define BCM_REG_CLOCK_ENABLE 4
#define BCM_REG_CLOCK_KILL   5
#define BCM_REG_CLOCK_BUSY   7
#define BCM_REG_CLOCK_FLIP   8
#define BCM_REG_CLOCK_MASH   9

// https://pinout.xyz/pinout/gpclk#
#define BCM_CLOCK_OSCILLATOR_HZ  19200000
#define BCM_CLOCK_PLLD_HZ       500000000
#define BCM_CLOCK_HDMI_AUX_HZ   216000000

// </clock>

#endif
