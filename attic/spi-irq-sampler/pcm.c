#include "common.h"
#include "pcm.h"
#include "bcm-hw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// usleep
#include <unistd.h>


// refrences:
// /DS/  "BCM2835 Peripherials"

static void setup_pcm(pcm* me)
{
    uint8_t ch0_width_bits = 8;
    uint8_t ch1_width_bits = 8;
    
    // Acroding to /DS/ we should enable the module but not set anything first:
    me->pcm_reg->CS_A = 1 << BCM_PCM_CS_A_EN;
    
    // Now we set all the things
    
    me->pcm_reg->MODE_A = \
        (1<<BCM_PCM_MODE_A_FRXP) | \
        (1<<BCM_PCM_MODE_A_FTXP) | \
        ( ( ch0_width_bits + ch1_width_bits - 1 ) << BCM_PCM_MODE_A_FLEN ) | \
        ( ( ch0_width_bits ) << BCM_PCM_MODE_A_FSLEN ) | \
        (1<<BCM_PCM_MODE_A_CLKM) | \
        (1<<BCM_PCM_MODE_A_FSM);
        
    me->pcm_reg->TXC_A = \
        (1<<BCM_PCM_TXC_A_CH1EN) | \
        (1<<BCM_PCM_TXC_A_CH2EN) | \
        (0<<BCM_PCM_TXC_A_CH1POS) | \
        (ch0_width_bits<<BCM_PCM_TXC_A_CH2POS) | \
        ( (ch0_width_bits-8) << BCM_PCM_TXC_A_CH1WID ) | \
        ( (ch1_width_bits-8) << BCM_PCM_TXC_A_CH2WID );
        
    me->pcm_reg->RXC_A = \
        (1<<BCM_PCM_TXC_A_CH1EN) | \
        (1<<BCM_PCM_TXC_A_CH2EN) | \
        (0<<BCM_PCM_TXC_A_CH1POS) | \
        (ch0_width_bits<<BCM_PCM_TXC_A_CH2POS) | \
        ( (ch0_width_bits-8) << BCM_PCM_TXC_A_CH1WID ) | \
        ( (ch1_width_bits-8) << BCM_PCM_TXC_A_CH2WID );
    
    puts("A");
    
    // Then we clear fifos and set the sync bit to 1
    me->pcm_reg->CS_A = \
        (1<<BCM_PCM_CS_A_SYNC ) | \
        (1<<BCM_PCM_CS_A_RXCLR) | (1<<BCM_PCM_CS_A_TXCLR) | \
        (1<<BCM_PCM_CS_A_EN);
        
    puts("B");
        
    // Wait for sync bit to read 1
    while( (me->pcm_reg->CS_A & (1<<BCM_PCM_CS_A_SYNC )) == 0);

    puts("C");
    
    // Set thresh holds and start
    me->pcm_reg->CS_A = \
        (0<<BCM_PCM_CS_A_SYNC ) | \
        (0<<BCM_PCM_CS_A_TXTHR) | (2<<BCM_PCM_CS_A_RXTHR) | \
        (0<<BCM_PCM_CS_A_TXON)  | (1<<BCM_PCM_CS_A_RXON)  | \
        (1<<BCM_PCM_CS_A_EN);
}

static void stop_pcm(pcm* me)
{
    me->pcm_reg->CS_A = 0;
}

double pcm_get_idle_time(pcm* me)
{
    double r = (double)(me->fill_call_count - me->fill_action_count);
    r /= (double)me->fill_call_count;
    return r;
}

void pcm_fill_tx(pcm* me, uint8_t ch0, uint8_t ch1)
{
    uint32_t d = ch0 | ch1 << 16;
    int action = 0;
    
    while( me->pcm_reg->CS_A & (1<<BCM_PCM_CS_A_TXW) )
    {
        me->pcm_reg->FIFO_A = d;
        action = 1;
    }
    
    me->fill_call_count += 1;
    me->fill_action_count += action;
}

void pcm_read_fifo(pcm* me, pcm_fifo* fifo)
{
    fifo->fill = 0;
    
    while( me->pcm_reg->CS_A & (1<<BCM_PCM_CS_A_RXD) )
    {
        fifo->frames[fifo->fill] = me->pcm_reg->FIFO_A;
        fifo->fill += 1;
    }
}

// /DS/ 6.2 "Alternative Function Assignments"

#define GPIO_PCM_CLK  18
#define GPIO_PCM_FS   19
#define GPIO_PCM_DIN  20
#define GPIO_PCM_DOUT 21

// NOTE this is made up based on gut feeling
//   However we want to transmit about 4Mbit of data per second,
//   so I went with something 5-times higher than that ...
//   See /DS/ section 8.1 for a block diagram
#define PCM_CLOCK_HZ (5*1000*1000)

pcm* pcm_new()
{
    pcm* o = malloc(sizeof(pcm));
    memset(o, 0, sizeof(pcm));
    
    o->gpio_reg  = bcm_register_map(BCM_REG_GPIO_OFF, sizeof(bcm_reg_gpio));
    o->pcm_reg   = bcm_register_map(BCM_REG_PCM_OFF, sizeof(bcm_reg_pcm));
    o->clock_reg = bcm_register_map(BCM_REG_CLOCK_OFF, sizeof(bcm_reg_clock_control));
    
    bcm_set_gpio_function(o->gpio_reg, GPIO_PCM_CLK, GPIO_PIN_FUNCTION_ALT0);
    bcm_set_gpio_function(o->gpio_reg, GPIO_PCM_FS, GPIO_PIN_FUNCTION_ALT0);
    bcm_set_gpio_function(o->gpio_reg, GPIO_PCM_DIN, GPIO_PIN_FUNCTION_ALT0);
    bcm_set_gpio_function(o->gpio_reg, GPIO_PCM_DOUT, GPIO_PIN_FUNCTION_ALT0);

    bcm_start_clock_plld("PCM", &(o->clock_reg->CM_PCMCTL), &(o->clock_reg->CM_PCMDIV), PCM_CLOCK_HZ);
    setup_pcm(o);
    
    return o;
}

void pcm_delete(pcm* me)
{
    stop_pcm(me);
    
    bcm_stop_clock("PCM", &(me->clock_reg->CM_PCMCTL), &(me->clock_reg->CM_PCMDIV));
    
    bcm_set_gpio_function(me->gpio_reg, GPIO_PCM_DOUT, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, GPIO_PCM_DIN, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, GPIO_PCM_FS, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, GPIO_PCM_CLK, GPIO_PIN_FUNCTION_INPUT);
    
    bcm_register_unmap(me->clock_reg, sizeof(bcm_reg_clock_control));
    bcm_register_unmap(me->pcm_reg, sizeof(bcm_reg_pcm));
    bcm_register_unmap(me->gpio_reg, sizeof(bcm_reg_gpio));
    free(me);
}

