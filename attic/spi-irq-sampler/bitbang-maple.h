#ifndef _BITBANG_MAPLE_H
#define _BITBANG_MAPLE_H

#include "bcm-registers.h"
#include <stdint.h>

typedef struct
{
    uint8_t  gpio_port_data5;
    uint8_t  gpio_port_data1;
    
    uint32_t* buffer;
    volatile bcm_reg_gpio* gpio_reg;
    volatile bcm_reg_timer* timer_reg;
}
bitbang_maple;


typedef struct
{
    uint32_t sample_count;
    
    // each bit is one sample 
    uint8_t* samples_data1;
    uint8_t* samples_data5;
}
maple_packed_raw;

bitbang_maple* bitbang_maple_new(uint8_t gpio_port_data1, uint8_t gpio_port_data5);
void bitbang_maple_delete(bitbang_maple* object);

void bitbang_maple_time_delay(bitbang_maple* object);
void bitbang_maple_detect_yield(bitbang_maple* object);
void bitbang_maple_send(bitbang_maple* object, uint8_t* raw_data, uint32_t len);
maple_packed_raw* bitbang_maple_read_next_raw(bitbang_maple* object);
void maple_packed_raw_print(maple_packed_raw* raw);
void maple_packed_raw_print_compact(maple_packed_raw* raw);
void maple_packed_raw_delete(maple_packed_raw* raw);



#endif
