#include "common.h"
#include "bitbang-maple.h"
#include "bcm-hw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// usleep
#include <unistd.h>
// nanosleep
#include<time.h>   

#define gpio_hi(me, pin)  (me)->gpio_reg->GPSET0 = (1<<(pin))
#define gpio_low(me, pin) (me)->gpio_reg->GPCLR0 = (1<<(pin))

#define _dbg_pin 27
#define debug_hi(me)   gpio_hi(me, _dbg_pin)
#define debug_low(me)  gpio_low(me, _dbg_pin)

#define data1_hi(me)   gpio_hi(me, me->gpio_port_data1)
#define data1_low(me)  gpio_low(me, me->gpio_port_data1)
#define data5_hi(me)   gpio_hi(me, me->gpio_port_data5)
#define data5_low(me)  gpio_low(me, me->gpio_port_data5)

// timed on a pi1 b+ to be long enough for a complete page of memory
#define _buffer  (1024*6)

// https://www.raspberrypi.org/forums/viewtopic.php?t=243528
#define nop() __asm( "nop" : )


static void delay_ns(bitbang_maple* me, uint32_t t)
{
    // NOTE the timer is probably setup by the OS and does the scheduling and stuff
    //   The timer clock is of course also configured, so we don't know how fast it
    //   ticks. The division below was timed to be somewhere in the ballpark of t-ns.
    //   Div=100 on a pi 1 b+ (v1.5)
    uint32_t end = me->timer_reg->CLO + (t/100);
    
    // FIXME overflow
    
    while(me->timer_reg->CLO < end);
}

static int block_on_timer_ticks(bitbang_maple* me, uint32_t ticks)
{
    uint32_t timer = 0;
    uint32_t end = 0;
    
    do
    {
        timer = me->timer_reg->CLO;
        end = timer + ticks;
    }
    while( end < timer);
    
    int i = 0;
    while( me->timer_reg->CLO < end )
    {
        ++i;
    }
    
    return i;
}

static void align_to_next_yield_1(bitbang_maple* me)
{
    uint32_t timer_ticks = 5;
    int avg_cnt = 10;
    int avg = 0;
    
    
    for(int i=0; i<avg_cnt; ++i)
    {
        avg += block_on_timer_ticks(me, timer_ticks);
    }
    
    avg /= avg_cnt;
    
    int err = avg / 2;
    
    while(1)
    {
        int cnt = block_on_timer_ticks(me, timer_ticks);
        if( abs( avg - cnt ) > err )
            return;
    }
}

static void align_to_next_yield_2(bitbang_maple* me)
{
    uint32_t timer_ticks = 5;
    int avg_cnt = 10;
    int avg = 0;
    
    
    for(int i=0; i<avg_cnt; ++i)
    {
        avg += block_on_timer_ticks(me, timer_ticks);
    }
    
    avg /= avg_cnt;
    
    int err = avg / 2;
    int cnt = 0;
    
    do
    {
        cnt = block_on_timer_ticks(me, timer_ticks);
    }
    while( abs( avg - cnt ) < err );
    
    cnt = 0;
    while(cnt < 10 )
    {
        if( abs( avg - block_on_timer_ticks(me, timer_ticks) ) < err )
        {
            ++cnt;
        }
        else
        {
            cnt = 0;
        }
    }
}

bitbang_maple* bitbang_maple_new(uint8_t gpio_port_data1, uint8_t gpio_port_data5)
{
    bitbang_maple* me = malloc(sizeof(bitbang_maple));
    memset(me, 0, sizeof(bitbang_maple));
    
    me->gpio_port_data1 = gpio_port_data1;
    me->gpio_port_data5 = gpio_port_data5;
    
    me->buffer = (uint32_t*)malloc(sizeof(uint32_t) * _buffer);
    
    me->gpio_reg  = bcm_register_map(BCM_REG_GPIO_OFF, sizeof(bcm_reg_gpio));
    me->timer_reg = bcm_register_map(BCM_REG_TIMER_OFF, sizeof(bcm_reg_timer));
    
    bcm_set_gpio_function(me->gpio_reg, gpio_port_data1, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, gpio_port_data5, GPIO_PIN_FUNCTION_INPUT);
    
    bcm_set_gpio_function(me->gpio_reg, _dbg_pin, GPIO_PIN_FUNCTION_OUTPUT);
    
    return me;
}

void bitbang_maple_delete(bitbang_maple* me)
{
    bcm_set_gpio_function(me->gpio_reg, _dbg_pin, GPIO_PIN_FUNCTION_INPUT);
    
    bcm_register_unmap(me->timer_reg, sizeof(bcm_reg_timer));
    bcm_register_unmap(me->gpio_reg, sizeof(bcm_reg_gpio));
    
    free(me->buffer);
    me->buffer=NULL;
    
    free(me);
}


static maple_packed_raw* buffer_to_raw(bitbang_maple* me, uint32_t samples)
{
    maple_packed_raw* raw = malloc(sizeof(maple_packed_raw));
    memset(raw, 0, sizeof(maple_packed_raw));
    
    
    uint32_t full_bytes = samples / 8;
    if( samples % 8 != 0 )
        full_bytes += 1;
    
    raw->sample_count = samples;
    raw->samples_data1 = (uint8_t*)malloc(full_bytes);
    memset(raw->samples_data1, 0, sizeof(full_bytes));
    raw->samples_data5 = (uint8_t*)malloc(full_bytes);
    memset(raw->samples_data5, 0, sizeof(full_bytes));
    
    for( int i=0; i<(full_bytes*8); ++i )
    {
        raw->samples_data1[ i/8 ] <<=1;
        raw->samples_data5[ i/8 ] <<=1;
        
        if( me->buffer[i/16] & (0x80000000 >> ((i*2+0)%32) ))
            raw->samples_data1[ i/8 ] |= 1;
        
        if( me->buffer[i/16] & (0x80000000 >> ((i*2+1)%32) ))
            raw->samples_data5[ i/8 ] |= 1;
    }
    
    return raw;
}

static maple_packed_raw* empty_raw(bitbang_maple* me)
{
    maple_packed_raw* raw = malloc(sizeof(maple_packed_raw));
    memset(raw, 0, sizeof(maple_packed_raw));
    return raw;
}

static void wait_for_idle(bitbang_maple* me)
{
    uint32_t mask = (1<<me->gpio_port_data1) | (1<<me->gpio_port_data5);
    uint32_t sync = 0;
    
    while(sync < 100)
    {
        if( (me->gpio_reg->GPLEV0 & mask) == mask )
        {
            ++sync;
        }
        else
        {
            sync = 0;
        }
    }
}

maple_packed_raw* bitbang_maple_read_next_raw(bitbang_maple* me)
{
    uint32_t mask = (1<<me->gpio_port_data1) | (1<<me->gpio_port_data5);
    uint32_t m1 = 1<<me->gpio_port_data1;
    uint32_t m5 = 1<<me->gpio_port_data5;
    
    wait_for_idle(me);
    
    // wait for next word to start
    uint32_t timeout = 128*1024; // FIXME this number is a guess and based on pi1 b+ (v1.5) measurements
    while((me->gpio_reg->GPLEV0 & mask) == mask)
    {
        --timeout;
        if( timeout == 0 )
        {
            debug_hi(me);
            usleep(500);
            debug_low(me);
            return empty_raw(me);
        }
    }
    
    uint32_t word = 0;
    uint32_t cur = 0;
    
    debug_hi(me);
    
    for(int w=0; w<_buffer;++w)
    {
        for(int b=0; b<16;++b)
        {
            cur = me->gpio_reg->GPLEV0;
            word <<= 2;
            
            if( cur & m1 )
                word |= 2;
            if( cur & m5 )
                word |= 1;
        }
        
        me->buffer[w] = word;
        // TODO break early
    }
    
    debug_low(me);
    
    return buffer_to_raw(me, _buffer*16);
}

static void send_lead_in(bitbang_maple* me)
{
    data1_low(me);
    delay_ns(me, 100);
    data5_low(me);
    
    delay_ns(me, 250);
    data5_hi(me);
    delay_ns(me, 100);
    data5_low(me);
    
    delay_ns(me, 250);
    data5_hi(me);
    delay_ns(me, 100);
    data5_low(me);
    
    delay_ns(me, 250);
    data5_hi(me);
    delay_ns(me, 100);
    data5_low(me);
    
    delay_ns(me, 250);
    data5_hi(me);
    delay_ns(me, 100);
    data1_hi(me);
    delay_ns(me, 100);
    data5_low(me);
    delay_ns(me, 100);
}

static void send_lead_out(bitbang_maple* me)
{
    delay_ns(me, 100);
    
    data5_hi(me);
    delay_ns(me, 100);
    data5_low(me);
    delay_ns(me, 100);
    
    data1_low(me);
    delay_ns(me, 250);
    data1_hi(me);
    delay_ns(me, 100);
    
    data1_low(me);
    delay_ns(me, 250);
    data1_hi(me);
    delay_ns(me, 100);
    
    data5_hi(me);
}

static void send_2_bits(bitbang_maple* me, uint8_t bit1, uint8_t bit2)
{   
    delay_ns(me, 100);
    
    if( bit1 != 0)
        data5_hi(me);
    
    delay_ns(me, 100);
    
    data1_low(me);
    
    delay_ns(me, 250);
    
    data5_hi(me);
    
    delay_ns(me, 100);
    
    if( bit2 != 0)
        data1_hi(me);
    
    delay_ns(me, 100);
    
    data5_low(me);
    
    delay_ns(me, 250);
    
    data1_hi(me);
}

void bitbang_maple_send(bitbang_maple* me, uint8_t* raw_data, uint32_t len)
{
    /*
    for(int i=0; i<10;++i)
        align_to_next_yield(me);*/
    
    align_to_next_yield_2(me);
    //delay_ns(me, 1000);
    
        
    
    
    
    debug_hi(me);
    
    data1_hi(me);
    data5_hi(me);
    bcm_set_gpio_function(me->gpio_reg, me->gpio_port_data1, GPIO_PIN_FUNCTION_OUTPUT);
    bcm_set_gpio_function(me->gpio_reg, me->gpio_port_data5, GPIO_PIN_FUNCTION_OUTPUT);

    send_lead_in(me);
    
    for(uint32_t i=0; i<len; ++i)
    {
        send_2_bits(me, raw_data[i] & 0x80, raw_data[i] & 0x40);
        send_2_bits(me, raw_data[i] & 0x20, raw_data[i] & 0x10);
        send_2_bits(me, raw_data[i] & 0x08, raw_data[i] & 0x04);
        send_2_bits(me, raw_data[i] & 0x02, raw_data[i] & 0x01);
    }
    
    send_lead_out(me);

    bcm_set_gpio_function(me->gpio_reg, me->gpio_port_data1, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, me->gpio_port_data5, GPIO_PIN_FUNCTION_INPUT);
    debug_low(me);
}

void bitbang_maple_time_delay(bitbang_maple* me)
{
    debug_hi(me);
    delay_ns(me, 100);
    debug_low(me);
    delay_ns(me, 250);
    debug_hi(me);
    delay_ns(me, 100);
    debug_low(me);
}

void bitbang_maple_detect_yield(bitbang_maple* me)
{
    uint32_t dbg = 0;
    
    for(int i=0; i<10; ++i)
    {
        align_to_next_yield_2(me);
        
        if( dbg )
        {
            debug_low(me);
        }
        else
        {
            debug_hi(me);
        }
        
        dbg ^= 1;
    }
}

void maple_packed_raw_print(maple_packed_raw* raw)
{
    printf("Read %d samples\n", raw->sample_count);
    
    printf("  D5  D1\n");
    
    for( int i=0; i<raw->sample_count; ++i)
    {
        uint8_t mask = 0x80 >> i%8;
        int index = i/8;
        printf("  %d   %d\n", (raw->samples_data5[index] & mask) != 0, (raw->samples_data1[index] & mask) != 0); 
    }
}

void maple_packed_raw_print_compact(maple_packed_raw* raw)
{
    printf("s=%d,D1=0x", raw->sample_count);
    
    int byte_count = raw->sample_count / 8;
    if( raw->sample_count % 8 != 0 )
        byte_count += 1;
        
    for(int i=0; i<byte_count; ++i)
    {
        printf("%02x", raw->samples_data1[i]);
    }
    
    printf(",D5=0x");
    
    for(int i=0; i<byte_count; ++i)
    {
        printf("%02x", raw->samples_data5[i]);
    }
    
    printf("\n");
}

void maple_packed_raw_delete(maple_packed_raw* raw)
{
    if( raw->samples_data1 != NULL )
        free(raw->samples_data1);
    if( raw->samples_data5 != NULL )
        free(raw->samples_data5);
    
    raw->samples_data1 = NULL;
    raw->samples_data5 = NULL;
    free(raw);
}
