#include "common.h"
#include "spi0.h"
#include "bcm-hw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// usleep
#include <unistd.h>


#define GPIO_SPI_MISO 9
#define GPIO_SPI_MOSI 10
#define GPIO_SPI_SCLK 11


static void setup(spi0* me)
{
    // clear fifo
    me->spi_reg->CS = 3 <<4;

    // not sure if that is needed to actually clear the fifos
    usleep(10);
    
    // CPOL=1 CPHA=0
    me->spi_reg->CS = \
        (1<<3) | (0<<2);
        
    me->spi_reg->CLK=32;
}

spi0* spi0_new()
{
    spi0* me = malloc(sizeof(spi0));
    memset(me, 0, sizeof(spi0));
    
    me->gpio_reg = bcm_register_map(BCM_REG_GPIO_OFF, sizeof(bcm_reg_gpio));
    me->spi_reg = bcm_register_map(BCM_REG_SPI0_OFF, sizeof(bcm_reg_spi0));
    
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_MISO, GPIO_PIN_FUNCTION_ALT0);
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_MOSI, GPIO_PIN_FUNCTION_ALT0);
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_SCLK, GPIO_PIN_FUNCTION_ALT0);
    
    setup(me);
    
    return me;
}

void spi0_delete(spi0* me)
{
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_MISO, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_MOSI, GPIO_PIN_FUNCTION_INPUT);
    bcm_set_gpio_function(me->gpio_reg, GPIO_SPI_SCLK, GPIO_PIN_FUNCTION_INPUT);

    bcm_register_unmap(me->spi_reg, sizeof(bcm_reg_spi0));
    bcm_register_unmap(me->gpio_reg, sizeof(bcm_reg_gpio));
}

#define SPI0_CS_TA 7
#define SPI0_CS_DONE 16

void spi0_send_one_fill(spi0* me, uint32_t pattern)
{
    // TA = true
    me->spi_reg->CS |= 1<<SPI0_CS_TA;
    
    // Fill fifo
    for(int i=0; i<16; ++i)
        me->spi_reg->FIFO = pattern;
    
    while((me->spi_reg->CS & (1<<SPI0_CS_DONE)) == 0 );
    
    me->spi_reg->CS &= ~(1<<SPI0_CS_TA);
}

