#ifndef _BCM_HW_H
#define _BCM_HW_H

#include "bcm-registers.h"
#include <stdint.h>
#include <sys/types.h>


#define u32_mask_inv(bit)            (~(1<<(bit)))
#define u32_bit_clear(word,bit)  (word & u32_mask_inv(bit))

// map register memory for direct access from user space and return a user space pointer to it
volatile void * bcm_register_map(off_t register_offset, size_t size_of_register);
void bcm_register_unmap(volatile void * reg, size_t size_of_register);

void bcm_set_gpio_function(volatile bcm_reg_gpio * gpio, uint8_t gpio_port, uint8_t function);

double bcm_get_plld_divider(uint32_t target_freq_hz);
uint32_t bcm_to_clock_divider(double divider);


void bcm_reg_pwm_print(volatile bcm_reg_pwm * reg, const char * name);
void bcm_reg_pcm_print(volatile bcm_reg_pcm * reg, const char * name);

void bcm_start_clock_plld(const char * name, volatile uint32_t * reg_clock_ctl, volatile uint32_t * reg_clock_div, uint32_t frequency_hz);
void bcm_stop_clock(const char * name, volatile uint32_t * reg_clock_ctl, volatile uint32_t * reg_clock_div);

#endif
