# References

- https://www.raspberrypi.org/documentation/usage/gpio/
- https://github.com/hzeller/rpi-gpio-dma-demo
- https://github.com/metachris/raspberrypi-pwm/blob/master/rpio-pwm/rpio_pwm.c
- https://github.com/repk/epd/blob/master/rpi/pwmconf.c#L244
- https://www.raspberrypi.org/forums/viewtopic.php?t=37770
- https://www.scribd.com/doc/127599939/BCM2835-Audio-clocks
- https://pinout.xyz/pinout/gpclk#
- https://www.raspberrypi.org/forums/viewtopic.php?t=243166
