#include <stdio.h>
#include <stdlib.h>

// usleep / sleep / signal
#include <unistd.h>
// signal
#include <signal.h>
// strcmp
#include <string.h>

#include "pcm.h"
#include "pwm.h"
#include "spi0.h"
#include "bitbang-maple.h"
#include "executor.h"
#include "common.h"

static int run;

void sig_handler(int signum)
{
    run = 0;
}

static void print_data(pcm_fifo* fifo)
{
    //printf("Got %d words\n", fifo->fill);
    
    for(int i=0; i< fifo->fill; ++i)
    {
        printf("W%02d: 0x%08X\n", fifo->fill, fifo->frames[i]);
    }
    
}

int args_to_freq(double* f_mhz, int argc, char** argv)
{
    if( argc != 1 )
    {
        puts("Need frequency in MHz");
        return -1;
    }
    
    (*f_mhz) = strtod(argv[0], NULL);
    
    if( (*f_mhz) < 0.1 )
    {
        puts("Frequency not useable");
        return -1;
    }
    
    return 0;
}

static int main_avr_clock(int argc, char** argv)
{
    double f_mhz;
    if( args_to_freq(&f_mhz, argc, argv) !=  0)
        return -1;
    
    
    pwm* avrclk = pwm_new(GPIO_AVR_CLOCK, f_mhz*1000*1000);
    pwm_start(avrclk);
    
    printf("AVR clock running at %lf MHz, press Ctrl+C to quit\n", f_mhz);
    
    run = 1;
    signal(SIGINT,sig_handler);
    
    while(run)
    {
        usleep(100000);
    }

    pwm_delete(avrclk);

    return 0;
}

static int main_rx_data(int argc, char** argv)
{
    double f_mhz;
    if( args_to_freq(&f_mhz, argc, argv) !=  0)
        return -1;
    
    pwm* avrclk = pwm_new(GPIO_AVR_CLOCK, f_mhz*1000*1000);
    pwm_start(avrclk);
    
    
    pcm_fifo fifo;
    pcm* pcm_spi = pcm_new();
    
    run = 1;
    signal(SIGINT,sig_handler);

    puts("Start");
    
    while(run)
    {
        pcm_read_fifo(pcm_spi, &fifo);
        print_data(&fifo);
    }

    pcm_delete(pcm_spi);
    pwm_delete(avrclk);
    
    return 0;
}

static int test_dma(int argc, char** argv)
{
    double f_mhz;
    if( args_to_freq(&f_mhz, argc, argv) !=  0)
        return -1;
    
    
    pwm* avrclk = pwm_new(GPIO_AVR_CLOCK, f_mhz*1000*1000);
    pwm_start(avrclk);
    
    printf("PWM clock running at %lf MHz, press Ctrl+C to quit\n", f_mhz);
    
    run = 1;
    signal(SIGINT,sig_handler);
    
    printf("bcm_reg_dma size: %d 0x%04x\n", sizeof(bcm_reg_dma), sizeof(bcm_reg_dma));
    
    while(run)
    {
        // TODO
        usleep(100000);
    }

    pwm_delete(avrclk);

    return 0;
}



static int read_bitbang(int argc, char** argv)
{    
    run = 1;
    signal(SIGINT,sig_handler);
    
    bitbang_maple* maple = bitbang_maple_new(GPIO_DATA_1, GPIO_DATA_5);

    while(run)
    {
        maple_packed_raw* raw = bitbang_maple_read_next_raw(maple);
        maple_packed_raw_print_compact(raw);
        maple_packed_raw_delete(raw);
    }

    bitbang_maple_delete(maple);

    return 0;
}

static int read_hex_from_stdin(uint8_t* buffer, int max_len)
{
    // stuff must start with "0x"
    
    int tmp = fgetc(stdin);
    if( tmp != '0' )
        die_1("Expected hex string starting with '0x', but got '%c'", (char)tmp);
    tmp = fgetc(stdin);
    if( tmp != 'x' )
        die_1("Expected hex string starting with '0x', but got '%c'", (char)tmp);
    
    int len = 0;
    while(len < max_len )
    {
        if( fscanf(stdin, "%2x", &tmp) != 1)
            return len;
        
        buffer[len] = (uint8_t)tmp;
        ++len;
    }
    
    die_1("Input was too long, max length is %d bytes", max_len);
    
    return 0;
}

static int request_bitbang(int argc, char** argv)
{    
    uint8_t data[1024];
    int len = read_hex_from_stdin(data, sizeof(data));
    
    // Logical: 0x09, 0x20, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01
    // Wire:    0x01, 0x00, 0x20, 0x09, 0x01, 0x00, 0x00, 0x00
    
    //uint8_t data[] = { 0x80 };
    //uint8_t data[] = { 0x01, 0x00, 0x20, 0x09, 0x01, 0x00, 0x00, 0x00,  0x29 };
    //uint8_t data[] = { 0x01, 0x00, 0x20, 0x01, 0x20 };
    // 0x0100200120
    
    bitbang_maple* maple = bitbang_maple_new(GPIO_DATA_1, GPIO_DATA_5);

    bitbang_maple_send(maple, data, len);
    
    maple_packed_raw* raw = bitbang_maple_read_next_raw(maple);
    maple_packed_raw_print_compact(raw);
    maple_packed_raw_delete(raw);

    bitbang_maple_delete(maple);

    return 0;
}

static int run_executor(int argc, char** argv)
{
    executor* ex = executor_new(stdin, stdout);
    executor_run(ex);
    executor_delete(ex);
    return 0;
}

static int time_delay(int argc, char** argv)
{    
    run = 1;
    signal(SIGINT,sig_handler);
    
    bitbang_maple* maple = bitbang_maple_new(GPIO_DATA_1, GPIO_DATA_5);

    while(run)
    {
        bitbang_maple_time_delay(maple);
        usleep(500000);
    }

    bitbang_maple_delete(maple);

    return 0;
}

static int detect_yield(int argc, char** argv)
{    
    run = 1;
    signal(SIGINT,sig_handler);
    
    bitbang_maple* maple = bitbang_maple_new(GPIO_DATA_1, GPIO_DATA_5);

    while(run)
    {
        bitbang_maple_detect_yield(maple);
        //usleep(500000);
        //run = 0;
    }

    bitbang_maple_delete(maple);

    return 0;
}

static int test_spi(int argc, char** argv)
{    
    run = 1;
    signal(SIGINT,sig_handler);
    
    spi0* spi = spi0_new();

    while(run)
    {
        run = 0;
        spi0_send_one_fill(spi, 0xffff0000);
    }

    spi0_delete(spi);

    return 0;
}

int main(int argc, char** argv)
{
    if( argc < 2 )
    {
        puts("Need action");
        return -1;
    }
    
    int n_argc = argc-2;
    char** n_argv = argv+2;
    
    if( strcmp(argv[1], "avr-clock") == 0)
    {
        return main_avr_clock(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "rx-data") == 0)
    {
        return main_rx_data(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "test-dma") == 0)
    {
        return test_dma(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "read-bitbang") == 0)
    {
        return read_bitbang(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "request-bitbang") == 0)
    {
        return request_bitbang(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "time-delay") == 0)
    {
        return time_delay(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "detect-yield") == 0)
    {
        return detect_yield(n_argc, n_argv);
    }
        
    if( strcmp(argv[1], "run-executor") == 0)
    {
        return run_executor(n_argc, n_argv);
    }
    
    if( strcmp(argv[1], "test-spi") == 0)
    {
        return test_spi(n_argc, n_argv);
    }
    
    printf("Don't know what to do with action '%s'\n", argv[1]);
    return -1;
}

