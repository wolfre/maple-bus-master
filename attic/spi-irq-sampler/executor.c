#include "common.h"
#include "executor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _BUFFER_SIZE 1024

executor* executor_new(FILE* input, FILE* output)
{
    executor* me = malloc(sizeof(executor));
    memset(me, 0, sizeof(executor));
    
    me->input = input;
    me->output = output;
    me->maple = bitbang_maple_new(GPIO_DATA_1, GPIO_DATA_5);
    me->buffer = (uint8_t*)malloc(_BUFFER_SIZE);
    return me;
}

void executor_delete(executor* me)
{
    free(me->buffer);
    bitbang_maple_delete(me->maple);
    me->maple = NULL;
    me->input = NULL;
    me->output = NULL;
    me->buffer = NULL;
    free(me);
}

static uint32_t read_data_to_buffer(executor* me)
{
    uint32_t len = 0;
    
    int tmp = fgetc(me->input);
    if( tmp < 0 )
        die_0("EOF while reading length (MSB)");
    
    len |= tmp & 0xff;
    len <<= 8;
    
    tmp = fgetc(me->input);
    if( tmp < 0 )
        die_0("EOF while reading length (LSB)");
    
    len |= tmp & 0xff;
    
    if( len > _BUFFER_SIZE )
        die_1("Length (%d) was greater than buffer size", len);
    
    
    size_t read = 0;
    while(read < len)
    {
        size_t left = len-read;
        size_t r = fread ( me->buffer, 1, left, me->input );
        
        if( r < 1 )
            die_0("EOF while reading data");
        
        read += r;
    }
    
    return len;
}

static void write_data_to_output(executor* me, uint32_t length, uint8_t* data)
{
    size_t written = 0;
    while(written < length)
    {
        size_t left = length - written;
        size_t w = fwrite ( data, 1, left, me->output );
        if( w < 1 )
            die_1("EOF while writing %d bytes of data", length);
        
        written += w;
    }
}

static void write_raw_packet_to_output(executor* me, maple_packed_raw* raw)
{
    uint8_t sample_count[4];
    
    uint32_t tmp = raw->sample_count;
    sample_count[3] = tmp & 0xff;
    tmp >>= 8;
    sample_count[2] = tmp & 0xff;
    tmp >>= 8;
    sample_count[1] = tmp & 0xff;
    tmp >>= 8;
    sample_count[0] = tmp & 0xff;
    
    write_data_to_output(me, sizeof(sample_count), sample_count);
    
    uint32_t byte_length = raw->sample_count;
    byte_length /= 8;
    if( raw->sample_count % 8 != 0)
        byte_length += 1;
    
    write_data_to_output(me, byte_length, raw->samples_data1);
    write_data_to_output(me, byte_length, raw->samples_data5);
    fflush(me->output);
}

static void do_request_response(executor* me)
{
    uint32_t len = read_data_to_buffer(me);
    
    bitbang_maple_send(me->maple, me->buffer, len);
    
    maple_packed_raw* raw = bitbang_maple_read_next_raw(me->maple);
    write_raw_packet_to_output(me, raw);
    maple_packed_raw_delete(raw);
}

static void test_request_response(executor* me)
{
    maple_packed_raw raw;
    memset(&raw, 0, sizeof(maple_packed_raw));
    
    uint32_t len = read_data_to_buffer(me);
    
    raw.sample_count = (len * 8);
    if( len % 2 )
        raw.sample_count -= 4;
    
    raw.samples_data1 = malloc(len);
    raw.samples_data5 = malloc(len);
    
    memcpy(raw.samples_data1, me->buffer, len);
    memcpy(raw.samples_data5, me->buffer, len);
    
    write_raw_packet_to_output(me, &raw);
    
    free(raw.samples_data1);
    free(raw.samples_data5);
}

void executor_run(executor* me)
{
    while(1)
    {
        int next_cmd = fgetc(me->input);
        if( next_cmd == 0 || next_cmd < 0 )
            return;
        
        if( next_cmd == 1 )
            do_request_response(me);
        
        if( next_cmd == 2 )
            test_request_response(me);
    }
    

}
