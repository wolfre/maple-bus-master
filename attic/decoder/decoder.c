#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>


static uint32_t hex_to_int(const char* hex)
{
    // https://stackoverflow.com/questions/10156409/convert-hex-string-char-to-int
    return (uint32_t)strtol(hex, NULL, 16);
}

static void d_bool(uint32_t n, uint8_t bit, const char* name)
{
    printf("%s=%s ", name, ((n>>bit) & 1) == 0 ? "false" : "true ");
}

static void d_bits(uint32_t n, uint8_t off, uint8_t bits, const char* name)
{
    printf("%s=", name);
    
    for(int i=0; i<bits; ++i)
    {
        uint32_t mask = 1 << (off-i);
        printf("%d", (n&mask) ? 1 : 0 );
    }
    
    printf(" ");
}

static void d_uint(uint32_t n, uint8_t off, uint8_t bits, const char* name)
{
    uint32_t uint = n >> off;
    uint &= ~(0xffffffff << bits);
    printf("%s=%d ", name, uint);
}

static void decoder_epr(uint32_t n)
{
    printf("EPnR 0x%04x: ", n);
    
    d_bool(n, 15, "CTR_RX");
    d_uint(n, 14, 1, "DTOG_RX");
    d_bits(n, 12, 2, "STAT_RX");
    d_bool(n, 11, "SETUP");
    d_bits(n, 9, 2, "EP_TYPE");
    d_bool(n, 8, "EP_KIND");
    d_bool(n, 7, "CTR_TX");
    d_uint(n, 6, 1, "DTOG_TX");
    d_bits(n, 4, 2, "STAT_TX");
    d_uint(n, 0, 4, "EA");
    
    printf("\n");
}

int main(int argc, const char** argv)
{
    if( argc != 3 )
    {
        puts("Need register and value");
        return -1;
    }
    
    if( strcmp("EPR", argv[1]) == 0 )
    {
        decoder_epr(hex_to_int(argv[2]));
        return 0;
    }
    
    printf("Don't know register %s\n", argv[1]);
    return -2;
}
