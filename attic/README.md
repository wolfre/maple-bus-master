# The attic

Things that I tried and didn't work, and other interesting things.

- Some work on [AVR 8-Bit sampling code](avr)
- Some work on [Raspberry PI 1 sampling code](spi-irq-sampler)
- A decoder for the [STM32F1 USB EndPointRegister](decoder) to help debug USB state machine

# Things I tried, and failed

## Sampling with an AVR 8-Bit

So I used an ATMega8 and clocked it from a PWM output from a Raspberry PI.
This way I could freely adjust the clock / sample rate.
The idea was to sample with the AVR and stream the sampled data live to the PI.
This was done using the I2S in slave mode on the PI, as none of the SPI interfaces can be used in slave mode.
The AVR was using its SPI interface in master mode.

The problem is that sampling is just about fast enough at 18Mhz AVR clock, but SPI is too slow.
The max speed is fCPU / 2, which gives about 9Mhz and 4.5 MSps (2 data lines).
This could work with a faster AVR maybe.

## Sampling with the Raspberry PI in SPI

The idea was to use one of the SPI interfaces on the PI in master mode, and with the SPI clock output, clock the I2S interface in slave mode.
So the SPI interface on its MISO would have sampled one of the two data lines, and the I2S interface on its input would have sampled the other data line.
This didn't work out, because I couldn't get the PI to continuously output SPI clock.
There's always around 1 (SPI) clock cycle of idle time between words.
This is no problem for the I2S, but basically cuts the effective sample rate in half.
Or from another PoV, the sample rate would have to be around 2x the actually needed rate, just to overcome this "clock gap".

This behavior may be different if the SPI is served via DMA (see BCM preipherials data sheet for some hints).

## Sampling with the Raspberry PI in GPIO bitbang mode

So this was never going to work reliably, with an OS running at the same time.
Even if the sampling application gets an entire CPU core to itself, the GPIO is still on a shared bus with other things.
Most noteably the USB interface (and ethernet chip) that produces a lot of traffic.

However the idea was to find a short amount of time where we do get the bus to ourselfs.
A full VMU page takes just around 4 ms or so, and this seemed possible.
The way I tried this was to track the amount of wait time I got, polling a HW timer on the PI.
Looking at this I tried to find a quite time, and then doing the MapleBus request + response sampling.

The results were quite interesting, playing around with chrt and migrating process between various cores.
However they were never good enough to reliably get bigger responses (the one I used to test was the get device info which returns a bit over 100 bytes).
The raw [notes can be found here](pi-gpio-bitbang-tests.md).
