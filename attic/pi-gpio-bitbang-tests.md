# Notes on Raspberry PI GPIO bitbang tests

## Things done to quite down pi

went through

```bash
sudo apt list --installed
```

and removed a few packages that provide daemons, or daemon-ish things, to reduce bg activity:

```bash
sudo apt purge cron bluez wpasupplicant triggerhappy
```

Then I disabled a few things:

```bash
sudo systemctl stop mono-xsp4.service 
sudo systemctl disable mono-xsp4.service 
sudo killall wpa_supplicant
```

## Things found out for reliability

pi is noisy, but it highly depends on the core you are running on.

```bash
sudo taskset -c 1 ./sampler detect-yield
```

yielded quite good results in timing tests. Now this may be different for each startup, so this is how to migrate all tasks to cores 0,2,3

```bash
ps aux | sed "s/ \+/ /g" | cut -d " " -f 2 | while read -r pid ; do sudo taskset -p -c 0,2,3 $pid ; done
```

## measurements 1

average tries (averaged over 25 rounds) to get one successfull status word with 116 bytes read (bailout after 11 tries)

with tasks migrated to cpus 0,2,3 and smapler on cpu 1, no aligning to yields

```
taskset -c 0,2,3 mono BusMaster.exe sampler 
Average of 11 tries with chrt=off, taskset=True
Average of 9.48 tries with chrt=off, taskset=False
Average of 11 tries with chrt=1, taskset=True
Average of 6.72 tries with chrt=1, taskset=False
Average of 10.16 tries with chrt=99, taskset=True
Average of 7.68 tries with chrt=99, taskset=False
tests took 109.649761 sec
```

with tasks migrated to cpus 0,2,3 and smapler on cpu 1, aligning to yields v1

```
taskset -c 0,2,3 mono BusMaster.exe sampler 
Average of 10.04 tries with chrt=off, taskset=True
Average of 11 tries with chrt=off, taskset=False
Average of 10.68 tries with chrt=1, taskset=True
Average of 8.96 tries with chrt=1, taskset=False
Average of 11 tries with chrt=99, taskset=True
Average of 5.8 tries with chrt=99, taskset=False
tests took 117.152875 sec
```

with tasks migrated to cpus 0,2,3 and smapler on cpu 1, aligning to yields v2

```
taskset -c 0,2,3 mono BusMaster.exe sampler 
Average of 10.6 tries with chrt=off, taskset=True
Average of 11 tries with chrt=off, taskset=False
Average of 11 tries with chrt=1, taskset=True
Average of 7.8 tries with chrt=1, taskset=False
Average of 11 tries with chrt=99, taskset=True
Average of 9.88 tries with chrt=99, taskset=False
tests took 127.851442 sec
```

results don't look pretty especially taskset ones.
So for some reason even if that helps with simple tests, it does not do anything good for real usage.

will restart pi and remove taskset and retry measurements.


no aligning to yields
```
mono BusMaster.exe sampler 
Average of 7.88 tries with chrt=off, taskset=False
Average of 5.4 tries with chrt=1, taskset=False
Average of 4.4 tries with chrt=99, taskset=False
tests took 31.608733 sec
```

aligning to yields v1
```
mono BusMaster.exe sampler 
Average of 7.64 tries with chrt=off, taskset=False
Average of 6.56 tries with chrt=1, taskset=False
Average of 4.04 tries with chrt=99, taskset=False
tests took 36.148142 sec
```

aligning to yields v2
```
mono BusMaster.exe sampler 
Average of 6.52 tries with chrt=off, taskset=False
Average of 3.72 tries with chrt=1, taskset=False
Average of 3.6 tries with chrt=99, taskset=False
tests took 27.556543 sec
```

## improving runtime

As of now, each transaction will startup a new sampler exe and do one transaction then quit.
This probably causes lots of noise, expecially in scheduling.
Will now change that to have a continuesly running sampler.

## measurements 2

with continuesly running sampler (averaged over 100 rounds):

no aligning to yields

```
mono BusMaster.exe sampler 
Average of 1.13 tries with chrt=off, taskset=False
Average of 1.19 tries with chrt=1, taskset=False
Average of 1.16 tries with chrt=99, taskset=False
tests took 2.630261 sec

```

aligning to yields v1

```
mono BusMaster.exe sampler 
Average of 1.19 tries with chrt=off, taskset=False
Average of 1.03 tries with chrt=1, taskset=False
Average of 1.03 tries with chrt=99, taskset=False
tests took 5.713113 sec
```

aligning to yields v2

```
mono BusMaster.exe sampler 
Average of 4.95 tries with chrt=off, taskset=False
Average of 1.34 tries with chrt=1, taskset=False
Average of 1.05 tries with chrt=99, taskset=False
tests took 14.176974 sec
``` 
